\documentclass[9.5pt,headsepline,cleardoubleempty,bibtotoc,tablecaptionabove,english,twoside]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage{multicol}
\usepackage{xcolor}
\usepackage{fontspec}
\usepackage{physics}
\usepackage{multirow}
\usepackage{bm}
\usepackage{mathtools}
\setcounter{MaxMatrixCols}{32}
\usepackage{arydshln}
\usepackage{blkarray}

\setlength{\columnsep}{7mm}
\setlength{\parskip}{3mm}

\newfontfamily\fontheading[SizeFeatures={Size=48}]{MyriadPro-Bold}
\newfontfamily\fontauthor[SizeFeatures={Size=24}]{MyriadPro-Light}
\newfontfamily\fontpagenr[SizeFeatures={Size=14}]{Myriad Pro}
\newfontfamily\fontsubheading[SizeFeatures={Size=16}]{MyriadPro-Bold}
\newfontfamily\fontcapt[SizeFeatures={Size=10}]{MyriadPro-Light}
\newfontfamily\fontfignr[SizeFeatures={Size=10}]{Myriad Pro}

\definecolor{headings}{HTML}{00adbd}
% violett #c02555
% dunkelblau #2b367c
% sehr dunkles Grau #231f20
% hellblau #00adbd
% orange #f99d1c
% lila #f99d1c
% grün #62bb46

\usepackage{libertine}
\usepackage{babel}

\addto{\captionsenglish}{%
  \renewcommand{\refname}{}
}


\usepackage{geometry}
\geometry{a4paper, margin=21mm}

\makeatletter
\newcommand*{\shifttext}[2]{%
  \settowidth{\@tempdima}{#2}%
  \makebox[\@tempdima]{\hspace*{#1}#2}%
}
\makeatother
\setlength{\parindent}{0pt}% Just for this example


\usepackage{fancyhdr}

\renewcommand{\headrulewidth}{0pt}
% \lhead{\shifttext{-15mm}{\fontpagenr\thepage}}
\cfoot{}
\pagestyle{fancy}

% \linespread{1.3}
% \setlength{\parskip}{\baselineskip}%
% \setlength{\parindent}{0pt}%
%generally needs more spacing for legibility

%\usepackage[onehalfspacing]{setspace}%
\title{Smooth Piecewise Polynomials}
\author{Josef Greilhuber}
\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{stmaryrd}
\usepackage{svg}
% \usepackage{array}
\usepackage[document]{ragged2e}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{calc}

\usepackage{dirtytalk}
%for adding a comment

\newcommand{\Ls}{L^2(\mathbb{R})}
\newcommand{\La}{L^1(\mathbb{R})}
\newcommand{\C}{\mathbb{C}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\fpf}{\cos (t |\triangledown | )f}
\newcommand{\fpg}{\frac{\sin(t| \triangledown | )}{|\triangledown |}g}
\newcommand{\cint}[4]{\int_{#1}^{#2} #3 d #4}
\newcommand{\F}{\mathcal{F}}
%\newtheorem{stz}{Satz}
\newtheorem{cor}{Corollary}
%\newtheorem{lem}{Lemma}
\newtheorem{exa}{Example}
\newtheorem{thm}{Theorem}
\newtheorem{defi}{Definition}
%\theoremstyle{definition}
\newtheorem{rem}{Remark}
%\newtheorem*{bem}{Bemerkung}
%\newtheorem{bsp}{Beispiel}
\newtheorem{prop}{Proposition}
\newtheorem{lem}{Lemma}

\numberwithin{equation}{section}
% Datum der Abgabe
\setmainfont[BoldFont={Minion Pro Bold}, ItalicFont={Minion Pro Italic}]{Minion Pro}

\newcommand{\subheading}[1]{
\vspace{8mm}
{\fontsubheading\textcolor{headings}{#1}}\vspace{2mm}

}

\begin{document}
% \maketitle

{\fontheading\textcolor{headings}{\foreignlanguage{nohyphenation}{\begin{sloppypar}
\baselineskip=48pt
  Tetrahedral Symmetry and High Temperature Superconductivity
\end{sloppypar}}}}

\vspace{5mm}

{\fontauthor\textcolor{headings}{
  Piper Fowler-Wright
}}

\vspace{30mm}

\justify

\begin{multicols}{2}

  The origin of high-temperature superconductivity\linebreak remains a major open problem
  in condensed matter physics.  A simple lattice model believed to capture the
  essential physics of the phenomenon is the \textit{Hubbard model.} In this
  article we aim to solve the Hubbard model for various numbers of spins on a
  single tetrahedron. Central to our strategy will be the use of
  symmetries. As such, we firstly review the symmetry group relevant to
  the problem at hand -- that of the tetrahedron.


  \subheading{Tetrahedral Symmetry}%
  % \label{sec:tetrahedral_symmetry}

  The symmetry group of the regular tetrahedron is \( S_4 \), the collection of
  permutations of four objects.  In fact, we cannot make use of the entire group,
  since in general its elements do not commute, a property necessary for the
  simultaneous diagonalisation our method will rely on.  Instead we turn to the
  largest \textit{abelian} subgroups of \( S_4 \).  These are \( C_4 \), the
  cyclic group of order four, and \( V_4 \), the Klein-4 group.  We choose to use
  \( V_4 \), but \( C_4 \) would be equally effective in reaching a solution.


  The elements of \( V_4 \) correspond to \( \pi \) rotations of the tetrahedron
  about the three coordinate axes shown in Figure 1.  Labelling the
  vertices 1 through 4 and performing these rotations, we obtain a representation
  \( \{I,R_x,R_y,R_z\} \)  of the group where \( I \) is the identity matrix and
   \begin{align*}
        R_x &= \begin{bmatrix}
        0 & 0 & 1 & 0 \\
        0 & 0 & 0 & 1 \\
        1 & 0 & 0 & 0 \\
        0 & 1 & 0 & 0 \\
      \end{bmatrix} \\[1em]
      R_y &= \begin{bmatrix}
        0 & 1 & 0 & 0 \\
        1 & 0 & 0 & 0 \\
        0 & 0 & 0 & 1 \\
        0 & 0 & 1 & 0 \\
      \end{bmatrix} \\[1em]
      R_z &= \begin{bmatrix}
        0 & 0 & 0 & 1 \\
        0 & 0 & 1 & 0 \\
        0 & 1 & 0 & 0 \\
        1 & 0 & 0 & 0 \\
      \end{bmatrix}.
  \end{align*}
  We will have need of the eigensystem of these matrices.  This is particularly
  simple as \( R_\alpha^2=I \) (\( \alpha \in \{x,y,z\} \)) restricts the
  possible eigenvalues to \( \pm1 \). Solving the 4-by-4 problem one obtains

\begin{center}
  \begin{tabular}{cc}
      $\begin{bmatrix}
        1 \\ 1 \\ 1 \\ 1
      \end{bmatrix}$
    &
      $\begin{bmatrix}
        -1 \\ -1 \\ 1 \\ 1
      \end{bmatrix}$
    \\[2em]
      $\begin{array}{c}
        R_x = + 1 \\
        R_y = +1 \\
        R_z = R_x R_y = +1
      \end{array}$
    &
      $\begin{array}{c}
        R_x = - 1 \\
        R_y=+1 \\
        R_z=-1
      \end{array}$
    \\[2em]
      $\begin{bmatrix}
         1 \\ -1 \\ 1 \\ -1
      \end{bmatrix}$
    &
      $\begin{bmatrix}
        -1 \\ 1 \\ 1 \\ -1
      \end{bmatrix}$
    \\[2em]
      $\begin{array}{c}
        R_x = + 1 \\
        R_y=-1 \\
        R_z=-1
      \end{array}$
    &
      $\begin{array}{c}
        R_x = - 1 \\
        R_y=-1 \\
        R_z=+1
      \end{array}$
  \end{tabular}
\end{center}
  These vectors, as ordered, will be referred to as the \( R_\alpha \) basis.




  \subheading{The Hubbard Model}


  The Hubbard Hamiltonian describes the behaviour of \textit{spins} (random
  variables with two possible values, \(\sigma =\, \uparrow \) or \(\downarrow
  \)).  It is written by the physicists as

  \vspace{7em}
  \begin{centering}
  \includegraphics[width={\columnwidth-0.5cm}]{figures/fig1largefont.pdf} \\
  \begin{quote}
    ${\textcolor{headings}{\blacktriangle}}$\enspace{\fontcapt {\fontfignr Figure 1:} A tetrahedral	arrangement of lattice sites and a two dimensional
    schematic (top right)  we use to represent this geometry.
    Tetrahedral symmetry: The elements of \(V_4\) describe \(\pi\)
    rotations about three axes passing through the \textit{centre}
    of each pair of opposite faces of the cube shown.  For example, \(R_x\)
    is the product of the two-cycles \((13)\) and   \((24)\), explaining
    its matrix elements on the previous page.} \\
  \end{quote}
  \label{fig:tet}
  \end{centering}
  \vspace{1em}

\columnbreak
%\footnote{%
% 	\( c_{j\sigma}^\dagger \)
% 	and \( c_{j \sigma} \) are creation and annihilation operators for a
% 	spin-1/2 particle on site \(j\)  satisfying the anti-commutation
% 	relations \(\{c_{j\sigma }^{\dagger},c_{j' \sigma'}\}= \delta_{j
% 	j'}\delta_{\sigma \sigma'}\), and \(n_{j\sigma}:=
% 	c^{\dagger}_{j\sigma}c_{j\sigma}\).  Particle states are constructed by
% 	acting the \(c^\dagger_{j\sigma}\) on the vacuum \(\ket{0}\);
% 	\(\ket{1}=c^{\dagger}_{1\uparrow}\ket{0}\) for example.  This is the
% 	formalism of \textit{second quantisation.}%
% }
\begin{align}
	H = -t \sum_{\langle j, j'\rangle,\, \sigma}
	c^{\dagger}_{j\sigma}c_{j'\sigma} + U \sum_j n_{j\uparrow}n_{j\downarrow}
	\tag{\( \ast \)}\label{eq:hubbard}
\end{align}
where \( j \) labels the sites of the lattice and \( t, U \) are positive
parameters obeying \( U\gg t \).
\( c_{j\sigma}^\dagger \)
	and \( c_{j \sigma} \) are creation and annihilation operators for a
	spin-1/2 particle at \(j\)  satisfying the anti-commutation
	relations \(\{c_{j\sigma }^{\dagger},c_{j' \sigma'}\}= \delta_{j
	j'}\delta_{\sigma \sigma'}\), and \(n_{j\sigma}:=
	c^{\dagger}_{j\sigma}c_{j\sigma}\).
%As we shall see, the two sums appearing here
%capture, in turn, kinetic and potential energy.

Fortunately, one does not need to be familiar with these notations in order
to work with the model: the terms in \eqref{eq:hubbard} are simply a set of
rules for the operation of \( H \) on any vector or \textit{ket} \( \ket{v} \)
representing the physical state of the system.  These rules are readily
understood by example.  Take a single up spin on the tetrahedron. Four possible
states are immediately evident:

\begin{center}
	\includegraphics{figures/inline1.pdf}
\end{center}
These states constitute  an \textit{orthonormal basis} for the single spin system.

The first term of \( H \), known as the \textit{hopping term,} describes the
transfer of the spin from one site \( j \) to a neighbouring site \( j' \).
For instance,
\begin{align*}
	H\ket{1} = -t \ket{2}-t\ket{3}-t\ket{4},
\end{align*}
since from the bottom left site the spin can hop to any of the other three
sites.  The sum \( \sum_{\langle j,j' \rangle \uparrow } \) is taken over all
nearest neighbours once; i.e., the 6 edges of our schematic.

The second term of \( H \) is only relevant in the presence of multiple spins:
\( n_{j\uparrow} \) counts the number of ups at \( j \) and \( n_{j\downarrow}
\) the number of downs.  Consider the states from a system of two spins:
\begin{center}
	\includegraphics{figures/inline2.pdf}
\end{center}
One has
\[
	U \sum_j n_{j\uparrow}n_{j\downarrow}  \ket{\vphantom{1}\smash{\widetilde{1}}}
	= U \ket{\vphantom{1}\smash{\widetilde{1}}}
\]
and
\[
	U \sum_j n_{j\uparrow}n_{j\downarrow}  \ket{\vphantom{2}\smash{\widetilde{2}}} = 0 .
\]
In other words, this is a diagonal term giving \( +U \) in the case of double
occupation and zero otherwise. It represents an on-site repulsion between, for
example, negatively charged electrons. In general, any site may be occupied by
an up, a down, both or neither.  Note that two spins of same orientation cannot
share a site, a fact known as the \textit{Pauli exclusion principle}.


Using these simple rules we can construct a matrix representation of \( H \).
Returning to the single spin system,
{%
\setlength{\abovedisplayskip}{0pt}%
\setlength{\belowdisplayskip}{-10pt}%
\setlength{\belowdisplayshortskip}{-10pt}%
\begin{align*}
	H =
	 \renewcommand\arraystretch{1.4}
	\begin{blockarray}{cccc}
		\ket{1} & \ket{2} & \ket{3}&\ket{4}\\
\begin{block}{[cccc]}
	0&-t&-t&-t\\
	-t&0&-t&-t\\
	-t&-t&0&-t\\
	-t&-t&-t&0\\
  \end{block}\\[-6pt]
\end{blockarray}.
\end{align*}
`Solving' the Hubbard Hamiltonian amounts to solving the eigenvalue problem
associated with this matrix. Of particular interest is the eigenvector with the
\textit{smallest} eigenvalue, since this describes the minimum energy or
\textit{ground} state of the system in question.
}

In the present case we notice that
\begin{align*}
 	H = -t(R_x+R_y+R_z).
\end{align*}
Therefore \(H\) is diagonal in the \(R_\alpha\) basis:
 \begin{align*}
H = \begin{bmatrix}
      -3t & 0 & 0 & 0 \\
      0   & t & 0 & 0 \\
      0   & 0 & t & 0 \\
      0   & 0 & 0 & t
    \end{bmatrix}
\end{align*}

We conclude that the ground state is non-degenerate and of energy \(-3t\). It
is characterised by the vector \([1,1,1,1]^T\), an equally-weighted
superposition of all four states, as one would expect on the grounds of
symmetry.  Note that exactly the same result would have been found for a single
fixed \textit{down} spin and the corresponding set of states%\footnote{The
%terms in \(H\) preserve spin, so there is no cross over between the two sets;
%they form \textit{invariant subspaces.}}
-- another symmetry.

This illustrates the use of symmetries in the most basic case.  In the main
part of this article we solve \( H \) for two \textit{opposite} spins, in the
limit \( U\gg t \) prescribed by the model.  Systems of 3 or more spins can be
handled in the same way.

\columnbreak

\subheading{Opposite Spins}

Each spin is free to occupy any of the four sites independently of the other,
giving \(4^2=16\) basis states, four of which involve double occupation.
Although \( H \) is no longer of the same dimension as the \( R_\alpha \), we
crucially use the symmetries to guide the labelling of our starting basis in
the following way: Starting from an arbitrary state \(\ket{1}\),  apply each
rotation to generate the next three states \(\ket{2}\), \(\ket{3}\),
\(\ket{4}\). Pick an unused state \(\ket{5}\) and do the same to obtain \(
\ket{6} \), \( \ket{7} \), \( \ket{8} \).  Repeat this procedure until all 16
states have been accounted for (Figure 2).

\vspace{1em}
\begin{centering}
\includegraphics[width={\columnwidth-0cm}]{figures/fig2.pdf} \\
\begin{quote}
  ${\textcolor{headings}{\blacktriangle}}$\enspace{\fontcapt {\fontfignr Figure 2:} Opposing spins. Successive states in each row are obtained by
  rotating the first state in that row \(\pi\) radians  around the
  \(x\), \(y\) and \(z\) axes  in Figure 1.
  \textit{Technical point:} The operators \(c_{j\uparrow}^{\dagger}\),
  \(c_{j'\downarrow}^{\dagger}\) anticommute, so one should be mindful
  of the order in which they occur.  We choose to always write the up
  spin first (e.g.\linebreak  \(\ket{1}:=c_{2\downarrow}^\dagger
  c_{1\uparrow}^{\dagger}\ket{0} = -
  c_{1\uparrow}^{\dagger}c_{2\downarrow}^\dagger \ket{0}\)).  That way,
  there are never any sign changes (\(+t\) terms) due to reordering
  after a hop between states\linebreak (Exercise: anyone who has seen second
  quantisation may like to think why this is).}
\end{quote}
\label{fig:tet}
\end{centering}
\vspace{1em}

Writing down \(H\) is then  a matter of determining which states are connected
by a \textit{single} hop of \textit{either} spin; each such connection conveys
a \(-t\) entry to the matrix in accordance with the above rules.  For instance,
one clearly has \(\mel{5}{H}{1}=-t\) but \(\mel{2}{H}{1}=0\).  Since the
rotations move \textit{two} spins, the states in any one row are always more
than one hop away from each other.  Remembering that states
\(\ket{13}\text{--}\ket{16}\) also come with a \( U \) self term, we find


\end{multicols}

\begin{align*}
   \renewcommand\arraystretch{1}
  H =\left[
  \begin{array}{cccc:cccc:cccc:cccc}
     0 & 0 & 0 & 0 & -t & 0 & 0 & -t & -t & -t & 0 & 0 & -t & 0 & -t & 0 \\
     0 & 0 & 0 & 0 & 0 & -t & -t & 0 & -t & -t & 0 & 0 & 0 & -t & 0 & -t \\
     0 & 0 & 0 & 0 & 0 & -t & -t & 0 & 0 & 0 & -t & -t & -t & 0 & -t & 0 \\
     0 & 0 & 0 & 0 & -t & 0 & 0 & -t & 0 & 0 & -t & -t & 0 & -t & 0 & -t \\ \hdashline
     -t & 0 & 0 & -t & 0 & 0 & 0 & 0 & -t & 0 & -t & 0 & -t & -t & 0 & 0 \\
     0 & -t & -t & 0 & 0 & 0 & 0 & 0 & 0 & -t & 0 & -t & -t & -t & 0 & 0 \\
     0 & -t & -t & 0 & 0 & 0 & 0 & 0 & -t & 0 & -t & 0 & 0 & 0 & -t & -t \\
     -t & 0 & 0 & -t & 0 & 0 & 0 & 0 & 0 & -t & 0 & -t & 0 & 0 & -t & -t \\\hdashline
     -t & -t & 0 & 0 & -t & 0 & -t & 0 & 0 & 0 & 0 & 0 & -t & 0 & 0 & -t \\
     -t & -t & 0 & 0 & 0 & -t & 0 & -t & 0 & 0 & 0 & 0 & 0 & -t & -t & 0 \\
     0 & 0 & -t & -t & -t & 0 & -t & 0 & 0 & 0 & 0 & 0 & 0 & -t & -t & 0 \\
     0 & 0 & -t & -t & 0 & -t & 0 & -t & 0 & 0 & 0 & 0 & -t & 0 & 0 & -t \\\hdashline
     -t & 0 & -t & 0 & -t & -t & 0 & 0 & -t & 0 & 0 & -t & U & 0 & 0 & 0 \\
     0 & -t & 0 & -t & -t & -t & 0 & 0 & 0 & -t & -t & 0 & 0 & U & 0 & 0 \\
     -t & 0 & -t & 0 & 0 & 0 & -t & -t & 0 & -t & -t & 0 & 0 & 0 & U & 0 \\
     0 & -t & 0 & -t & 0 & 0 & -t & -t & -t & 0 & 0 & -t & 0 & 0 & 0 & U \\
  \end{array}\right].
\end{align*}

\vspace{2em}

It is straightforward to identify the \(R_\alpha\):


\begin{align*}
  H =
   \renewcommand\arraystretch{1.8}
 \left[\begin{array}{c|c|c|c}
      O_{4\times4} & -t(I+R_z) & -t(I+R_y) & -t(I+R_x) \\\hline
      -t(I+R_z) & O_{4\times4} & -t(I+R_x) & -t(I+R_y) \\\hline
      -t(I+R_y) & -t(I+R_x) & O_{4\times4} & -t(I+R_z) \\\hline
      -t(I+R_x) & -t(I+R_y) & -t(I+R_z) & UI
  \end{array}\right]\qquad
\end{align*}

\vspace{2em}

\begin{multicols}{2}
Given this form, it is natural to look to a\linebreak 16-eigenvector based on one of the
vectors of the \(R_\alpha\) basis.  Working firstly with \([1,1,1,1]^T\), we
look for a solution to
% \\[2.5em]
\end{multicols}
\begin{align*}
   \renewcommand\arraystretch{3}
 \left[\begin{array}{c|c|c|c}
      O_{4\times4} & -t(I+R_z) & -t(I+R_y) & -t(I+R_x) \\[12pt]\hline
      -t(I+R_z) & O_{4\times4} & -t(I+R_x) & -t(I+R_y) \\[12pt]\hline
      -t(I+R_y) & -t(I+R_x) & O_{4\times4} & -t(I+R_z) \\[12pt]\hline
      -t(I+R_x) & -t(I+R_y) & -t(I+R_z) & UI
  \end{array}\right]
  & \hspace{-.7cm}
  \renewcommand\arraystretch{0.9}
  \begin{split}
  a
      \begin{bmatrix}
    1 \\ 1 \\ 1 \\1
  \end{bmatrix} \\
  b
      \begin{bmatrix}
    1 \\ 1 \\ 1 \\1
  \end{bmatrix} \\
  c
      \begin{bmatrix}
    1 \\ 1 \\ 1 \\1
  \end{bmatrix} \\
  d
      \begin{bmatrix}
    1 \\ 1 \\ 1 \\1
  \end{bmatrix}
  \end{split}
  =
  E \times \left\{ \vphantom{\begin{array}{c}
    1\\1\\1\\1\\1\\1\\1\\1\\1\\1\\1\\1\\1\\1\\1\\1
  \end{array}} \right.
  \renewcommand\arraystretch{0.9}
  \begin{split}
  a
      \begin{bmatrix}
    1 \\ 1 \\ 1 \\1
  \end{bmatrix} \\
  b
      \begin{bmatrix}
    1 \\ 1 \\ 1 \\1
  \end{bmatrix} \\
  c
      \begin{bmatrix}
    1 \\ 1 \\ 1 \\1
  \end{bmatrix} \\
  d
      \begin{bmatrix}
    1 \\ 1 \\ 1 \\1
  \end{bmatrix}
  \end{split},
\end{align*}


\begin{multicols}{2}


where \(a\), \(b\), \(c\) and \(d\) are to be determined. Accounting for the
action of each block on \(\left[1,1,1,1\right]^T\), which is \(R_\alpha\)
eigenvector with eigenvalue \(+1\), we have the equivalent to the 4-dimensional
problem \linebreak
\(A\bm{a}=(-E/2t)\bm{a}\) with
\begin{align*}
A=
  \begin{pmatrix}
    0 & 1 & 1 & 1 \\
    1 & 0 & 1 & 1 \\
    1 & 1 & 0 & 1 \\
    1 & 1 & 1 & -U/2t
  \end{pmatrix}\text{,} \quad  \bm{a}=
  \begin{pmatrix}
    a \\ b \\c \\ d
  \end{pmatrix}.
\end{align*}
Following tedious calculation or the use of a computer,
the characteristic polynomial of \(A\) is determined to be
\begin{align*}
  \chi(x) &= - U \left(16 t^3-12 t^2 x+x^3\right) \\
       &\phantom{=}\ -48 t^4+64 t^3 x-24 t^2 x^2+ x^4 \text .
\end{align*}
This confers four solutions.  To obtain information of the ground state energy
and its \( 1/U \) correction (\( U\gg t \)), scale \( \chi \) by a factor of \(
1/U \), as in
\begin{align*}
	\chi = P_3 + \frac{P_4}{U}
\end{align*}
where
\begin{align*}
	P_3(x) &= -\left(16 t^3-12 t^2 x+x^3\right) \\ &= -(x+4t)(x-2t)^2
	\intertext{and}
	P_4(x) &= -48 t^4+64 t^3 x-24 t^2 x^2+ x^4 .
\end{align*}
When \(U=\infty\), \(P_3\) gives the least root \(x=-4t\). For finite \(U\gg
t\), this is subject to a small correction according to
\begin{align*}
	0 = P_3+ \frac{P_4}{U} \rightarrow x &= -4t +
	\left.\frac{P_4}{U(x-2t)^2}\right\rvert_{x=-4t} \\ &= -4t - \frac{12t^2}{U},
\end{align*}
which gives us a candidate for the ground state energy.  It may be verified
that the other three vectors of the \(R_{\alpha}\) basis in fact yield 4-by-4
matrices with the same characteristic polynomial,
\begin{align*}
	\chi(x)
	&= (x-2t)^2(x+2t)^2-Ux(x-2t)(x+2t),
\end{align*}
which has least root  \(x\approx-2t\) for large \(U\).  Therefore our first
solution does describe the ground state, which is hence non-degenerate and of
energy
\newcommand*{\boxedcolor}{black}
\makeatletter
\renewcommand{\boxed}[1]{\textcolor{\boxedcolor}{%
  \fbox{\normalcolor\m@th$\displaystyle#1$}}}
\makeatother
\begin{align*}
  \Aboxed{E_0 =-4t - \frac{12t^2}{U}.}
\end{align*}

We can go further to determine the corresponding eigenvector in the limit \(
U\to\infty \).  To do so, note that in order to satisfy the eigenvalue equation
in this limit the bottom entry \(d\) of \(\bm{a}\) must be \(0\). This means we can `knock
out' the final row and column of \(A\) and solve
\begin{align*}
	  \begin{pmatrix}
    0 & 1 & 1 \\
    1 & 0 & 1 \\
    1 & 1 & 0
  \end{pmatrix} \bm{\widetilde{a}} = (-E/2t)\bm{\widetilde{a}}.
\end{align*}
When \(E=-4t\), we obtain the eigenvector \(\bm{a}=(1,1,1,0)^T\) which corresponds to
\begin{align*}
	 \ket{E_{0}} &=
	 \sum_{i=1}^{\bm{12}}\ket{i}
\end{align*}
in the original basis.
	This solution has important physical
	significance in relation to resonating valence bond (RVB) state
	proposed by P. Anderson in 1987 to account for high temperature
	superconductivity in the cuprates \cite{anderson87}.


\subheading{Conclusion}%
  \label{sec:conclusions}
  Symmetries are an integral part of any mathematical physicist's problem-solving
  toolkit.  Here we have seen the symmetries of the tetrahedron partition a 16
  dimensional matrix \( H \) into 4-by-4 blocks, reducing the dimensionality of
  the problem by a factor of 4.  For more complicated systems the reduction can
  be far greater and indeed may offer the only way into an otherwise intractable
  problem.


  \subheading{Further Reading}
  \vspace{-4.5em}
  \renewcommand\refname{}
  \begin{thebibliography}{9}
  \small
    \bibitem{anderson87}
    P. W. Anderson,
    Science \textbf{235},
    1996 (1987).

    Anderson's original paper on the resonating valence bond state in
    La\textsubscript{2}CuO\textsubscript{4} and superconductivity.

    \bibitem{lee06}
    P. A. Lee, N. Nagaosa, and X.-G. Wen,
    Rev. Mod. Phys \textbf{78},
    17 (2006).

    Review of high temperature superconductivity and the resonating valence bond
    theory.

    \bibitem{scalettar16}
    R. T. Scalettar, An Introduction to the Hubbard Hamiltonian,
    in \textit{Quantum Materials: Experiments and Theory}
    (Forschungszentrum Jülich, Jülich, 2016)
    Chap. 4.

    Accessible introduction to the Hubbard model and its basic physics. Open
    access.

    \bibitem{arfken13}
    G. B. Arfken, H. J. Weber and F. E. Harris,
    \textit{Mathematical Methods For Physicists: A Comprehensive Guide,}
    7th ed.
    (Academic Press, Oxford, 2013)
    Chap. 17.
    %pp. 815-871.

    Covers elementary group theory and the use of symmetries in physics.

  \end{thebibliography}

\end{multicols}

\end{document}
