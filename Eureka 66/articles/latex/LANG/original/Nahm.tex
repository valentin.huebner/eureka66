\documentclass[12pt]{article}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}

\title{Simplifying Differential Equations Using Group Work}
\author{C.~J.~Lang \\ Queens' College \\ University of Cambridge \\ cjl94@cam.ac.uk}

\begin{document}
\maketitle

As any mathematician will testify, finding all solutions to a differential equation is a daunting task. One made much easier when working in groups. No, not a collection of people --- though that certainly would help --- rather, the algebraic structures.

Typically, solutions to differential equations depend on several independent variables, like the intial conditions. As such, the solution space is often massive. However, we can create equivalence classes, via a group action, reducing this space to a more manageable size. By choosing specific representatives from these classes, we can solve the differential equations more easily. Using our group action on our representatives, we then recover all solutions to the differential equations. Moreover, multiple group actions can be used in conjunction to reduce our space and simplify our task of solving these differential equations even further. 

While this technique is general, we apply it to the Nahm equations: a system of first-order, coupled, nonlinear differential equations. By focusing on this example, we can see the power of this technique firsthand. This article presents and elaborates on two of Andrew Dancer's papers~\cite{Dancer, DancerA}. 

Suppose we have $N$, a natural number, and $(a,b)$, an open interval of the real line. Let $T_0,T_1,T_2,T_3\colon (a,b)\rightarrow\mathfrak{u}(N)$ be analytic, where $\mathfrak{u}(N)$ is the set of $N\times N,$ skew-Hermitian, complex matrices. The \textbf{Nahm equations} are given by 
\begin{equation}
T_j'+[T_0,T_j]=[T_k,T_l],
\end{equation}
for all cyclic permutations $(j,k,l)$ of $(1,2,3)$, with the commutator $[A,B]:=AB-BA$. Let $\tau$ be the set of quadruples $(T_0, T_1, T_2, T_3)\equiv T$ of matrix valued functions satisfying the following:
\begin{enumerate}
\item $T_0$, $T_1$, $T_2$, and $T_3$ must be $\mathfrak{u}(N)$-valued and analytic;
\item $T_0$ is analytic on $[a,b]$ and $T_1$, $T_2$, and $T_3$ are analytic on $(a,b)$ with simple poles at $t=a,b$;
\item $T$ satisfies the Nahm equations;
\item the residues (from complex analysis) of $T_1$, $T_2$, and $T_3$ at $t=a,b$ form an irreducible representation of $\mathfrak{su}(2)$.
\end{enumerate}
Elements of $\tau$ are called \textbf{Nahm data}. 

The Nahm equations are obtained by imposing translation invariance in three dimensions on the four-dimensional, self-dual, Yang-Mills equations. Via the ADHMN procedure, Nahm data is used to create magnetic monopoles. 

\section{The Power of Group Work}

A system of coupled, nonlinear differential equations, the Nahm equations are extremely difficult to solve. However, we can define group actions that allow us to greatly simplify the form our solutions can take. This is done in the next section.
\begin{theorem}
Every element $(T_0, T_1, T_2, T_3) \in \tau$ is equivalent, via the actions of the gauge group and $\mathbb{R}^3$, to Nahm data of the form
\begin{equation}
T_0=0; \quad
T_1,T_2,T_3 \quad \mathfrak{su}(N)\mathrm{-valued},
\end{equation}
where $\mathfrak{su}(N)$ is the set of traceless matrices in $\mathfrak{u}(N)$. \end{theorem}

Before using group actions, we were tasked with finding $4N^2$ real, analytic functions. The above theorem ensures that we only have to find $3N^2-3$ such functions. In the $N=2$ case, this means we have to find nine of the original 16 functions. By using an additional action of SU(2) on the $N=2$ Nahm data, we can reduce this to three functions, though we do not cover that here~\cite{DancerA}.

The above theorem also has ramifications for the Nahm equations themselves. Indeed, given Nahm data of the form in the theorem, the Nahm equations become $T_j'=[T_k,T_l]$, for all cyclic permutations $(j,k,l)$ of $(1,2,3)$. Not only have we removed the need to solve for $N^2+3$ functions, we have also simplified our system of equations through our use of group actions.

For $T,\bar{T}\in\tau$, we say $T\sim \bar{T}$ if there are elements of $\mathbb{R}^3$ and the gauge group which act on $T$ to give $\bar{T}$. This is an equivalence relation. Our theorem tells us that for every $T\in\tau$, there is a $\bar{T}$ equivalent to $T$ of the desired form. If we solve the Nahm equations for $\bar{T}$, then we can use the group actions to recover our initial Nahm data. In fact, we can determine all Nahm data in that class through application of the group actions on $\bar{T}$. Hence, solutions of the form in the theorem generate all Nahm data, so we need only focus on them when solving the Nahm equations.

\section{Lights, Camera, Actions}

For $\lambda\in\mathbb{R}^3$ and $T\in\tau$, the action of $\mathbb{R}^3$ on $\tau$ is given by
\begin{equation}
\lambda.T:=(T_0,T_1-i\lambda_1I,T_2-i\lambda_2I,T_3-i\lambda_3I),
\end{equation}
where $I$ is the $N\times N$ identity matrix. We can confirm this is a group action by directly checking the definition. Note that the changes to the $j=1,2,3$ components do not affect the residues. 

Before we use our new action, we first prove a helpful feature of the Nahm data.
\begin{lemma}
For $T\in\tau$, $\mathrm{Tr}(T_j)$ is constant, for $j=1,2,3$.\label{lemma:Trace}
\end{lemma}
\begin{proof}
As the trace operator is linear, $\left(\mathrm{Tr}(T_j)\right)'=\mathrm{Tr}(T_j')$. Since $T\in\tau$, it satisfies the Nahm equations, so $T_j'=[T_k,T_l]-[T_0,T_j]$. Hence, we have to take the trace of commutators, which is zero. 
\end{proof}

We use this lemma together with our action to simplify the $j=1,2,3$ components of the Nahm data.
\begin{lemma}
Up to the action of $\mathbb{R}^3$, $T_j$ is $\mathfrak{su}(N)$-valued, for $j=1,2,3$. That is, there exists $\lambda\in\mathbb{R}^3$ such that for $j=1,2,3$, $(\lambda.T)_j$ is $\mathfrak{su}(N)$-valued.\label{lemma:RAction}
\end{lemma}
\begin{proof}
Suppose $T\in\tau$. Consider the function $\lambda_j:=\frac{1}{iN}\mathrm{Tr}(T_j)$, which is $\mathbb{R}$-valued, as $T_j$ is $\mathfrak{u}(N)$-valued. By Lemma~\ref{lemma:Trace}, $\lambda_j$ is constant, so $\lambda\in\mathbb{R}^3$. Hence, $\lambda.T\in\tau$. As the trace of $I$ is $N$, $\mathrm{Tr}((\lambda.T)_j)=0$, for $j=1,2,3$.
\end{proof}

Let $\mathcal{G}$ be the group of analytic functions $g\colon[a,b]\rightarrow\mathcal{U}(N)$, where $\mathcal{U}(N)$ is the set of $N\times N$, unitary, complex matrices. This is a group under pointwise matrix multiplication, called the \textbf{gauge group}. For $g\in\mathcal{G}$ and $T\in\tau$, the action of $\mathcal{G}$ on $\tau$ is given by
\begin{equation}
g.T:=(gT_0g^{-1}-g'g^{-1},gT_1g^{-1},gT_2g^{-1},gT_3g^{-1}),
\end{equation}
where we multiply pointwise. Again, we can confirm this is a group action by directly checking the definition. Note that as $g$ is analytic on $[a,b]$, we still have simple roots for the $j=1,2,3$ components. This action conjugates the residues at the endpoints, giving a new representation, equivalent to the original.

We use our gauge group action to simplify the zeroth component of the Nahm data.
\begin{lemma}
Up to the action of $\mathcal{G}$, $T_0=0$. \label{lemma:GAction}
\end{lemma}
\begin{proof}
Suppose $T\in\tau$. We want an analytic, $\mathcal{U}(N)$-vaued function $g$ satisfying $g'=gT_0$. As $T_0$ is analytic and $\mathfrak{u}(N)$-valued, such a function exists, for instance, $g(t)=\mathrm{exp}\left(\int_a^t T_0(\bar{t})d\bar{t}\right)$. By the definition of the gauge group, $g\in\mathcal{G}$. We know $g.T\in\tau$ and, by construction, $(g.T)_0=0$.
\end{proof}

Using our actions, we now prove the theorem. That is, we show that there are $\lambda\in\mathbb{R}^3$ and $g\in\mathcal{G}$ such that $\lambda.(g.T)\in\tau$ possesses the desired properties: $(\lambda.(g.T))_0=0$ and $(\lambda.(g.T))_j$ is $\mathfrak{su}(N)$-valued for $j=1,2,3$.
\begin{proof}[Proof of Theorem]
Suppose $T\in\tau$. By Lemma~\ref{lemma:GAction}, there is a $g\in\mathcal{G}$ such that $g.T\in\tau$ satisfies $(g.T)_0=0$. Furthermore, by Lemma~\ref{lemma:RAction}, there is a $\lambda\in\mathbb{R}^3$ such that $(\lambda.(g.T))_j$ is $\mathfrak{su}(N)$-valued, for $j=1,2,3$. Finally, the action of $\mathbb{R}^3$ does not affect the zeroth component of $g.T$, so $(\lambda.(g.T))_0=0$.
\end{proof}

\begin{thebibliography}{50}
\bibitem[1]{Dancer} Dancer, A. Nahm data and SU(3) monopoles. \textit{Nonlinearity 5}, 6 (1992).

\bibitem[2]{DancerA} Dancer, A. Nahms equations and hyperk\"{a}hler geometry. \textit{Comm. Math. Phys. 158}, 3 (1993), 545568.
\end{thebibliography}

\end{document}