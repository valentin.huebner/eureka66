\documentclass[reqno,12pt]{amsart}
\usepackage{amssymb, verbatim}
\usepackage{graphicx, enumitem}
\usepackage[foot]{amsaddr}
\setlength{\topmargin}{-.5in}
\setlength{\textheight}{9in}
\setlength{\textwidth}{6in}

\newtheorem{bigthm}{Theorem}
\newtheorem{thm}{Theorem}[section]
\newtheorem{lem}[thm]{Lemma}
\newtheorem{cor}[thm]{Corollary}
\newtheorem{prop}[thm]{Proposition}

\theoremstyle{remark}
\newtheorem{rem}[thm]{Remark}

\title{A Four Player Game of Intransitive Dice}

\begin{document}

\author[J. Collins]{Jared T. Collins} 
\author[I. Harris]{Isabel E. Harris}

\address[J. Collins]{Department of Mathematics and Computer Science\\
  Freed-Hardeman University\\Henderson, TN 38340}
  
\email{jtcollins@fhu.edu}
\email{ieh0005@auburn.edu}


\begin{abstract}
We produce a set of nineteen intransitive dice satisfying that for any subset of three dice there exists a die which will roll higher than each of them with probability greater than one half. Moreover we do so using nine-sided dice which is currently thought to be the least possible and allows this game to be physically created. \bigskip

\textit{2010 Mathematics Subject Classification}: 00A08, 91A60, 60C05 \medskip

\textit{Keywords:} Intransitive Dice, NonTransitive Dice.

\end{abstract}

\date{\today}

\maketitle

\section{Politely Cheating}

Transitivity is one of our most prevalent mathematical properties: if $A>B$ and $B>C$ then $A>C$. Unfortunately transitivity is a terrible property for games. Imagine a transitive variant of Paper-Rock-Scissors. Paper beats Rock and Rock beats Scissors so Paper beats Scissors too. Paper is king in this game, and you would be foolish to pick either of the other items.

No, we want transitivity far away from the mechanics of our games. Not to say we want it completely removed: scores should still rank players when needed. But you will be hard pressed to find people willing to play a game in which one specific move guarantees a win. So instead of guaranteeing a win, can we just tip the odds in our favor a little bit?

Consider the following game. We both roll a standard six-sided die and the higher number wins. Seems pretty boring, but I bet you have played it before to decide who goes first in some other game. How about we play a more complicated game? Instead of standard six-sided dice, we will use a special set of dice:
\begin{center} 
\includegraphics[width=.7in]{147147} \hspace{1in}
\includegraphics[width=.7in]{255255} \hspace{1in}
\includegraphics[width=.7in]{336336}
\end{center}

A casual look at these dice suggests another boring game, but at least there will not be any ties. The dice all have an expected value of $4$ and the dice often repeat. But if it were a boring game we would not be talking about it. A closer look shows that die 3 beats die 2 five out of nine times, die 2 beats die 1 five out of nine times and die 1 beats die 3 five out of nine times. 

\begin{center}
\includegraphics[width=2in]{3d3}
\end{center}
\begin{center}
\includegraphics[width=5in]{d3explicit}
\end{center}

So we have an intransitive system much like Paper-Rock-Scissors but with some random chance thrown in to make us look less like cheaters. But, to make our scheme complete we will need to ensure we always pick our die last. That is why we are polite cheaters: always allowing our opponent to pick their die first.

\section{Double the Fun}

There are several ways we can improve the game we have just described. Our winning probability of $0.\overline{5}$ while better than $0.5$ could be higher and there are dice sets that do so, although it is impossible to surpass $0.75$ [6]. We could try to use fewer dice or reduce the number of sides on each die, but three dice is the best we can do in a two player game and our dice are already effectively three-sided. Keep these three kinds of improvements in mind: higher probability, fewer dice and fewer sides. 

We, however, are concerned with a different kind of improvement. Rather than play with just one friend, could we play with more and cheat two people at once. It is not quite the same as playing a single person twice since the probabilities against two opponents are not independent. But it should be clear that our set of three dice is not up to the task. 

Can we find a set of dice such that for any pair of dice there is another die in the set that beats each of them with probability greater than one half? This is a special case of the Sch{\"u}tte Tournament Problem [4] which asks us to find a complete directed graph with $n$ vertices such that for any subset of $k$ vertices there is at least one vertex with edges to each of the $k$ vertices and to do so with the smallest value of $n$. In our discussion the direction of the edge is equivalent to one die beating another with probability greater than one half.

Such a set of dice exists. In 1992 Oskar van Deventer [7] described a set of seven three-sided dice that realize a Sh{\"u}tte Tournament with $k=2$. The dice beat one another with a probability of $0.\overline{5}$ just like our first example.

\begin{center}
\includegraphics[width=2.5in]{7d3}
\end{center}

\section{Going Bigger}

Winning against two friends at a time is great. But we can do better. Inspired by Deventer's dice and with the knowledge of how many dice are needed for the Sch{\"u}tte Tournament with $k=3$ we set to work trying to create a set of dice with a small enough number of sides so that we could physically create them. At the time we believed the best set had dice with 342 sides [5], not something you would be able to hold in your hands. We got to work and made a set with 9 sides. 

Of course, physically making a die with 9 sides is a bit of a challenge. So much like how Deventer disguised a set of three-sided dice by duplicating each side to create a six-sided die, we will need a trick to make our dice more standard. The easiest way is for us to embed each nine-sided die into a ten-sided die by letting the tenth side be blank or giving it a symbol that instructs the player to re-roll the die. So here they are:

\begin{center}
\includegraphics[width=6in]{19d9}
\end{center}

As you can imagine in a complete digraph with nineteen vertices it gets a little difficult to see the direction of each arrow. So we have simplified the picture to focus on one die and the dice that it beats. If you are curious to know what dice any specific die beats you can determine that utilizing the rotational symmetry of the graph or by utilizing the quadratic residues of 19. The probability associated with one of these dice beating another is $\displaystyle \frac{41}{81}$ That is not quite as strong as the two and three player games, but it is still in your favor. The figure below shows the comparisons between die 10 and die 14. The figure adjacent to it shows a more general picture for any pair of these nine-sided dice. In each comparison the main diagonal will split into 5 wins and 4 losses, though the pattern of that split will vary.
\begin{center}
\includegraphics[width=6in]{d19explicit}
\end{center}

\section{Can We Do Better?}

Think back to our three means of improving the game: higher probability, fewer dice and fewer sides. Add to those our fourth way to improve the game: more players. Higher probability is an area where there is plenty of room for improvement. $\frac{41}{81}$ is very close to $\frac{1}{2}$ and we know that the upper bound on possible probabilities is $\frac{3}{4}$. Note that if we were willing to relax our condition of uniform probabilities this gets far easier to achieve.

Fewer dice is an area that we cannot improve without adjusting the rules of the game. It is known that the Sch{\"u}tte Tournament problem with $k=3$ requires $n\geq19$. However James Grimes [7] has had success in using fewer distinct dice in the three player game. His method does require you to have two of each die and the rules of the game are a little different but a similar technique could lower the number of unique dice needed for the four player game.

Fewer sides is for us the most compelling avenue of improvement. Independently of our work Dezs{\"o} Bednay and S{\'a}ndor Boz{\'o}ki [2], [3] also have created a set of nine-sided dice. They are working on the much larger problem of realizing any digraph with a set of intransitive dice. Levi Angel and Matt Davis [1] also have accomplished some great work in creating intransitive dice sets for digraphs. This area is most interesting to us because any decrease in sides would necessarily bring an increase in probability.

Finally the issue of more players is interesting but is tied up in the Sch{\"u}tte Tournament Problem [4]. All of the games described in this paper are examples of Paley tournaments [3]. If we believe that the optimal Sch{\"u}tte tournaments are all Paley tournaments then a five player game would require 67 dice, but there may be a way to do it with fewer dice if we do not require our set to form a Paley tournament.

\section{References}

\begin{enumerate}[label={[\arabic*]}, noitemsep]
\item L. Angel and M. Davis \emph{A direct construction of nontransitive dice sets}, J of Combinatorial Designs 25(11)(2017), 523-529.
\item D. Bednay and S. Boz{\'o}ki, \emph{Constructions for nontransitive dice sets}, in Proceedings of the 8th Japanese-Hungarian Symposium on Discrete Mathematics and its Applications, Veszpr{\'e}m, Hungary, 2013, 15-23.
\item S. Boz{\'o}ki, \emph{Nontransitive dice sets realizing Paley tournaments for solving Sch{\"u}tte's tournament problem}, Miskolc Math Notes 15 (2014), 39-50.
\item E. Szekeres and G. Szekeres, \emph{On a problem of Sch{\"u}tte and Erd{\"o}s}, The Mathematical Gazette 49(369)(1965), 290-293. 
\item cp4space.wordpress.com/2013/02/15/tournament-dice
\item math.stackexchange.com/questions/57338
\item singingbanana.com/dice/article.htm





\end{enumerate}

\end{document} 