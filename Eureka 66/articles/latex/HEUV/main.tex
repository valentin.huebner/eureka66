\documentclass[9.5pt,headsepline,cleardoubleempty,bibtotoc,tablecaptionabove,english,twoside]{scrartcl}


\usepackage[utf8]{inputenc}
\usepackage{multicol}
\usepackage{xcolor}
\usepackage{fontspec}

\setlength{\columnsep}{7mm}
\setlength{\parskip}{3mm}

\newfontfamily\fontheading[SizeFeatures={Size=48}]{MyriadPro-Bold}
\newfontfamily\fontauthor[SizeFeatures={Size=24}]{MyriadPro-Light}
\newfontfamily\fontpagenr[SizeFeatures={Size=14}]{Myriad Pro}
\newfontfamily\fontsubheading[SizeFeatures={Size=16}]{MyriadPro-Bold}
\newfontfamily\fontcapt[SizeFeatures={Size=10}]{MyriadPro-Light}
\newfontfamily\fontfignr[SizeFeatures={Size=10}]{Myriad Pro}

\definecolor{headings}{HTML}{ec008c}
% violett #c02555
% dunkelblau #2b367c
% sehr dunkles Grau #231f20
% hellblau #00adbd
% orange #f99d1c
% lila #f99d1c
% grün #62bb46
% pink #ec008c


% \usepackage{mathabx}
\usepackage{libertine}
\usepackage{babel}

\addto{\captionsenglish}{%
  \renewcommand{\refname}{}
}


\usepackage{geometry}
\geometry{a4paper, margin=21mm}

\makeatletter
\newcommand*{\shifttext}[2]{%
  \settowidth{\@tempdima}{#2}%
  \makebox[\@tempdima]{\hspace*{#1}#2}%
}
\makeatother
\setlength{\parindent}{0pt}% Just for this example


\usepackage{fancyhdr}

\renewcommand{\headrulewidth}{0pt}
% \lhead{\shifttext{-15mm}{\fontpagenr\thepage}}
\cfoot{}
\pagestyle{fancy}

% \linespread{1.3}
% \setlength{\parskip}{\baselineskip}%
% \setlength{\parindent}{0pt}%
%generally needs more spacing for legibility

%\usepackage[onehalfspacing]{setspace}%
\title{Smooth Piecewise Polynomials}
\author{Josef Greilhuber}
\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{stmaryrd}
\usepackage{svg}
\usepackage{array}
\usepackage[document]{ragged2e}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{calc}

\DeclareMathOperator{\supp}{supp}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\card}{card}
\DeclareMathOperator{\Proj}{Proj}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\Koh}{H}
\DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\coker}{coker}
\DeclareMathOperator{\cl}{cl}
\DeclareMathOperator{\interior}{int}
\DeclareMathOperator{\fr}{fr}
\DeclareMathOperator{\bd}{bd}
\DeclareMathOperator{\Ind}{Ind}
\DeclareMathOperator{\Obj}{Obj}
\DeclareMathOperator{\Ext}{Ext}
\DeclareMathOperator{\PDO}{PDO}
\DeclareMathOperator{\tr}{tr}
\DeclareMathOperator{\Str}{Str}
\DeclareMathOperator{\cosch}{cosech}
\DeclareMathOperator{\cosech}{cosech}
\DeclareMathOperator{\ind}{ind}
\DeclareMathOperator{\Cl}{Cl}
\DeclareMathOperator{\ch}{ch}
\DeclareMathOperator{\ad}{ad}
\DeclareMathOperator{\Spin}{Spin}
\DeclareMathOperator{\End}{End}
\DeclareMathOperator{\gr}{gr}
\DeclareMathOperator{\Aut}{Aut}
\DeclareMathOperator{\GL}{GL}
\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\Spec}{Spec}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\VR}{VR}

\usepackage{thmtools}
%\newtheorem{stz}{Satz}
\newtheorem{cor}{Corollary}
%\newtheorem{lem}{Lemma}
\newtheorem{exa}{Example}
\newtheorem{thm}{Theorem}
\newtheorem{defi}{Definition}
%\theoremstyle{definition}
\newtheorem{rem}{Remark}
%\newtheorem*{bem}{Bemerkung}
%\newtheorem{bsp}{Beispiel}
\newtheorem{prop}{Proposition}
\newtheorem{lem}{Lemma}

\numberwithin{equation}{section}
% Datum der Abgabe
\setmainfont[BoldFont={Minion Pro Bold}]{Minion Pro}

\newcommand{\subheading}[1]{
\vspace{8mm}
{\fontsubheading\textcolor{headings}{#1}}\vspace{2mm}

}

\begin{document}

{\fontheading\textcolor{headings}{\foreignlanguage{nohyphenation}{\begin{sloppypar}
\baselineskip=48pt
  Topological Data Analysis
\end{sloppypar}}}}

\vspace{5mm}

{\fontauthor\textcolor{headings}{
  Simon Heuveline % University of Durham
}}

\vspace{30mm}


\justify

\begin{multicols}{2}

\textit{Mathematics is the art of giving the same name to different things.}\vspace{-1.5em}
\begin{flushright}--- Henri Poincaré\end{flushright}

  \subheading{Algebraic Topology}

This section gives a very informal introduction to the subject of algebraic topology. The goal of algebraic topology is to classify spaces by their global structure. For this aim, we have to define what it means for two spaces to have the same global structure. We will focus on the very intuitive notion of homeomorphic spaces. We say that two spaces $M_1$ and $M_2$ are homeomorphic (written $M_1 \cong M_2$) if they can be continuously deformed into each other, i.e. there is a continuous bijection $\phi: M_1\rightarrow M_2$ that has a continuous inverse. The well-known standard example is that a doughnut can be continuously deformed into a coffee mug and hence they are homeomorphic. This deformation can be seen in Figure 1.


In a certain sense, it is an easy task to prove that two spaces are homeomorphic since we 'just' have to write down a homeomorphism between them. It is a much harder task to prove that two spaces are non-homeomorphic since we would have to 'try all possible homeomorphisms' which is, of course, a nonsensical idea. Algebraic topology gives a way to tackle the latter task in a conceptually new way: We assign an algebraic object (e.g.\,a number, group, ring, etc.) to each space. %not clear what you mean by this parenthesis: maybe just remove it
Moreover, we demand that homeomorphic spaces get assigned with the same (at least up to some notion of isomorphism) algebraic object. Such an assignment is called a (homeomorphism-)invariant. If one has properly defined such an invariant, one can compute the invariants of two given spaces and compare them. If they are not the same, we have proved that the two spaces are not homeomorphic. Observations like this are the starting point of classification results for certain spaces. To give an intuitive example of such an invariant, the 'the genus of a surface' we can consider the assignment \begin{align*}
\beta_1:\{2\textrm{-dimensional surfaces}\}&\rightarrow \mathbb{N}\\
M &\mapsto \#\{\textrm{Holes of }M\}.
\end{align*}

If we can continuously deform a surface into another one (think about squeezing and stretching it, as in Figure 1), we will not change the number of \linebreak


\vspace{2em}

\begin{centering}
${\textcolor{headings}{\blacktriangledown}}$\enspace{\fontcapt {\fontfignr Figure 1:} A homeomorphism from a \\doughnut to a coffee mug.}\\[1em]
\hspace{2.3em}\includegraphics[width=0.9\linewidth]{TDA_Coffeemug}
\end{centering}
\vspace{-30em}
% \pagebreak

\end{multicols}

\begin{flushright}
\includegraphics[width=0.6\linewidth]{TDA_Brezel} \\

\end{flushright}

\vspace{-90mm}
\qquad\qquad\qquad\qquad\qquad{\fontcapt {\fontfignr Figure 2:} A German pretzel.\enspace${\textcolor{headings}{\blacktriangleright}}$}

\vspace{80mm}

\begin{multicols}{2}

holes, which makes $\beta_1$ a homeomorphism invariant.
 In this way, we could 'prove' that a doughnut is not
homeomorphic to a pretzel (see Figure 2), because
$\beta_1(\textrm{Pretzel})=3 \neq 1= \beta_1(\textrm{Doughnut}).$

This is, of course, not a very interesting result. However, once we formalise the steps of the heuristic discussion above, we can also define a notion of holes and higher dimensional holes of higher dimensional spaces for which we have a priori no geometric intuition, so that the question of whether two spaces are homeomorphic may be highly nontrivial. This (and much more) is done in the framework of so-called homology and cohomology theories. The important invariants in homology, the 'homology groups' (for us these will be in fact $\mathbb{Q}$-vector spaces) $H_k(X)$ can be formally defined for a general topological space $X$. However, for us, it will be enough to know that $\beta_k(X)=\dim_{\mathbb{Q}}(H_k(X))$, which is called the $k$-th Betti-number of $X$, counts the number of '$k$-dimensional holes of $X$' in a certain sense and generalises the above intuitive notion of holes. An example of a 'one-dimensional hole' would be the hole in a circle, while an example of a 'two-dimensional hole' would be the hole in a $2$-dimensional sphere (think about a Kinder egg, with the 'hole' being the interior of the egg).

Even though algebraic topology is a very deep mathematical theory and applies to several inner-mathematical disciplines, it seems rather abstract and not suitable to be applied to 'real-world problems'. However, with time topology started to become a topic of interest in theoretical physics. There, it appears on a fundamental level in Quantum field theory and String theory but also in statistical physics. For instance, the genus (as discussed above) of a string worldsheet gives us the order in a perturbative expansions of string interaction amplitudes in analogy to the number of loops in the context of ordinary Feynman diagrams. Topology is actually a key discipline in the interplay between mathematics and physics since physical intuition turned out to be useful for proving mathematical results about certain topological spaces (e.g.\,many results about 3 and 4 dimensional manifolds or the local Atiyah-Singer Index Theorem). A rather new and quite surprising development on which I want to focus in this article is the application of algebraic topology to computer science.

\subheading{Topological Data Analysis}

In data science, one is faced with the problem of extracting nontrivial information from datasets, which can often be represented by point clouds (i.e. finite subsets) in high-dimensional vector spaces. Such datasets may arise from an array of sensor readings in an engineering testbed, from questionnaire responses in a psychology experiment, or from population sizes in an ecosystem. At first glance, it does not seem like we can extract any interesting information from computing the homology of a discrete set of points. All sets of $n$ points (with the discrete\linebreak

\vspace{-1.9em}
${\textcolor{headings}{\blacktriangledown}}$\enspace{\fontcapt {\fontfignr Figure 3:} A point cloud with a hole in it.}

\begin{centering}
\hspace{1em}\includegraphics[width=0.9\linewidth]{TDA_S1}
\end{centering}
\vspace{5.1em}


 topology) are topologically indistinguishable, so calculating the homology of this will count the number of points and give us no more information. In particular, it cannot detect any holes.

On the other hand, if we look at the example of a point cloud in Figure 3, we intuitively see that there is a 'hole' and in particular that some nontrivial topology is at play.


Without going into any concrete example, it is also quite clear that this hole that we see intuitively is a piece of interesting information about the given dataset that we would like to extract. Now, we need to formalise what we mean by this hole so that we can generalise it to higher dimensions and eventually get some information about given data sets in arbitrarily high dimensional spaces. There is a computationally feasible approach to do this, which is known as 'persistent homology'.


\subheading{Persistent Homology}

As we saw, it does not make sense to compute the homology of the given point cloud as it is. Our intuition tells us that we should draw lines between all the points that are close to each other, then fill out all the little triangles and so on to get a simplicial complex. We do not need the formal definition of a simplicial complex for our intuitive approach here, so just think about lines, triangles, tetrahedra and further generalised triangles glued to each other along their faces. It is possible to compute the homology of such a simplicial complex on a computer, so computing the homology of the emerging simplicial complex will give us the interesting information: Our data set has one one-dimensional hole. This intuition is perfectly right, but comes with one big problem: At which length scale should we stop drawing lines and triangles? The cutoff should be large enough such that all the 'noise' on a small scale closes. But if it is too large, we draw lines and triangles between all points such that the interesting hole closes and computing the homology will not give us any interesting information. %maybe this is obvious, but maybe you should stress the idea of an 'ideal point' here: the cutoff also has to be large enough that the holes actually 'close', so it could be rephrased as 'it needs to be large enough... but if it's too large...'
So we have to find this cutoff dynamically. We will now sketch the idea of how to do this.

Given a point cloud $D\subset \mathbb{R}^n$ (which is just a finite metric space with the induced metric of $\mathbb{R}^n$) we imagine to draw balls of radius $\epsilon\geq 0$ around each point and draw a line between two points $x,y \in D$ if $B_{\epsilon}(x)\cap B_{\epsilon}(y)\neq \emptyset$. We draw triangles between three points if the balls around any two of these points intersect each other and we proceed in the same way for higher dimensions. The resulting simplicial complex is known as the Vietoris-Rips-Complex of $D$ at scale $\epsilon$, written $\VR_{\epsilon}(D)$. It can formally be defined as the abstract simplicial complex \begin{align*}
\VR_{\epsilon}(D):=\{\sigma \subset D: \|x-y\|\leq 2 \epsilon \textrm{ for all } x,y \in \sigma \}.
\end{align*}
The following example in Figure 4 shows the Vietoris-Rips-Complex of an annulus-shaped point cloud $D$ for different values of $\epsilon$ (Here, $D$ stands for doughnut and not data).



Observe, that even though $\epsilon \geq 0$ may be varied continuously, the Vietoris-Rips-Complex only changes at discrete $\epsilon$-values $\epsilon_1<\dots <\epsilon_n$ since there are only finitely many points in $D$. So we arrive at a filtered simplicial complex
\begin{multline*}
\VR_0(D) \hookrightarrow \VR_{\epsilon_1}(D)\hookrightarrow  \\ \hookrightarrow \VR_{\epsilon_2}(D)\hookrightarrow  \dots \hookrightarrow \VR_{\epsilon_n}(D),
\end{multline*}
where we can compute the homology at each filtration step to get
\begin{multline*}
H_{*}(\VR_0(D)) \rightarrow H_{*}(\VR_{\epsilon_1}(D)) \rightarrow \\ \rightarrow H_{*}(\VR_{\epsilon_2}(D)) \dots \rightarrow H_{*}(\VR_{\epsilon_n}(D)).
\end{multline*}


\subheading{The mathematical structure of TDA}

We saw above, that for each $i\leq j$ we have a natural inclusion of simplicial complexes $\VR_{\epsilon_i}(D) \hookrightarrow \VR_{\epsilon_j}(D)$, which induces a natural map $H_*(\VR_{\epsilon_i}(D)) \rightarrow H_*(\VR_{\epsilon_j}(D))$ by the functoriality of homology.\pagebreak

\end{multicols}


\begin{centering}
\includegraphics[width=\linewidth]{TDA_VR2} \\
\vspace{-0.2cm}
\begin{center}${\textcolor{headings}{\blacktriangle}}$\enspace{\fontcapt {\fontfignr Figure 4:} $\VR_\epsilon(D)$ for six different, increasing $\epsilon$-values.}\end{center}
\end{centering}


\vspace{3.9em}

\begin{multicols}{2}

So we have a functor from the partially ordered set $\{\{0,\dots,n\},\leq)$ (considered as a category) to the category of $R$-modules (in this article we only consider the case $R=\mathbb{Q}$). This motivates the following definition:

\begin{defi}
A persistence module over a partially ordered set $(P, \leq)$ with coefficients in a commutative ring $R$ is a functor from $(P,\leq)$ considered as a category to the category of $R$-modules.
\end{defi}

This more abstract approach gives rise to surprising applications of persistent homology to inner-mathematical disciplines such as symplectic topology (see e.g.\,\cite{UZ15}). In these applications, the concept of a barcode is crucial as well. We will discuss the intuition behind these in the following section.

\subheading{Barcodes}
It is common to depict the homology groups $H_k(\VR_{\epsilon}(D))$ in a so-called barcode. In Figure 6 we can see the barcode of the above example from figure Figure 4. Staying close to this example, we would like to explain how this barcode is constructed and which information is encoded in it.



The barcode represents the homology classes as horizontal lines that 'are born' at some value $\epsilon_1$ and 'die' at some other value $\epsilon_2>\epsilon_1$. We use different colours for different values of $k\in \mathbb{N}$. The difference $\epsilon_2-\epsilon_1$ is called the 'lifetime' of a homology class. This barcode finally gives us all of the qualitative information we were looking for. The homology classes with a long lifetime can be understood to be 'holes on a reasonable scale' while the nontrivial homology classes that have a short lifetime (compared to the class with the longest lifetime) can be seen as noise in the data. The 'important' classes with a long lifetime can be interpreted to persist over a long parameter range, which justifies the name 'persistent homology'. In Figure 6, the longest horizontal green line represents such a persistent homology class. This persistent homology class, in turn, represents the hole in our doughnut-shaped data set $D$. The other relatively long green lines are seen because our original data set is still quite far away from an ideal doughnut. If we would compute the barcode of Figure 3 we would see one very long green line and other very short green lines for small $\epsilon$-values.

To put it in a nutshell, we can extract information from the barcode by drawing a vertical line at fixed $\epsilon \geq 0$ (the blue dashed lines in Figure 6). Then we can count the intersection points with the green lines and get the number of holes of $\VR_{\epsilon}(D)$ or in other words $\beta_1(\VR_{\epsilon}(D))$. Counting the number of intersections of a blue line with the red lines gives us the number of connected components of $\VR_{\epsilon}(D)$ or in other words $\beta_0(\VR_{\epsilon}(D))$. The longest horizontal lines represent the 'important' topological features of the dataset.

Now, where the formalism is operational, we should argue that the above example does not demonstrate the full power of persistent homology. We only observed a $1$-dimensional hole. In principle, we can compute holes of arbitrarily high dimension simultaneously and compare their length scales. Think for example of a data set $D \subset \mathbb{R}^3$ which approximates a $2$-sphere with a small disk removed that we call$M$ (see Figure 5). Here, $D$ stands for Death star not for Data. Here we would see no non-trivial homology for a large range of $\epsilon$ (since a sphere with a 'hole' cut out is contractible), until for large enough $\epsilon$ we obtain non-trivial second homology, suggesting indeed that our space looks like a $2$-sphere with a '$1$-dimensional hole'.
\columnbreak

\vspace{1em}
\begin{centering}
\includegraphics[width=1\linewidth]{TDA_Deathstar} \\
\vspace{0.5em}
${\textcolor{headings}{\blacktriangle}}$\enspace{\fontcapt {\fontfignr Figure 5:} Roughly what $M=S^2\setminus D^2_{\epsilon}$ looks like.}
\end{centering}
\vspace{1em}


\vspace*{0.5cm}

\subheading{References}

\vspace*{-2cm}

\bibliographystyle{alpha}
\begin{thebibliography}{\enspace}

	\addcontentsline{toc}{section}{References}


\bibitem[1]{CA14}
	Carlson G.: {\em Topological pattern recognition for point cloud data}.
	Acta Numerica, vol. \textbf{23}, 289--368. 2014.

\bibitem[2]{GH08}
	Ghrist R.: {\em Barcodes: The persistent topology of data}.
	Bull. Amer. Math. Soc., vol. \textbf{45}, 61--75. 2008.

	\bibitem[3]{Hat02}
	Hatcher A.: {\em Algebraic Topology}.
	Cambridge University Press 2002.

\bibitem[4]{UZ15}
	Usher M., Zhang J.: {\em Persistent homology and Floer-Novikov theory}.
	arXiv:1502.07928. 2015.


\end{thebibliography}

\end{multicols}


\vspace{3em}
\begin{center}
${\textcolor{headings}{\blacktriangledown}}$\enspace{\fontcapt {\fontfignr Figure 6:} The barcode of the dataset $D$.}
\vspace*{0.7cm}
\includegraphics[width=0.8\linewidth]{TDA_Barcode}
\end{center}
\vspace*{-1em}

\end{document}
