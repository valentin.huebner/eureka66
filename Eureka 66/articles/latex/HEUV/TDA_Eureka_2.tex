\documentclass[11pt]{article}

\makeatletter

\newenvironment{chapquote}[2][2em]
  {\setlength{\@tempdima}{#1}%
   \def\chapquote@author{#2}%
   \parshape 1 \@tempdima \dimexpr\textwidth-2\@tempdima\relax%
   \itshape}
  {\par\normalfont\hfill--\ \chapquote@author\hspace*{\@tempdima}\par\bigskip}
\makeatother


\usepackage[english]{babel}
\usepackage[a4paper, top=3.5cm, bottom=5cm]{geometry}
\usepackage{microtype}

\usepackage{fancyhdr}
\fancypagestyle{style1}{
  \lhead{}
  \chead{\rightmark}
  \rhead{}
  \lfoot{}
  \rfoot{}
  \cfoot{\thepage}
}
\fancypagestyle{style2}{
  \lhead{}
  \chead{Appendix}
  \rhead{}
  \lfoot{}
  \rfoot{}
  \cfoot{\thepage}
}

\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{amsopn}
\usepackage{mathrsfs}
\usepackage{mathtools}
\usepackage{stmaryrd}
\usepackage[T1]{fontenc}
\usepackage{lipsum}
\usepackage{graphicx}
\graphicspath{ {/home/user/images/} }
\usepackage{subcaption}

\usepackage{tikz}
  \usetikzlibrary{matrix}
  \usetikzlibrary{fit}
  \usetikzlibrary{backgrounds}
  \usetikzlibrary{arrows}
  \usetikzlibrary{shapes}
  \usetikzlibrary{decorations.pathmorphing}
\usepackage{tikz-cd}

\newtheoremstyle{mystyle}
	{}
	{}
	{\itshape}
	{ }
	{\bfseries}
	{.}
	{ }
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}

\DeclareMathOperator{\supp}{supp}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\card}{card}
\DeclareMathOperator{\Proj}{Proj}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\vspan}{span}
\DeclareMathOperator{\Koh}{H}
\DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\coker}{coker}
\DeclareMathOperator{\cl}{cl}
\DeclareMathOperator{\interior}{int}
\DeclareMathOperator{\fr}{fr}
\DeclareMathOperator{\bd}{bd}
\DeclareMathOperator{\Ind}{Ind}
\DeclareMathOperator{\Obj}{Obj}
\DeclareMathOperator{\Ext}{Ext}
\DeclareMathOperator{\PDO}{PDO}
\DeclareMathOperator{\tr}{tr}
\DeclareMathOperator{\Str}{Str}
\DeclareMathOperator{\cosch}{cosech}
\DeclareMathOperator{\cosech}{cosech}
\DeclareMathOperator{\ind}{ind}
\DeclareMathOperator{\Cl}{Cl}
\DeclareMathOperator{\ch}{ch}
\DeclareMathOperator{\ad}{ad}
\DeclareMathOperator{\Spin}{Spin}
\DeclareMathOperator{\End}{End}
\DeclareMathOperator{\gr}{gr}
\DeclareMathOperator{\Aut}{Aut}
\DeclareMathOperator{\GL}{GL}
\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\Spec}{Spec}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\VR}{VR}

\newcommand{\RN}[1]{\uppercase\expandafter{\romannumeral#1}}

\begin{document}

\pagestyle{style1}

\theoremstyle{mystyle}
\newtheorem{theorem}{Theorem}
\newtheorem{cor}[theorem]{Corollary}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{prop}[theorem]{Proposition}

\theoremstyle{remark}
\newtheorem{rem}[theorem]{Remark}

\theoremstyle{definition}
\newtheorem{exs}[theorem]{Examples}
\newtheorem{defi}[theorem]{Definition}
\newtheorem{ex}[theorem]{Example}

\numberwithin{theorem}{section}



% cover sheet

\iffalse

\fi





\section{Topological Data Analysis - Simon Heuveline}


\begin{chapquote}{Henri Poincaré}
Mathematics is the art of giving the same name to different things.
\end{chapquote}

\subsection{Algebraic Topology}

This section gives a very informal introduction to the subject of algebraic topology. The goal of algebraic topology is to classify spaces by their global structure. For this aim, we have to define what it means for two spaces to have the same global structure. We will focus on the very intuitive notion of homeomorphic spaces. We say that two spaces $M_1$ and $M_2$ are homeomorphic (written $M_1 \cong M_2$) if they can be continuously deformed into each other, i.e. there is a continuous bijection $\phi: M_1\rightarrow M_2$ that has a continuous inverse. The well-known standard example is that a doughnut can be continuously deformed into a coffee mug and hence they are homeomorphic. This deformation can be seen in figure \ref{fig:Coffeemug}.





\begin{figure}[h]
\centering
\includegraphics[width=0.4\linewidth]{TDA_Coffeemug}

    \label{fig:Coffeemug}
    \caption{A homeomorphism from a doughnut to a coffee mug}
\end{figure}

In a certain sense, it is an easy task to prove that two spaces are homeomorphic since we 'just' have to write down a homeomorphism between them. It is a much harder task to prove that two spaces are non-homeomorphic since we would have to 'try all possible homeomorphisms' which is, of course, a nonsensical idea. Algebraic topology gives a way to tackle the latter task in a conceptually new way: We assign an algebraic object (e.g. a number, group, ring, etc.) to each space. %not clear what you mean by this parenthesis: maybe just remove it
Moreover, we demand that homeomorphic spaces get assigned with the same (at least up to some notion of isomorphism) algebraic object. Such an assignment is called a (homeomorphism-)invariant. If one has properly defined such an invariant, one can compute the invariants of two given spaces and compare them. If they are not the same, we have proved that the two spaces are not homeomorphic. Observations like this are the starting point of classification results for certain spaces. To give an intuitive example of such an invariant, the 'the genus of a surface' we can consider the assignment \begin{align*}
\beta_1:\{2\textrm{-dimensional surfaces}\}&\rightarrow \mathbb{N}\\
M &\mapsto \#\{\textrm{Holes of }M\}.
\end{align*}
If we can continuously deform a surface into another one (think about squeezing and stretching it, as in figure \ref{fig:Coffeemug}), we will not change the number of holes, which makes $\beta_1$ a homeomorphism-invariant. In this way, we could 'prove' that a doughnut is not homeomorphic to a pretzel (see figure \ref{fig:pretzel}), because
$\beta_1(\textrm{Pretzel})=3 \neq 1= \beta_1(\textrm{Doughnut}).$


\begin{figure}[h]
\centering
\includegraphics[width=0.4\linewidth]{TDA_Brezel}
  \caption{a German Pretzel}
    \label{fig:pretzel}
\end{figure}

This is, of course, not a very interesting result. However, once we formalise the steps of the heuristic discussion above, we can also define a notion of holes and higher dimensional holes of higher dimensional spaces for which we have a priori no geometric intuition, so that the question of whether two spaces are homeomorphic may be highly nontrivial. This (and much more) is done in the framework of so-called homology and cohomology theories. The important invariants in homology, the 'homology groups' (for us these will be in fact $\mathbb{Q}$-vector spaces) $H_k(X)$ can be formally defined for a general topological space $X$. However, for us, it will be enough to know that $\beta_k(X)=\dim_{\mathbb{Q}}(H_k(X))$, which is called the $k$-th Betti-number of $X$, counts the number of '$k$-dimensional holes of $X$' in a certain sense and generalises the above intuitive notion of holes. An example of a 'one-dimensional hole' would be the hole in a circle, while an example of a 'two-dimensional hole' would be the hole in a $2$-dimensional sphere (think about a kinder egg, with the 'hole' being the interior of the egg). %this seems like a confusing example, since the earth doesn't really have a hole. perhaps an easter egg (or kinder egg, if that's a universal thing) would be more elucidating

Even though algebraic topology is a very deep mathematical theory and applies to several inner-mathematical disciplines, it seems rather abstract and not suitable to be applied to 'real-world problems'. However, with time topology started to become a topic of interest in theoretical physics. There, it appears on a fundamental level in Quantum field theory and String theory but also in statistical physics. For instance, the genus (as discussed above) of a string worldsheet gives us the order in a perturbative expansions of string interaction amplitudes in analogy to the number of loops in the context of ordinary Feynman diagrams. Topology is actually a key discipline in the interplay between mathematics and physics since physical intuition turned out to be useful for proving mathematical results about certain topological spaces (e.g. many results about 3 and 4 dimensional manifolds or the local Atiyah-Singer Index Theorem). A rather new and quite surprising development on which I want to focus in this article is the application of algebraic topology to computer science.

\subsection{Topological Data Analysis}
In data science, one is faced with the problem of extracting nontrivial information from datasets, which can often be represented by point clouds (i.e. finite subsets) in high-dimensional vector spaces. Such datasets may arise from an array of sensor readings in an engineering testbed, from questionnaire responses in a psychology experiment, or from population sizes in an ecosystem. At first glance, it does not seem like we can extract any interesting information from computing the homology of a discrete set of points. All sets of $n$ points (with the discrete topology) are topologically indistinguishable, so calculating the homology of this will count the number of points and give us no more information. In particular, it cannot detect any holes.

 %I think here it may be better to be more explicit about exactly what homology is doing: perhaps instead the last sentence should read 'all sets of n points are topologically indistinguishable, so calculating the homology of this will count the number of points and give us no more information (namely it cannot detect any holes)

On the other hand, if we look at the example of a point cloud in figure \ref{fig:S1}, we intuitively see that there is a 'hole' and in particular that some nontrivial topology is at play.

\begin{figure}[h]
\centering
\includegraphics[width=0.4\linewidth]{TDA_S1}
 \caption{A point cloud with a hole in it}
    \label{fig:S1}

\end{figure}


Without going into any concrete example, it is also quite clear that this hole that we see intuitively is a piece of interesting information about the given dataset that we would like to extract. Now, we need to formalise what we mean by this hole so that we can generalise it to higher dimensions and eventually get some information about given data sets in arbitrarily high dimensional spaces. There is a computationally feasible approach to do this, which is known as 'persistent homology'.

\subsection{Persistent Homology}
As we saw, it does not make sense to compute the homology of the given point cloud as it is. Our intuition tells us that we should draw lines between all the points that are close to each other, then fill out all the little triangles and so on to get a simplicial complex. We do not need the formal definition of a simplicial complex for our intuitive approach here, so just think about lines, triangles, tetrahedra and further generalised triangles glued to each other along their faces. It is possible to compute the homology of such a simplicial complex on a computer, so computing the homology of the emerging simplicial complex will give us the interesting information: Our data set has one one-dimensional hole. This intuition is perfectly right, but comes with one big problem: At which length scale should we stop drawing lines and triangles? The cutoff should be large enough such that all the 'noise' on a small scale closes. But if it is too large, we draw lines and triangles between all points such that the interesting hole closes and computing the homology will not give us any interesting information. %maybe this is obvious, but maybe you should stress the idea of an 'ideal point' here: the cutoff also has to be large enough that the holes actually 'close', so it could be rephrased as 'it needs to be large enough... but if it's too large...'
So we have to find this cutoff dynamically. We will now sketch the idea of how to do this.

Given a point cloud $D\subset \mathbb{R}^n$ (which is just a finite metric space with the induced metric of $\mathbb{R}^n$) we imagine to draw balls of radius $\epsilon\geq 0$ around each point and draw a line between two points $x,y \in D$ if $B_{\epsilon}(x)\cap B_{\epsilon}(y)\neq \emptyset$. We draw triangles between three points if the balls around any two of these points intersect each other and we proceed in the same way for higher dimensions. The resulting simplicial complex is known as the Vietoris-Rips-Complex of $D$ at scale $\epsilon$, written $\VR_{\epsilon}(D)$. It can formally be defined as the abstract simplicial complex \begin{align*}
\VR_{\epsilon}(D):=\{\sigma \subset D: \|x-y\|\leq 2 \epsilon \textrm{ for all } x,y \in \sigma \}.
\end{align*}
The following example in figure \ref{fig:VR} shows the Vietoris-Rips-Complex of an annulus-shaped point cloud $D$ for different values of $\epsilon$ (Here, $D$ stands for doughnut and not data).

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{TDA_VR2}
  \caption{$\VR_\epsilon(D)$ for six different, increasing $\epsilon$-values}
    \label{fig:VR}
\end{figure}

Observe, that even though $\epsilon \geq 0$ may be varied continously, the Vietoris-Rips-Complex only changes at discrete $\epsilon$-values $\epsilon_1<\dots <\epsilon_n$ since there are only finitely many points in $D$. So we arrive at a filtered simplicial complex \begin{align*}
\VR_0(D) \hookrightarrow \VR_{\epsilon_1}(D)\hookrightarrow \VR_{\epsilon_2}(D) \dots \hookrightarrow \VR_{\epsilon_n}(D)
\end{align*}
where we can compute the homology at each filtration step to get \begin{align*}
H_{*}(\VR_0(D)) \rightarrow H_{*}(\VR_{\epsilon_1}(D))\rightarrow H_{*}(\VR_{\epsilon_2}(D)) \dots \rightarrow H_{*}(\VR_{\epsilon_n}(D))
\end{align*}
%IMPORTANT: this seems wrong — if you fill in a triangle you get an inclusion on simplicial complexes as mentioned but certainly not on homology. I think if I'm right just end this after '...filtration step' since the rest is in the next section anyway

\subsection{The mathematical structure of TDA}

We saw above, that for each $i\leq j$ we have a natural inclusion of simplicial complexes $\VR_{\epsilon_i}(D) \hookrightarrow \VR_{\epsilon_j}(D)$, which induces a natural map $H_*(\VR_{\epsilon_i}(D)) \rightarrow H_*(\VR_{\epsilon_j}(D))$ by the functoriality of homology.

So we have a functor from the partially ordered set $\{\{0,\dots,n\},\leq)$ (considered as a category) to the category of $R$-modules (in this article we only consider the case $R=\mathbb{Q}$). This motivates the following definition:

\begin{defi}
A persistence module over a partially ordered set $(P, \leq)$ with coefficients in a commutative ring $R$ is a functor from $(P,\leq)$ considered as a category to the category of $R$-modules.
\end{defi}

This more abstract approach gives rise to surprising applications of persistent homology to inner-mathematical disciplines such as symplectic topology (see e.g. \cite{UZ15}). In these applications, the concept of a barcode is crucial as well. We will discuss the intuition behind these in the following section.

\subsection{Barcodes}
It is common to depict the homology groups $H_k(\VR_{\epsilon}(D))$ in a so-called barcode. In figure \ref{fig:Barcode} we can see the barcode of the above example from figure \ref{fig:VR}. Staying close to this example, we would like to explain how this barcode is constructed and which information is encoded in it.

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{TDA_Barcode}
\caption{The barcode of the dataset $D$}
    \label{fig:Barcode}


\end{figure}

%since the meaning of H0 hasn't come up yet you could make a note of what the red lines are in the first part of the following paragraph, as it may be confusing
The barcode represents the homology classes as horizontal lines that 'are born' at some value $\epsilon_1$ and 'die' at some other value $\epsilon_2>\epsilon_1$. We use different colours for different values of $k\in \mathbb{N}$. The difference $\epsilon_2-\epsilon_1$ is called the 'lifetime' of a homology class. This barcode finally gives us all of the qualitative information we were looking for. The homology classes with a long lifetime can be understood to be 'holes on a reasonable scale' while the nontrivial homology classes that have a short lifetime (compared to the class with the longest lifetime) can be seen as noise in the data. The 'important' classes with a long lifetime can be interpreted to persist over a long parameter range, which justifies the name 'persistent homology'. In figure \ref{fig:Barcode}, the longest horizontal green line represents such a persistent homology class. This persistent homology class, in turn, represents the hole in our doughnut-shaped data set $D$. The other relatively long green lines are seen because our original data set is still quite far away from an ideal doughnut. If we would compute the barcode of figure \ref{fig:S1} we would see one very long green line and other very short green lines for small $\epsilon$-values.

To put it in a nutshell, we can extract information from the barcode by drawing a vertical line at fixed $\epsilon \geq 0$ (the blue dashed lines in figure \ref{fig:Barcode}). Then we can count the intersection points with the green lines and get the number of holes of $\VR_{\epsilon}(D)$ or in other words $\beta_1(\VR_{\epsilon}(D))$. Counting the number of intersections of a blue line with the red lines gives us the number of connected components of $\VR_{\epsilon}(D)$ or in other words $\beta_0(\VR_{\epsilon}(D))$. The longest horizontal lines represent the 'important' topological features of the dataset.

Now, where the formalism is operational, we should argue that the above example does not demonstrate the full power of persistent homology. We only observed a $1$-dimensional hole. In principle, we can compute holes of arbitrarily high dimension simultaneously and compare their length scales. Think for example of a data set $D \subset \mathbb{R}^3$ which approximates a $2$-sphere with a small disk removed that we call$M$ (see figure \ref{fig:Deathstar}). Here, $D$ stands for Death star not for Data. Here we would see no non-trivial homology for a large range of $\epsilon$ (since a sphere with a 'hole' cut out is contractible), until for large enough $\epsilon$ we obtain non-trivial second homology, suggesting indeed that our space looks like a $2$-sphere with a '$1$-dimensional hole'.
%I feel like you could expand a bit here, since I'm not sure what happens in general: I suppose you could choose your scale so that the circle closes and it looks homologically like a sphere, but if the circle doesn't close you would get something contractible (bad!). Would you ever find a 1-hole?


\begin{figure}[h]
\centering
\includegraphics[width=0.4\linewidth]{TDA_Deathstar}
  \caption{Roughly what $M=S^2\setminus D^2_{\epsilon}$ looks like}
    \label{fig:Deathstar}
\end{figure}



\newpage
\thispagestyle{plain}

\bibliographystyle{alpha}
\begin{thebibliography}{\hspace{1.5cm}}

	\addcontentsline{toc}{section}{References}


\bibitem[CA14]{CA14}
	Carlson G.: {\em Topological pattern recognition for point cloud data}.
	Acta Numerica, vol. \textbf{23}, 289--368. 2014.

\bibitem[GH08]{GH08}
	Ghrist R.: {\em Barcodes: The persistent topology of data}.
	Bull. Amer. Math. Soc., vol. \textbf{45}, 61--75. 2008.

	\bibitem[Hat02]{Hat02}
	Hatcher A.: {\em Algebraic Topology}.
	Cambridge University Press 2002.

\bibitem[UZ15]{UZ15}
	Usher M., Zhang J.: {\em Persistent homology and Floer-Novikov theory}.
	arXiv:1502.07928. 2015.


\end{thebibliography}


\end{document}
