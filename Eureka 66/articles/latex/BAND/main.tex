\documentclass[9.5pt,headsepline,cleardoubleempty,bibtotoc,tablecaptionabove,english,twoside]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage{multicol}
\usepackage{xcolor}
\usepackage{fontspec}

\setlength{\columnsep}{7mm}
\setlength{\parskip}{3mm}

\newfontfamily\fontheading[SizeFeatures={Size=48}]{MyriadPro-Bold}
\newfontfamily\fontauthor[SizeFeatures={Size=24}]{MyriadPro-Light}
\newfontfamily\fontpagenr[SizeFeatures={Size=14}]{Myriad Pro}
\newfontfamily\fontsubheading[SizeFeatures={Size=16}]{MyriadPro-Bold}
\newfontfamily\fontcapt[SizeFeatures={Size=10}]{MyriadPro-Light}
\newfontfamily\fontfignr[SizeFeatures={Size=10}]{Myriad Pro}

\definecolor{headings}{HTML}{f99d1c}
% violett #c02555
% dunkelblau #2b367c
% sehr dunkles Grau #231f20
% hellblau #00adbd
% orange #f99d1c
% lila #f99d1c
% grün #62bb46

\usepackage{libertine}
\usepackage{babel}

\addto{\captionsenglish}{%
  \renewcommand{\refname}{}
}


\usepackage{geometry}
\geometry{a4paper, margin=21mm}

\makeatletter
\newcommand*{\shifttext}[2]{%
  \settowidth{\@tempdima}{#2}%
  \makebox[\@tempdima]{\hspace*{#1}#2}%
}
\makeatother
\setlength{\parindent}{0pt}% Just for this example


\usepackage{fancyhdr}

\renewcommand{\headrulewidth}{0pt}
% \lhead{\shifttext{-15mm}{\fontpagenr\thepage}}
\cfoot{}
\pagestyle{fancy}

% \linespread{1.3}
% \setlength{\parskip}{\baselineskip}%
% \setlength{\parindent}{0pt}%
%generally needs more spacing for legibility

%\usepackage[onehalfspacing]{setspace}%
\title{Smooth Piecewise Polynomials}
\author{Josef Greilhuber}
\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{stmaryrd}
\usepackage{svg}
\usepackage{array}
\usepackage[document]{ragged2e}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{calc}

\usepackage{thmtools}
%\newtheorem{stz}{Satz}
\newtheorem{cor}{Corollary}
%\newtheorem{lem}{Lemma}
\newtheorem{exa}{Example}
\newtheorem{thm}{Theorem}
\newtheorem{defi}{Definition}
%\theoremstyle{definition}
\newtheorem{rem}{Remark}
%\newtheorem*{bem}{Bemerkung}
%\newtheorem{bsp}{Beispiel}
\newtheorem{prop}{Proposition}
\newtheorem{lem}{Lemma}

\numberwithin{equation}{section}
% Datum der Abgabe
\setmainfont[BoldFont={Minion Pro Bold}, ItalicFont={Minion Pro Italic}]{Minion Pro}

\newcommand{\subheading}[1]{
\vspace{8mm}
{\fontsubheading\textcolor{headings}{#1}}\vspace{2mm}

}

\begin{document}

{\fontheading\textcolor{headings}{\foreignlanguage{nohyphenation}{\begin{sloppypar}
\baselineskip=48pt
  Archimedes' Tool Kit
\end{sloppypar}}}}

\vspace{5mm}

{\fontauthor\textcolor{headings}{
   Eric L. Bandurski % University of Durham
}}

\vspace{30mm}

\justify

\begin{multicols}{2}

A skilled person of Archimedes’ time could have made accurate models of all 18 Platonic and Archimedean polyhedra by inscribing circles and chords on spheres. Using a few measurements from geometry, the tools required would be: a lathe, dividers, and a drill.

Using a knowledge of six ordinary polygons, one can construct all 18 Platonic and Archimedean polyhedra.  One must first abandon the idea that measuring precise angles is necessary.  A woodworker making molecular models soon realizes that depending on protractors is crude.  If you want precise angles, use sines, effectively chords -- the old-fashioned way. There is a tendency of those who are perhaps overschooled in maths to bludgeon the problem of constructing these polyhedra by applying advanced mathematics. Instead, let’s simplify and let the beauty of rigorous geometry and the concepts of circle, chord, and sphere do all our work.

\subheading{Our mathematical tool kit}

The Platonic and Archimedean polyhedra use one or more of six regular polygons. 3, 4, or 5 regular polygons meet at each vertex.  The polygons can be the same at each vertex (5 Platonic polyhedra) or mixed (13 Archimedean polyhedra).


\end{multicols}


\vspace{1em}
\begin{tabular}{lp{4cm}}
\includegraphics[width={11cm}]{fig1.png}
\hspace{1cm}
&
\vspace{-8cm}

${\textcolor{headings}{\blacktriangleleft}}$\enspace{\fontcapt {\fontfignr Figure 1:} The lengths of the diagonals, $BE$, across two sides of these six polygons were surely known to Archimedes. He could easily have constructed and measured them using dividers and straightedge.  In Ptolemy’s time (c.\ AD 100--170),  these chords corresponding to the interior angles of the polygons were used in compiling the Almagest.}
\end{tabular}

\pagebreak

\begin{multicols}{2}

\subheading{Seeking the Simple Approach}

I knew from my reading that vertices of Platonic and Archimedean polyhedra are interchangeable.  This requires that they be located on the surface of a sphere, which is called the \textit{circumsphere}.
While studying Wikipedia drawings I made the second critical observation that the neighbouring vertices immediately surrounding each vertex lie on a circle.
They \textit{must} lie on a circle because they lie on the intersection of two spheres: a smaller sphere whose radius equals the edge length, $S$, and the circumsphere which is the locus of all vertices. The smaller sphere I will call the \textit{vertex sphere}. The intersection of these spheres, i.e., the circle, I will call the \textit{vertex circle}.

Eureka! Our problem simplifies to precisely fitting a set of chords of known length around a circle.  This suggests a purely geometric method for making any of the eighteen Platonic and Archimedean polyhedra.  We don’t need a complicated apparatus for setting angles; we only need: 1) the radius of this vertex circle, 2) a chord length for scribing this circle on a sphere, and 3) the lengths of the diagonals for the regular polygons meeting at each vertex.

\subheading{The icosidodecahedron}

To illustrate this concept we will construct the geometry of one of the most beautiful of the Archimedean polyhedra, the icosidodecahedron. To make a%
\hspace*{5mm}\linebreak
long story short (because it’s fully%
\hspace*{5mm}\linebreak
expanded in another paper), each of the%
\hspace*{5mm}\linebreak
18 Platonic and Archimedean polyhedra can%
\hspace*{5mm}\linebreak
be constructed precisely in ball-and-%
\hspace*{8mm}\linebreak
stick form by scribing a vertex pattern%
\hspace*{14mm}\linebreak
on accurate spheres, one for each vertex.%
\hspace*{17mm}\linebreak
The icosidodecahedron (designated%
\hspace*{20mm}\linebreak
3,5,3,5) has 30 vertices, each of them%
\hspace*{22mm}\linebreak
with four polygons (triangle,%
\hspace*{24mm}\linebreak
pentagon, triangle, pentagon)%
\hspace*{25mm}\linebreak
arranged around the vertex, as%
\hspace*{26mm}\linebreak
shown in Figure 2.  For convenience,%
\hspace*{25mm}\linebreak

\columnbreak
% \hspace*{26mm}\linebreak
let’s set the radius of our vertex %
% \hspace*{26mm}\linebreak
sphere, therefore the edge length of %
% \hspace*{26mm}\linebreak
our polyhedron, $S = 1$.  Chords of %
the four polygons must fit perfectly
% \hspace*{23mm}\linebreak
on the vertex circle of smaller radius, $r_{vc}$:

\vspace{8mm}
\begin{centering}
\includegraphics[width={\columnwidth-1cm}]{fig2.png} \\
\begin{quote}
  ${\textcolor{headings}{\blacktriangle}}$\enspace{\fontcapt {\fontfignr Figure 2:} View from above\\ vertex $A$ of four surrounding vertices}
\end{quote}
\end{centering}
\vspace{1em}



\end{multicols}




\vspace{-11cm}
\hspace{6cm}
\includegraphics[width={\columnwidth-6cm}]{fig3.png} \\
\vspace{-1cm}
\begin{quote}
  {\fontcapt {\fontfignr Figure 3:} Geometry of the icosidodecahedron derived from the radius of the vertex circle}\enspace\textcolor{headings}{$\blacktriangle$}
\end{quote}
\vspace{1em}


\pagebreak

\begin{multicols}{2}


    \textbf{1.\ Finding the vertex circle radius}

    The chords, $BE$, $EF$, $FG$, and $GB$ (diagonals of polygons) must fit perfectly on the circle of radius $r_{vc}$.  From Pythagoras we can find the radius of the\linebreak
icosidodecahedron
 vertex circle, $r_{vc}$, a projection of edge $S$, as $\frac{\sqrt{\varphi +2}}2 \approx 0.951056$.  The radii of the vertex
 circles of all except two of the remaining polyhedra can be calculated analytically.  The radii of the vertex
  circles for the snub cube and the snub dodecahedron can be calculated very precisely with a few iterations.


  \textbf{2.\ Constructing a cross-section}

   Once the radius of the vertex circle, $r_{vc}$, has been determined, a cross-section of any of the 18 polyhedra can be constructed with dividers and straightedge, as seen in Figure 3.  Beginning at $O$, we lay down $XB$, a diameter of the vertex circle, and $YZ$, its perpendicular bisector.  We locate vertex $A$ by striking a unit arc from $B$, intersecting $YZ$.  The same arc from $A$ defines the vertex sphere and locates $H$.  $HB$ is an important chord to be used later.  $VW$, the perpendicular bisector of edge $AB$, intersects $YZ$ and defines $C$, the centre of the circumsphere.

\textbf{3.\ Using a model sphere}

Any small sphere centred at vertex $A$ can serve as a perfect miniature of the vertex sphere of radius $AB$, including the angles.  Figure 5 shows a cross section of the model sphere.  Before locating holes for the dowels, chord $H'B' = HB \cdot r$ is used to scribe the vertex circle on a model sphere.


\textbf{4.\ Laying out chords}

Below is the model sphere viewed from $C$,
the centre of the polyhedron.  The chords will have
lengths proportional to the smaller radius: $B'E' = r$,
$E'F' = \varphi \cdot r$, $F'G' = r$, and $G'B' = φ r$.  Using the dividers
we can step off these four chords around the miniature
vertex circle described earlier: first, $B'E'$, $E'F'$, and $F'G'$,
then $G'B'$.  This should bring us back to B' and complete
the quadrilateral.  This is a valuable check for precision.

\vspace{0.4em}
\begin{centering}
\hspace{5mm}
\includegraphics[width={\columnwidth-13mm}]{fig5.png} \\
\begin{quote}
  ${\textcolor{headings}{\blacktriangle}}$\enspace{\fontcapt {\fontfignr Figure 4:} Model sphere viewed from $C$}
\end{quote}
\end{centering}
% \vspace{5.6mm}

The final step is to drill four holes of equal depth, exactly toward the centre of the sphere, at the four locations $B'$, $E'$, $F'$, and $G'$.  Thirty of these spheres and sixty edges of equal length close perfectly into a rigid, classic icosidodecahedron. A picture is shown on the next page.


\end{multicols}



\vspace{1em}
\begin{centering}
\includegraphics[width={\columnwidth-1cm}]{fig4.png} \\
\vspace{-0.5em}
\begin{quote}
  \hspace{80mm}
    ${\textcolor{headings}{\blacktriangle}}$\enspace{\fontcapt {\fontfignr Figure 5:} Cross-section of the model sphere}
\end{quote}
\end{centering}


\pagebreak

\begin{multicols}{2}



  \vspace*{17mm}
  \begin{centering}
  \includegraphics[width={\columnwidth-0cm}]{fig6.jpg}
  \begin{quote}
    ${\textcolor{headings}{\blacktriangle}}$\enspace{\fontcapt {\fontfignr Figure 6}\\
    \hspace{4mm} The icosidodecahedron I constructed}

      % \textcolor{headings}{$\blacktriangle$}
  \end{quote}
  \end{centering}
  \vspace{1em}

  \columnbreak

\subheading{What we used}

The tool kits required to make accurate ball-and-stick models of polyhedra turn out to be quite simple:

\textbf{Mathematical Tool Kit:} \textit{chord lengths} (polygon diagonals), \textit{an iterative or analytical method to find $r_{vc}$} (vertex circle radius), and \textit{geometry} to find $H'B'$ (arc length to scribe a miniature vertex circle on a sphere).

\textbf{Physical Tool Kit:}  \textit{lathe} (spheres of equal diameter for vertices and dowels of equal length for edges), \textit{dividers}, and a \textit{drilling apparatus} (holes of equal depth, drilled toward the exact centre of each sphere).

Could the ancients in Archimedes’ time have constructed his 13 polyhedra, using geometry and the simple tools available? I believe the answer is \textit{yes}. In fact, it appears that an approach using circles and chords scribed on spheres is the simplest and most precise way that the polyhedra can be constructed.

Accurate spheres could have been made of a stable wood, e.g., olive heartwood in Archimedes’ time.   Fortunately for us, precision spheres of polyethylene or polypropylene are used in ball valves, so they’re about as affordable as wood and usually accurate to within a thousandth of an inch.  A stringent test of this construction is the creation of a chiral pair of snub dodecahedra – my original goal:




\end{multicols}


\vspace{20mm}
\begin{centering}
\includegraphics[width={\columnwidth-3cm}]{fig7.jpg} \\

\begin{quote}
  \hspace{50mm}
  ${\textcolor{headings}{\blacktriangle}}$\enspace{\fontcapt {\fontfignr Figure 7}\\
  \hspace{54.8mm} A chiral pair of snub dodecahedra}
    % \textcolor{headings}{$\blacktriangle$}
\end{quote}
\end{centering}

\end{document}
