\documentclass{article}
\usepackage{geometry}
%\usepackage[utf8]{inputenc}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{graphicx}
\usepackage{float}
\newtheorem{theorem}{Theorem}
\newtheorem{example}{Example}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}
\newtheorem{corollary}{Corollary}
\newtheorem{question}{Question}
\newtheorem{exercise}{Exercise}

\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\length}{length}

\title{Doodles}
\author{Jeremy Taylor \\ jwt45@cam.ac.uk}
\date{}

\begin{document}
	
	\maketitle
	
	\section{Doodles}
	A \textbf{doodle}, that is a curve that you can draw in the plane without lifting your pencil.
	
	\centerline{\includegraphics[height=7cm]{exampledoodle.png}}
	
	
	The doodle divides the plane into faces. Notice that they can be colored with two colors so that adjacent faces have different colors.
	
	We will give three proofs that the faces of a doodle can always be $2$-colored.
	
	\newpage
	\section{Graph Theory}
	\subsection{Euler Cycles}
	First we need to develop the right language to talk about doodles.
	
	A doodle can be formalized as a planar graph $D$, consisting of vertices connected by edges and embedded in the plane. And a doodle divides the plane into faces.
	
	But not all planar graphs correspond to a doodles.
	
	
	\centerline{\includegraphics[height=3cm]{nondoodle.png}}
	
	For example, this planar graph is not a doodle. Notice that the faces cannot be $2$-colored.
	
	\begin{definition}
		An \textbf{Euler cycle} is a path that visits every edge exactly once.
	\end{definition}
	
	So a planar graph is a doodle if and only if it has an Euler cycle. Such graphs are characterized by the following simple condition.
	
	\begin{proposition}
		A connected graph has an Euler cycle if and only if all vertices have even degree.
	\end{proposition}
	\begin{proof}
		Suppose $G$ has an Euler cycle $\gamma$. Then every time $\gamma$ enters a vertex it must also leave so the degree is even.
		
		Now suppose that every vertex has even degree. We would like to construct an Euler cycle. Start at some vertex $v$ and repeatedly follow unvisited vertices. Since all vertices have even degree we can only run out of vertices to follow after returning to $v$. So we have constructed a cycle. If it does not visit all edges, we can append additional loops until all edges are visited.
	\end{proof}
	
	
	\subsection{Two Coloring}
	Now we need to understand precisely the statement that the faces can be $2$-colored.
	
	\begin{definition}
		The \textbf{dual} of a planar graph is obtained by replacing faces with vertices and connecting those that are adjacent faces. We denote the dual of $G$ by $G^*$.
	\end{definition}
	
	
	\centerline{\includegraphics[height=4cm]{dualgraph.png}}
	
	So the faces of $D$ can be $2$-colored if and only if the vertices of $D^*$ can be $2$-colored.
	
	\begin{proposition}
		A graph's vertices can be $2$-colored if and only if there are no odd length cycles.
	\end{proposition}
	\begin{proof}
		We wish to color the vertices black and white.
		
		First we take some initial vertex $v$ and color it black.
		%Then the coloring is completely determined locally. That is for each red vertex all of the adjacent vertices are colored blue.
		
		\centerline{\includegraphics[height=5cm]{pathindependence.png}}
		
		Then for any other vertex $w$ choose a path $\gamma$ from $v$ to $w$. Since the colors of vertices along the path must alternate the color of $w$ is determined by $\length(\gamma)$ mod $2$. However we need to know that the result is independent of path. This happens if and only if there are no odd length cycles.
		%This is somehow saying that there is no global obstruction.
	\end{proof}
	
	There are two themes to notice here.
	
	First is parity. We are working mod 2.
	
	Second there is an interaction between the local and the global. A two coloring of a graph is determined locally. The condition that the graph has no odd length cycles means that there are no global obstructions.
	
	\subsection{Two-Coloring Doodles}
	
	\begin{theorem}
		\label{doodle1}
		The dual graph of any doodle $D$ can be $2$-colored.
	\end{theorem}
	\begin{proof}
		We have the following chain of reasoning.
		
		\begin{enumerate}
			\item $D$ is a doodle.
			\item $D$ has an Euler cycle.
			\item All vertices of $D$ have even degree.
			\item The dual $D^*$ has no odd length cycles.
			\item The dual $D^*$ can be $2$-colored.
		\end{enumerate}
		
		The only implication left to prove is $3 \Rightarrow 4$.
		
		We say that a cycle in $D*$ is \textbf{simple} if it goes around a single vertex.
		
		Suppose that all vertices of $D$ have even degree. Then any simple cycle in $D*$ has even length.
		
		\centerline{\includegraphics[height=3cm]{simplecycle.png}}
		
		Now suppose that we have a cycle $\gamma$ in $D^*$ that is not simple.
		
		\centerline{\includegraphics[height=6cm]{longoddcycle.png}}
		
		Then our cycle bounds a region containing a collection of vertices, $V_1 \dots V_r$.
		
		\centerline{\includegraphics[height=6cm]{cycledecomposition.png}}
		
		We break up the full cycle into simple cycles $\gamma_1 \dots \gamma_r$ that bound the individual vertices.
		
		Then $$\sum_{i} \length \gamma_i = 2I + \length \gamma,$$
		where $I$ is the number of interior dual edges (of some $\gamma_i$ but not $\gamma$).
		
		Since $\length \gamma_i$ is even, we have that $\length \gamma$ is even as desired.
	\end{proof}
	
	\subsection{Doodles on the Torus}
	
	Now suppose we draw our doodles on the torus instead of the plane.
	
	\centerline{\includegraphics[height=3cm]{torus.png}}
	
	Consider the following counterexample.
	
	\centerline{\includegraphics[height=4.5cm]{torusdoodle.png}}
	
	Gluing opposite sides of the square gives a doodle $D$ that cannot be $2$-colored.
	
	So we need to see where the argument goes wrong.
	
	%\centerline{\includegraphics[height=4.5cm]{oddcycle.png}}
	
	
	
	\centerline{\includegraphics[height=4cm]{toruscycle.png}}
	
	In the counterexample there is an odd length cycle in the $D*$. The problem is that our odd length cycle goes around a hole torus so it does not bound a region. Thus the argument in Theorem $\ref{doodle1}$ fails.
	
	
	\newpage
	\section{Winding Number}
	
	\begin{definition}
		For any point $p \in \mathbb{R}^2$ that does not lie on the doodle $D$, the \textbf{winding number} $I(\gamma_D, p)$ is the number of times that the curve $\gamma_D$ wraps around $p$.
	\end{definition}
	
	\centerline{\includegraphics[height=6.5cm]{windingnumber.png}}
	
	Notice that $I(\gamma_D, p)$ is constant on the faces. Furthermore the winding number of adjacent faces differs by $\pm 1$. This gives a $2$-coloring of the faces where the color is determined by the parity of the winding number.
	
	\begin{proposition}
		The winding number differs by $\pm 1$ on adjacent faces.
	\end{proposition}
	
	\begin{proof}
		$ $
		
		\centerline{\includegraphics[height=4cm]{adjacentwindingnumber2.png}}
		
		We wish to prove that the winding numbers, $I(\gamma_D, P_1)$ and $I(\gamma_D, P_2)$ differ by $\pm 1$.
		
		
		\centerline{\includegraphics[height=4cm]{adjacentwindingnumber.png}}
		
		To do this we consider adding a path $\delta$ around $P_1$ that `cancels out' part of the edge between $P_1$ and $P_2$.
		
		We have $$I(\gamma_D + \delta, P_1) = I(\gamma_D, P_1) + I(\delta, P_1) = I(\gamma_D, P_1) \pm 1.$$
		
		\centerline{\includegraphics[height=4cm]{gammaplusdelta.png}}
		
		But after adding $\delta$ to $\gamma_D$ we have that $P_1$ and $P_2$ lie in the same connected component.
		
		So $$I(\gamma_D + \delta, P_1) = I(\gamma_D + \delta, P_2) = I(\gamma_D, P_2) + I(\delta, P_2) = I(\gamma_D, P_2).$$
		
		Hence $I(\gamma_D, P_2) = I(\gamma_D, P_1) \pm 1$ as desired.
	\end{proof}
	
	\newpage
	\section{Homology}
	
	\subsection{Cycles and Boundaries}
	Consider a doodle $D$.
	
	Let $V$ be the set of vertices, $E$ the set of edges, and $F$ the set of faces.
	
	We define $$C_0 = \bigoplus V_i\mathbb{Z}/2, \quad C_1 = \bigoplus E_i\mathbb{Z}/2, \quad \text{and } C_2 = \bigoplus F_i\mathbb{Z}/2,$$
	
	the free $\mathbb{Z}/2$-modules generated by the set of vertices, edges, and faces respectively.
	
	Note that each element of $C_0, C_1, C_2$ corresponds to a subset of $V, E, F$ respectively.
	
	Then there are `boundary' maps ($\mathbb{Z}/2$-module homomorphisms) $$C_2 \xrightarrow{\quad d_2 \quad} C_1 \xrightarrow{\quad d_1 \quad} C_0.$$
	
	The map $d_2$ sends a set of faces to the set of edges on its boundary.
	
	\centerline{\includegraphics[height=3cm]{d2.png}}
	
	The map $d_1$ sends a set of edges to the set of vertices on its boundary.
	
	\centerline{\includegraphics[height=6cm]{d1.png}}
	
	We have $$d_1 \circ d_2 = 0 \quad \text{or equivalently} \quad \im d_2 \subset \ker d_1$$ because the boundary of a boundary is zero.
	
	The elements of $\ker d_1$ are called \textbf{cycles} because they correspond to (one or multiple) closed loops on the graph.
	
	\centerline{\includegraphics[height=3.5cm]{cyclenotboundary.png}}
	
	The elements of $\im d_2$ are called \textbf{boundaries} because they correspond to loops that bound some collection of faces.
	
	\centerline{\includegraphics[height=3.5cm]{boundaryandcycle.png}}
	
	
	
	\subsection{An Example}
	
	Consider the following planar doodle where we have labeled the vertices, edges, and faces.
	
	\centerline{\includegraphics[height=7cm]{labeleddoodle.png}}
	
	For example, we have $d_1(E_1) = V_1 + V_2$ are the two vertices on the boundary of $E_1$. 
	
	Similarly $d_2(F_2) = E_5 + E_{15} + E_{3} + E_{20}$ are the four edges on the boundary on $F_2$.
	
	As expected these form a cycle so $d_1 \circ d_2 (F_2) = d_1(E_5 + E_{15} + E_{3} + E_{20}) = \emptyset$.
	
	\centerline{\includegraphics[height=7cm]{cycleboundary2.png}}
	
	Another example of a cycle is $B = E_1 + E_5 + E_{17} + E_{11}, E_{12} + E_{13} + E_9 + E_{18} \in C_1$.
	
	We have $d_1(B) = 0$ so $B$ is indeed a cycle.
	
	But $B$ is also a boundary since $B = d_2(F_1 + F_9 + F_8 + F_{11})$.
	
	\subsection{Two-Coloring Doodles}
	
	Suppose that $D$ is a doodle in the plane. Then since the plane has no holes, every cycle bounds some set of faces. So we have equality $\im d_2 = \ker d_1$.
	
	\begin{theorem}
		The dual graph of a planar doodle can be $2$-colored.
	\end{theorem}
	\begin{proof}
		Consider the sum of all edges $$B = \sum E_i \in C_2.$$ We have $d_1(B) = 0$ since every vertex has even degree. Therefore since $\ker d_1 = \im d_2$ it follows that $B \in \im d_2$.
		
		Let $$B = d_2(A) \text{\quad for some subset \quad} A = \sum a_i F_i \in C_2.$$ Then $a_i$ gives a 2-coloring of the faces.
	\end{proof}
	
	\subsection{Spaces with Holes}
	
	We have said that $\im d_2 = \ker d_1$ for any graph $D$ embedded on a topological surface without holes.
	
	On a general space we do not always have equality. A cycle will fail to be a boundary when it goes around a `hole'.
	
	To be more precise, recall that the cycles, $\ker d_1$, correspond to loops. But the boundaries, $\im d_2$, are the cycles that don't actually go around a hole so we quotient out by them.
	
	Thus we define the first homology module $$H_1(D, \mathbb{Z}/2) = \ker d_1 / \im d_2.$$
	
	As long as the faces of $D$ are reasonably behaved, $H_1(D, \mathbb{Z}/2)$ doesn't actually depend on the graph $D$ but only on the topological space $X$ in which the graph is embedded. Given a space $X$, we can thus define $H_1(X, \mathbb{Z}/2)$ which encodes information about the 1-dimensional holes in $X$.
	
	We said before that since the plane has no holes $H_1(\mathbb{R}^2, \mathbb{Z}/2) = \{0\}$ is trivial.
	
	On the torus, $$H_1(T^2, \mathbb{Z}/2) \cong \mathbb{Z}/2 \oplus \mathbb{Z}/2$$ which corresponds to the two holes of a torus.
	
	\centerline{\includegraphics[height=4.5cm]{homologyclass.png}}
	
	For instance, $\gamma_1 = \{E_1, E_2, E_3\} \in \ker d_1 \setminus \im d_2$ is a cycle. But it is not the boundary since it goes around a hole.
	
	\centerline{\includegraphics[height=4cm]{homologyclass2.png}}
	
	Also $\gamma_2 = \{E_4, E_5, E_6\} \in \ker d_1 \setminus \im d_2$ is a cycle but not a boundary.
	
	Thus $[\gamma_1]$ and $[\gamma_2]$ represent nonzero elements in $H_1(T^2, \mathbb{Z}/2)$.
	
	Furthermore they are generators, $$H_1(T^2, \mathbb{Z}/2) = \langle [\gamma_1], [\gamma_2]\rangle \cong \mathbb{Z}/2 \oplus \mathbb{Z}/2.$$
	
	
	
	
	\subsection{Other Coefficients}
	
	We can also do homology with coefficients in other rings.
	
	More generally for a ring $R$, we can define $$C_0 = \bigoplus V_iR, \quad C_1 = \bigoplus E_iR, \quad \text{and } C_2 = \bigoplus F_iR.$$
	
	This just means that every element of $C_0$ is of the form $\sum r_i V_i$ a sum of vertices with coefficients in $R$. 
	In the case $R = \mathbb{Z}/2$, elements of $C_i$ correspond to subsets, so we recover our old definition. We only work with coefficients mod $2$ so we do not have to worry about orienting faces.
	
	
	In the general case, orienting the faces and vertices is necessary to get boundary maps $$C_2 \xrightarrow{\quad d_2 \quad} C_1 \xrightarrow{\quad d_1 \quad} C_0 \quad \text{ with } \quad \im d_2 \subset \ker d_1.$$
	
	Again we can define $$H_1(G, R) = \ker d_1 / \im d_2.$$
	
	
	
	\subsection{Two More Coloring Results}
	
	
	\begin{theorem}
		Let $G$ be a cubic planar graph.
		
		Then the faces can be $4$-colored if and only if the edges can be $3$-colored.
	\end{theorem}
	
	\begin{proof}
		We will consider colorings of the faces, vertices and edges by the four elements of $\mathbb{Z}/2 \oplus \mathbb{Z}/2$.
		
		Then colorings of the faces correspond to elements of $C_2$. And colorings of the edges correspond to elements of $C_1$. (Here we do not yet require that adjacent faces or edges have different colors.)
		
		Let $A \in C_2$ be a coloring of the faces and $B \in C_1$ be a coloring of the edges.
		
		\begin{enumerate}
			\item Adjacent faces have different colors in $A$ if and only if $d_2(A)$ is a coloring of the edges by nonzero elements of $\mathbb{Z}/2 \oplus \mathbb{Z}/2$.
			\item Adjacent edges of $B$ have different nonzero colors if and only if $d_1(B) = 0$.
		\end{enumerate}
		
		
		Suppose that $A$ is a $4$-coloring of the faces. Then by 1 we have that $d_2(A)$ colors the edges with the three nonzero colors. But $d_1 \circ d_2(A) = 0$. So by 2 we have that $d_2(A)$ assigns distinct colors to adjacent edges.
		
		
		For the other direction we use the fact that the plane has no holes so $H_1(G, \mathbb{Z}/2 \oplus \mathbb{Z}/2) = \{0\}$.
		
		Suppose that $B$ is a $3$-coloring of the edges. Then 2 implies that $B \in \ker d_1$ is a cycle. And $H_1(G, \mathbb{Z}/2 \oplus \mathbb{Z}/2) = \{0\}$ means that $B = d_2(A)$ is the boundary of some $A \in C_2$. By 1, adjacent faces of $A$ have different colors.
	\end{proof}
	
	\begin{theorem}
		Let $G$ be a triangulation of a simply connected surface. Then the vertices can be $3$-colored if and only if $G$ is a doodle.
	\end{theorem}
	
	\begin{proof} $ $

$(\Rightarrow) $ First suppose that $G$ is not a doodle. Then $G$ has a vertex $V_1$ of odd degree.

\centerline{\includegraphics[height=3cm]{trianglenotdoodle.png}}

But $V_1$ and its adjacent vertices cannot be 3-colored.


$(\Leftarrow) $ Now suppose that $G$ is a doodle so the faces can be 2-colored.
Orient edges based on the coloring of the faces (so that the the black face is always on the right).

\centerline{\includegraphics[height=4cm]{triangledoodle3.png}}
Now we do cohomology with coefficients in $\mathbb{Z}/3$.
A 1-cocycle is a function on edges. Such a 1-cocycle induces a functions on faces by summing the values on the oriented boundary edges. For example, consider the sum of the coedges $\beta := \sum E_i^{\vee}$ which is the function that takes the value 1 on all edges. Its coboundary vanishes modulo three since the faces are oriented triangles.

Since the space is simply connected, our 1-cocycle $\beta$ must be the coboundary of a 0-cocycle, $\sum E_i^{\vee} = \delta(\alpha)$ for some $\alpha = \sum a_i F_i^{\vee} \in C^0$. This means there is a function $\alpha$ on the vertices such that the difference between its values on adjacent vertices is 1. Therefore the values $a_i \in \mathbb{Z}/3$ of the 0-cocycle give a three coloring of the vertices.
\end{proof}

	
	Using the dual of the graph, the following result can also be obtained.
	
	\begin{theorem}
		Let $G$ be a planar cubic graph. Then the faces can be $3$-colored if and only if the vertices of $G$ can be $2$-colored.
	\end{theorem}
	
	
	
	
	
\end{document}