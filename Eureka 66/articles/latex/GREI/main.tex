\documentclass[9.5pt,headsepline,cleardoubleempty,bibtotoc,tablecaptionabove,english,twoside]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage{multicol}
\usepackage{xcolor}
\usepackage{fontspec}

\setlength{\columnsep}{7mm}
\setlength{\parskip}{3mm}


\newfontfamily\fontheading[SizeFeatures={Size=48}]{MyriadPro-Bold}
\newfontfamily\fontauthor[SizeFeatures={Size=24}]{MyriadPro-Light}
\newfontfamily\fontpagenr[SizeFeatures={Size=14}]{Myriad Pro}
\newfontfamily\fontsubheading[SizeFeatures={Size=16}]{MyriadPro-Bold}
\newfontfamily\fontcapt[SizeFeatures={Size=10}]{MyriadPro-Light}
\newfontfamily\fontfignr[SizeFeatures={Size=10}]{Myriad Pro}

\definecolor{headings}{HTML}{c02555}
\usepackage{libertine}
\usepackage{babel}

\addto{\captionsenglish}{%
  \renewcommand{\refname}{}
}


\usepackage{geometry}
\geometry{a4paper, margin=21mm}

\makeatletter
\newcommand*{\shifttext}[2]{%
  \settowidth{\@tempdima}{#2}%
  \makebox[\@tempdima]{\hspace*{#1}#2}%
}
\makeatother
\setlength{\parindent}{0pt}% Just for this example


\usepackage{fancyhdr}

\renewcommand{\headrulewidth}{0pt}
% \lhead{\shifttext{-15mm}{\fontpagenr\thepage}}
\cfoot{}
\pagestyle{fancy}

% \linespread{1.3}
% \setlength{\parskip}{\baselineskip}%
% \setlength{\parindent}{0pt}%
%generally needs more spacing for legibility

%\usepackage[onehalfspacing]{setspace}%
\title{Smooth Piecewise Polynomials}
\author{Josef Greilhuber}
\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{stmaryrd}
\usepackage{svg}
\usepackage{array}
\usepackage[document]{ragged2e}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}

\usepackage{dirtytalk}
%for adding a comment

\newcommand{\Ls}{L^2(\mathbb{R})}
\newcommand{\La}{L^1(\mathbb{R})}
\newcommand{\C}{\mathbb{C}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\fpf}{\cos (t |\triangledown | )f}
\newcommand{\fpg}{\frac{\sin(t| \triangledown | )}{|\triangledown |}g}
\newcommand{\cint}[4]{\int_{#1}^{#2} #3 \,\, \mathrm{d} #4}
\newcommand{\F}{\mathcal{F}}
%\newtheorem{stz}{Satz}
\newtheorem{cor}{Corollary}
%\newtheorem{lem}{Lemma}
\newtheorem{exa}{Example}
\newtheorem{thm}{Theorem}
\newtheorem{defi}{Definition}
%\theoremstyle{definition}
\newtheorem{rem}{Remark}
%\newtheorem*{bem}{Bemerkung}
%\newtheorem{bsp}{Beispiel}
\newtheorem{prop}{Proposition}
\newtheorem{lem}{Lemma}

\numberwithin{equation}{section}
% Datum der Abgabe
\setmainfont[BoldFont={Minion Pro Bold}, ItalicFont={Minion Pro Italic}]{Minion Pro}

\newcommand{\subheading}[1]{
\vspace{8mm}
{\fontsubheading\textcolor{headings}{#1}}\vspace{2mm}

}

\begin{document}
% \maketitle

{\fontheading\textcolor{headings}{\foreignlanguage{nohyphenation}{\begin{sloppypar}
\baselineskip=48pt
  Smooth Piecewise Polynomials
\end{sloppypar}}}}

\vspace{5mm}

{\fontauthor\textcolor{headings}{
  Josef Greilhuber
}}

\vspace{30mm}

\justify

\begin{multicols}{2}

% \subheading{Introduction}
When I was in my second year at university, my friend Sergey Yurkevich asked the following question:

\begin{quote}
  % {\fontheading\textcolor{headings}{``}}%
Is there a smooth function on $\mathbb{R}$ which is not a polynomial, but for any given point $x\in \mathbb{R}$, the sequence $f^{(n)}(x), n \in \mathbb{N}$ has only finitely many nonzero terms?%
% {\fontheading\textcolor{headings}{''}}
\end{quote}

We spent quite some time on this problem, attempting to construct examples as well as chasing ideas for proofs of the converse, e.g.\ via Baire's theorem. We gained no ground there. But in the search for examples, I found a peculiar function which almost (in the Lebesgue sense) answers the question -- because it is a smooth piecewise polynomial.

\subheading{Construction}
This function turns up most naturally as a solution to a functional equation involving first derivatives, similar to the (somewhat) famous Fabius distribution function \cite{fabius}. We first show that it exists and is unique, then show its piecewise polynomial properties
%moved the second part of the theorem out since it is the focus of the article and aids flow

\begin{thm}

There exists a unique function $f_{c} \in C^1(\R)$ with $f_{c}(x) = 0$ for $x <= 0$, $f_{c}(x) = 1$ for $x \geq 1$ and
\begin{align*}
    f_{c}'(x) = \begin{cases}
      \frac{3}{2} f(3x)  & x< \frac{1}{3} \\
      \frac{3}{2} & \frac{1}{3} \leq x \leq \frac{2}{3} \\
      \frac{3}{2} f(3-3x) & \frac{2}{3} < x.
   \end{cases}
\end{align*}
%since f is used throughout the proof for a general function, it's prudent to  distinguish the function we're interested in. Here I've used f_{c} for cantor

\end{thm}

\begin{proof}
Note that the $f_c$ satisfies $f_c(1-x) = 1 - f_c(x)$. We construct $f_c$ by a fixed-point iteration.

For this, we define $\mathcal{F}: C[0,1] \rightarrow C[0,1]$ as
$$\mathcal{F}f(x) = \frac{3}{2}\int_0^x{f(T(s))} \,\, \mathrm{d} s,$$
where $T:[0,1]\rightarrow [0,1]$ is given by
\begin{align*}
    T(x) = \begin{cases}
    3x & x\leq \frac{1}{3} \\
    1 & \frac{1}{3}\leq x \leq \frac{2}{3} \\
    3-3x & \frac{2}{3} \leq x.
    \end{cases}
\end{align*}

Then if $f_c$ is a fixed point of $\mathcal{F}$, it has the desired derivative on $(0,1)$.
%moved these sections around to cut down on repetition

Let $U$ be the closed affine subspace of $C[0,1]$ satisfying $f(0)=0$ and $f(1-x)=1-f(x)$ for all $f \in U$. Then $U$ is invariant under $\mathcal{F}$.

We observe that $\cint{0}{1}{f(s)}{s} = \frac{1}{2}$, since
\begin{multline*}
\cint{0}{1}{f(s)}{s} = \cint{0}{1}{\left(1-f(1-s)\right)}{s} \\ = 1 - \cint{0}{1}{f(x)}{s},
\end{multline*}
and hence that $\cint{0}{1}{f(T(s))}{s} = \frac{2}{3}$.
\pagebreak

\vspace*{8.72cm}

Now we observe that $\mathcal{F}$ commutes with reflection \linebreak on $[0,1]$:
\begin{align*}
    \F f(1-x) &= \frac{3}{2}\cint{0}{1-x}{f(T(s))\,}{s}\\[1.8em]
    &= \frac{3}{2}\cint{x}{1}{fT(1-t)\,}{t}\\[1.8em]
    &= \frac{3}{2}\cint{x}{1}{fT(t)\,}{t} \\[1.8em]
    &=\frac{3}{2}\cint{0}{1}{fT(t)\,}{t} - \frac{3}{2}\cint{0}{x}{fT(t)\,}{t} \\[1.8em]
    &= 1 - \F f(x),
\end{align*}
and hence $\F U \subseteq U$.\\[2em]

We now show $\F$ is a contraction on $U$, and hence obtain a fixed point. We must show that for $f, g \in U$ there exists $\lambda$ such that
\begin{align*}
    \sup_{x\in[0,1]} \left| \F f(x) - \F g(x) \right| \leq \lambda \sup_{x\in[0,1]} \left| f(x) - g(x) \right|
\end{align*}

But by symmetry, we only need to check for $x \leq \frac{1}{2}$:

\columnbreak

\vspace*{7.8cm}

\begin{align*}
    \left| \F f(x) - \F g(x) \right|
    &= \frac{3}{2}\left | \cint{0}{x}{\left(fT(s)-gT(s)\right)\,}{s} \, \right | \\[1em]
    &\leq \frac{3}{2}\cint{0}{x}{|fT(s)-gT(s)|\,}{s} \\[1em]
    &\leq \frac{3}{2}\cint{0}{\frac{1}{3}}{|fT(s)-gT(s)|\,}{s} \\[1em]
    &= \frac{3}{2}\cint{0}{\frac{1}{3}}{|f(3s)-g(3s)|\,}{s} \\[1em]
    &= \frac{1}{2}\cint{0}{1}{|f(s)-g(s)|\,}{s} \\[1em]
    &\leq \frac{1}{2} \sup_{s\in[0,1]\,} \left| f(s) - g(s) \right|,
\end{align*}
%again added more spacing

So $\F$ is a contraction with $\lambda = \frac{1}{2}$.
By Banach's fixed point theorem, there is a unique function $f_c$ satisfying $\F f_c = f_c$. We obtain our desired function by extending this onto $\mathbb{R}$ in the obvious way.
\end{proof}

Note that $f_c$ is differentiable at $0$, since
\begin{align*}
    f_c(\epsilon) = \frac{3}{2}\cint{0}{\epsilon}{f_c(s)}{s} \sim \epsilon f(0)
\end{align*}
And $f_c'(0) = 0$. By symmetry, $f_c'(1) = 0$, and therefore, $f$ is continuously differentiable, and satisfies all conditions imposed in the statement of the theorem.

\pagebreak

\end{multicols}

\vspace*{1cm}

% \begin{figure}[hb!]
    \centering
    \includegraphics[width=16cm]{fig.png} \\[1cm]
    ${\textcolor{headings}{\blacktriangle}}$\enspace{\fontcapt {\fontfignr Figure 1:} The function $f$ and its first few derivatives (scaled).}
% \end{figure}

\justify

\vspace{1cm}

\begin{multicols}{2}

It now evident that $f$ is in fact smooth, since arbitrary derivatives can be computed piecewise from the functional equation, and $f_c'(0) = f_c'(1) = 0$, so they agree wherever two pieces meet.

The formula $f_c'(x) = \frac{3}{2}f_c(T(x))$ allows us to compute by induction that
\begin{align*}
f_c^{(n)}(x) = \left(\frac{3}{2}\right)^n f(T^n(x)) \; \prod_{k=1}^{n-1}(T^k)'(x)
\end{align*}
%again modifying spacing
wherever $T^k(x)$ is differentiable around $x$ for\linebreak $k=1,\, \dots \, , n-1$. Note that $(T^k)'(x)$ is piecewise constant.

Since $T^n(x) = 1$ for $x \in (\frac{3k+1}{3^n},\frac{3k+2}{3^n})$, the $(n+1)^{st}$ derivative of $f_c$ vanishes on $(\frac{3k+1}{3^n},\frac{3k+2}{3^n})$, implying that $f$ agrees with its
$n^{th}$
Taylor approximation there, which we denote by $p_{n,k}$.

\columnbreak

In other words, for $n, k \in \mathbb{N}$ with $n \geq 1$ and $k < 3^{n-1}$, there exist a polynomial $p_{n,k}$ of degree at most $n$ such that
\begin{align*}
    f_c(x) = p_{n,k}(x) && x \in \left [  \frac{3k+1}{3^n},\frac{3k+2}{3^n} \right ]
\end{align*}

Notably, the set of points where $f$ does not agree with a polynomial of finite degree is exactly the cantor set, and thus on a dense open subset of $\mathbb{R}$ $f_c$ is piecewise polynomial. It can be given as a disjoint union of intervals $I_{n,k}$, where $n \in \mathbb{N}$ and $1 \leq k \leq 2^{n-1}$, together with $(-\infty,0)$ and $(1,\infty)$. On every $I_{n,k}$, there exists a polynomial of degree exactly $n$, such that $f\big|_{I_{n,k}} = p_{n,k}$, justifying the titular remark that $f_c$ really is a smooth piecewise polynomial.

\subheading{References}
\vspace{-4em}
\bibliography{references}
\bibliographystyle{abbrv}

\end{multicols}

\vspace*{0.5cm}

\pagebreak

\end{document}
