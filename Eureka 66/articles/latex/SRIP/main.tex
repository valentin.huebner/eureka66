\documentclass[9.5pt,headsepline,cleardoubleempty,bibtotoc,tablecaptionabove,english,twoside]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage{multicol}
\usepackage{xcolor}
\usepackage{fontspec}



\setlength{\columnsep}{7mm}
\setlength{\parskip}{3mm}

\newfontfamily\fontheading[SizeFeatures={Size=48}]{MyriadPro-Bold}
\newfontfamily\fontauthor[SizeFeatures={Size=24}]{MyriadPro-Light}
\newfontfamily\fontpagenr[SizeFeatures={Size=14}]{Myriad Pro}
\newfontfamily\fontsubheading[SizeFeatures={Size=16}]{MyriadPro-Bold}
\newfontfamily\fontcapt[SizeFeatures={Size=10}]{MyriadPro-Light}
\newfontfamily\fontfignr[SizeFeatures={Size=10}]{Myriad Pro}

\definecolor{headings}{HTML}{2b367c}
% violett #c02555
% dunkelblau #2b367c
% sehr dunkles Grau #231f20
% hellblau #00adbd
% orange #f99d1c
% lila #f99d1c
% grün #62bb46





\usepackage{libertine}
\usepackage{babel}

\addto{\captionsenglish}{%
  \renewcommand{\refname}{}
}


\usepackage{geometry}
\geometry{a4paper, margin=21mm}

\makeatletter
\newcommand*{\shifttext}[2]{%
  \settowidth{\@tempdima}{#2}%
  \makebox[\@tempdima]{\hspace*{#1}#2}%
}
\makeatother
\setlength{\parindent}{0pt}% Just for this example

\newcommand*\diff{\mathop{}\!\mathrm{d}}



\usepackage{fancyhdr}

\renewcommand{\headrulewidth}{0pt}
% \lhead{\shifttext{-15mm}{\fontpagenr\thepage}}
\cfoot{}
\pagestyle{fancy}

% \linespread{1.3}
% \setlength{\parskip}{\baselineskip}%
% \setlength{\parindent}{0pt}%
%generally needs more spacing for legibility

%\usepackage[onehalfspacing]{setspace}%
\title{Magnetic Monopoles in Motion}
\author{Josef Greilhuber}
\usepackage[intlimits]{amsmath}
\usepackage{amsfonts,amssymb,amsthm}
\usepackage{stmaryrd}
\usepackage{svg}
\usepackage{array}
\usepackage[document]{ragged2e}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{calc}

\usepackage{thmtools}
%\newtheorem{stz}{Satz}
\newtheorem{cor}{Corollary}
%\newtheorem{lem}{Lemma}
\newtheorem{exa}{Example}
\newtheorem{thm}{Theorem}
\newtheorem{defi}{Definition}
%\theoremstyle{definition}
\newtheorem{rem}{Remark}
%\newtheorem*{bem}{Bemerkung}
%\newtheorem{bsp}{Beispiel}
\newtheorem{prop}{Proposition}
\newtheorem{lem}{Lemma}

\numberwithin{equation}{section}
% Datum der Abgabe
\setmainfont[BoldFont={Minion Pro Bold}]{Minion Pro}
\setmonofont{Courier New}

\newcommand{\subheading}[1]{
\vspace{8mm}
{\fontsubheading\textcolor{headings}{#1}}\vspace{2mm}

}

\begin{document}

{\fontheading\textcolor{headings}{\foreignlanguage{nohyphenation}{\begin{sloppypar}
\baselineskip=48pt
  Magnetic Monopoles in Motion
\end{sloppypar}}}}

\vspace{5mm}

{\fontauthor\textcolor{headings}{
  Vit Sriprachyakul % University of Durham
}}

\vspace{30mm}

\justify

\begin{multicols}{2}

  In any standard textbooks about quantum mechanics, we can find an
  introduction about the idea of magnetic monopoles and its constraint
  on values of charge, known as Dirac’s quantisation condition.
  However, an approach taken to construct a monopole is rather
  mathematical and non-intuitive. I will approach this in yet another
  way, the whole idea is believed to mainly due to Dirac. Also, I will
  push this model further and explore more properties of magnetic
  monopoles.

Throughout this article we use the following conventions:
The signature is $(+, −, −, −)$,
the unit system is SI, and
the Cartesian coordinate system is right-handed.

\subheading{Magnetic monopoles at rest}

Let me tell you what a standard method is. We start with a vector
potential
\[ \vec{A} = \frac{1- \cos \theta}{r \sin \theta}\hat{\phi}. \]
We then take a curl in spherical polar coordinates to get a magnetic
field
\[ \vec{B} = \frac{\hat{r}}{r^2}, \]
and by multiplying $\vec{A}$ by $\frac{\mu_0q_m}{4 \pi}$, we recover a point-like field,
\[ \vec{B} = \frac{\mu_0q_m}{4 \pi} \frac{\hat{r}}{r^2}. \]

Notice that $\vec{A}$ is not regular everywhere! When $\sin \theta = 0$, i.e.,
$\theta = 0$ or $\pi$, then $\vec{A}$ is singular. When $\theta = 0$, this is cancelled by
the numerator, however this is not the the case for $\theta = \pi$, so the negative
$z$-axis is a genuine singularity. This is crucial since Gauss's law says
that we cannot have monopoles, so, we cannot have monopole field
which is regular everywhere. The singularity along the whole negative
$z$-axis is known as Dirac's string.

\subheading{Dirac's string}

Let us explore this singularity. Consider a semi-infinite solenoid
along the negative $z$-axis. We are going to compute the field it produces.

\vspace{1.13em}
\begin{centering}
\includegraphics[width={\columnwidth-3cm}]{bild1} \\
${\textcolor{headings}{\blacktriangle}}$\enspace{\fontcapt {\fontfignr Figure 1:} A semi-infinite cylindrical solenoid} \\
\end{centering}
\vspace{1.13em}

We will assume that the current is uniform
and flows around the $z$-axis in a positive sense.
We also assume that the solenoid is
cylindrical and its radius is small. Note that if
we divide our solenoid into many thin rings
with thickness $\delta$, then we can approximate each\pagebreak
\end{multicols}

\vspace*{8.12cm}

\begin{multicols}{2}
ring as a magnetic
dipole!

The field produced by an ideal dipole is
\[ \vec{B} = \frac{\mu_0}{4 \pi} \left( \frac{3(\vec{m} \cdot \vec{r}) \vec{r}}{r^5}-\frac {\vec m} {r^3} \right), \]
where $\vec m$ is a dipole moment; in our case, this is $\delta \vec{m} = \sigma \delta z A \hat{z}$ with $A$ a cross-sectional area and $\sigma$ a current per unit length.

In cylindrical polar coordinates, we have
\begin{align*}
  B_z &= \int_{z = 0}^\infty \frac{\mu_0}{4 \pi} \Bigg(
    \frac{3(z+z_0)^2}{((z+z_0)^2+\rho_0^2)^\frac 52} \\
    & \qquad \qquad \qquad - \frac 1 {((z+z_0)^2+\rho_0^2)^\frac 32} \Bigg) \diff m
   \\
  B_z &= \int_{z = 0}^\infty \frac{\mu_0}{4 \pi} \Bigg(
    \frac{3(z+z_0) \rho_0}{((z+z_0)^2+\rho_0^2)^\frac 52} \Bigg) \diff m ,
\end{align*}
where $(z_0, \rho_0, \phi_0)$ is the point at which we evaluate the field and $z$ is
the position of our thin ring, measuring downwards.

This can be easily integrated. If you do not want a hint, skip the next
sentence: Use the substitution $z + z_0 = \rho_0 \tan \theta$, or by parts.

We then end up with
\begin{align*}
  B_z &= \frac{\mu_0}{4 \pi} \frac{\sigma A z_0}{\left(z^2_0 + \rho^2_0 \right)^{\frac 32}} \\
  B_\rho &= \frac{\mu_0}{4 \pi} \frac{\sigma A \rho_0}{\left(z^2_0 + \rho^2_0 \right)^{\frac 32}}
\end{align*}

This is exactly a monopole field! We then identify magnetic charge,
$q_m$, with $\sigma A$, which is a flux inside the solenoid divided by $\mu_0$. We
can show that magnetic charges are Lorentz invariant, just like electric
charges. This is one of the topic we are exploring below.


\subheading{\dots in motion}

Let us take magnetic charges to be the solenoid we discussed above.
We see that the charge is corresponding to the flux through the
interior of the solenoid. Let $S$ be an inertial frame that the solenoid is
at rest and $S'$ be an inertial which sees $S$ fly off with a constant
velocity $\vec{u}$ in their mutual $x$-direction.

\vspace{1em}
\begin{centering}
\includegraphics[width={\columnwidth-3cm}]{bild2} \\
\vspace{-1em}
${\textcolor{headings}{\blacktriangle}}$\enspace{\fontcapt {\fontfignr Figure 2:} The two frames of reference} \\
\end{centering}
\vspace{1em}

For the magnetic charge to be
invariant, we need to prove that the
flux through the solenoid is the
same. We can do this by looking at
how the magnetic and electric
transform under the change of
frames. Note that the solenoid can lie in any direction, with its end
fixed at the origin as shown.\pagebreak
\end{multicols}

\vspace*{12cm}

\begin{multicols}{2}

We have
\begin{align*}
  B_x &= B_x' \\
  \vec{E}' &= - \gamma_u \vec{u} \times \vec{B}.
\end{align*}
We do not need the second equation for now, but it is relevant for
later. We see that if we choose the surface for evaluating the flux to
lie in the mutual $Y - Z$-plane, this surface does not have a length
contraction effect, since it is perpendicular to the relative velocity.
Since the field in the $x$ direction is also the same, we conclude that
the flux is the same in both frames and thus is Lorentz invariant as
claimed.

There are some subtleties to be explained, such as the question ``Is the flux
through our clever surface the same as through the surface normal to
solenoid axis?''. This is easy: since the magnetic field inside the
solenoid lies in the same direction as the solenoid's symmetry axis,
the flux through the two surfaces are the same.

Secondly, what if the solenoid lies in the $Y-Z$-plane? Now we cannot
use our clever surface, but this case is actually simple! We choose our
surface to be the one normal to the symmetry axis. This surface lies in the
$X-Z$-plane and thus suffers a length contraction effect in the $x$-direction,
so the total cross sectional area is $\sigma' = \frac{\sigma}{\gamma_u}$.
But the field in the $y$-direction also transforms $B_y' = \gamma_uB_y$.
These two factors of $\gamma_u$ cancel and we still have the invariant property of magnetic charge!

Now in the velocity limit $|\vec{u}| \ll c$, we can use the transformation for
$\vec{E}$ to see that, in the frame that sees the charge move,
\[ \vec{E} \approx - \vec{u} \times \vec{B} = - \frac{\mu_0}{4 \pi} \frac{q_m \vec{u} \times \hat{r}}{r^2}. \]
This is the Biot-Savart Law for a magnetic charge.

\subheading{Interaction}

For our hypothetical magnetic charges to have a chance to be real
physical charges, the above Lorentz invariant property is expected.
Otherwise, one can send the monopole into motion and its charge
transforms, so we know it is not fundamental. It could even break
Dirac's quantisation as well, since the Lorentz transformation is
continuous and if the charge did transform, it should do so in a
continuous fashion. Dirac's condition however is discrete, so it is bound
to break the quantisation condition.

Nonetheless, this is not the only thing we can assume: we can expect
to derive the interaction with electromagnetic fields as well, which is
our main interest in this section.

Suppose we have two magnetic charges, both at rest, what is the force
between them? We take one of the charge to lie in negative $z$-direction
and this subject to a monopole field from the other. We
again slice our solenoid into thin rings.

The force acting on a dipole is given by
\[ \vec{F} = \nabla (\vec{m} \cdot \vec{B}). \]

Notice that the gradient is differentiating w.r.t.\ the position of the
dipole and this is inconvenient since we need to sum all these tiny forces to get a resultant force and the derivative is differentiating
different points! Luckily, in this case of a monopole field, one can
show that $\nabla = - \nabla_{\vec{R}}$, where $\vec{R}$ is the position vector of the monopole
producing the $\vec{B}$ field. The total force is then
\[ \vec{F} = - \nabla_{\vec{R}} \left( \int \vec{B}\cdot \diff \vec{m} \right) . \]

So we only need to compute the following integral:
\begin{align*}
\int \vec{B} \cdot \diff \vec{m} &= - \int \frac{\mu_0q^2_m}{4\pi}
\frac{(z+z_0)\sigma A \diff z}{((z+z_0)^2 + \rho^2_0)^{\frac 32}} \\
&= - \frac{\mu_0q^1_mq^2_m}{4 \pi R}
\end{align*}
So
\[ \vec{F} = \frac{\mu_0q^1_mq^2_m}{4 \pi R^2} \hat{R} . \]

This is Coulomb's law for magnetic charge. Notice that same-signed charges repulse and
opposite-signed charges attract.
Note also that the
superscripts here are indicators for the charges, not exponents.
By arguing that general magnetic fields are a superposition of
monopole fields, one can deduce that, for a general magnetic field,
\[ \vec{F} = q_m \vec{B} . \]

Next, we are interested in how it interacts with an electric field. Let's
assume for simplicity that we are in a low speed limit. Suppose that in our
frame, the monopole is moving with velocity $\vec{u}$ in a background EM field
$\vec{E}, \vec{B}$. By transforming these fields to an instantaneous rest frame
of the monopole, we get
\[ \vec{B}' \approx \vec{B} - \frac{\vec{u} \times \vec{E}}{c^2} . \]

The expected Lorentz force law is then
\[ \vec{F} = q_m \left(\vec{B} - \frac{\vec{u} \times \vec{E}}{c^2} \right) . \]

The above derivation is approximated, so how can we trust this result?
Well, at least it gives us a hint of what to write down! Since we need
our physics to be consistent with special relativity, we should have
\[ \frac{q_m}{c}G^{\mu \nu}u_\nu = \frac{\diff P^\mu}{\diff \tau} , \]
where $G^{\mu \nu} = \frac 12 \epsilon^{\mu\nu\rho\sigma}$ (i.e.\ $G^{\mu \nu}$ is the Hodge dual of $F^{\mu \nu}$), $u^\mu$ is the 4-velocity of the magnetic monopole, and $P^\mu = mu^\mu$ is the 4-momentum.

If you expand the above equation, you should recover the Lorentz
force law stated previously.

\subheading{Dirac’s quantisation condition}

There is no detailed calculation in this section, only an outline of how
the condition can be derived is presented.

We can take our solenoid model of monopoles, and then try to
observe its presence by using the quantum double-slit interference experiment.
We can calculate the variation in probability amplitude from the
Feynman path integral and the Lagrangian for a particle in an EM field.

We then say, for the monopole to exist, we need the solenoid to be
\emph{not} observable. This is because if we can observe the solenoid, then
we know that the field is not everywhere point-like and thus not a
genuine point charge. This gives a constraint on quantum interference
amplitude and so gives Dirac’s quantisation condition.

\subheading{References}

For more details related to what we use here, you can look up
Aharonov-Bohm effect.

Or, look up my Part III Seminar Series talk on Youtube:
\texttt{https://youtu.be/Z4cfc3MLvL0}

\end{multicols}

\end{document}
