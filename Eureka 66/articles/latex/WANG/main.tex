\documentclass[9.5pt,headsepline,cleardoubleempty,bibtotoc,tablecaptionabove,english,twoside]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage{multicol}
\usepackage{xcolor}
\usepackage{fontspec}

\setlength{\columnsep}{7mm}
\setlength{\parskip}{3mm}

\newfontfamily\fontheading[SizeFeatures={Size=48}]{MyriadPro-Bold}
\newfontfamily\fontauthor[SizeFeatures={Size=24}]{MyriadPro-Light}
\newfontfamily\fontpagenr[SizeFeatures={Size=14}]{Myriad Pro}
\newfontfamily\fontsubheading[SizeFeatures={Size=16}]{MyriadPro-Bold}
\newfontfamily\fontcapt[SizeFeatures={Size=10}]{MyriadPro-Light}
\newfontfamily\fontfignr[SizeFeatures={Size=10}]{Myriad Pro}


\definecolor{headings}{HTML}{ffa044}
% violett #c02555
% dunkelblau #2b367c
% sehr dunkles Grau #231f20
% hellblau #00adbd
% orange #f99d1c
% lila #f99d1c
% grün #62bb46





\usepackage{libertine}
\usepackage{babel}

\addto{\captionsenglish}{%
  \renewcommand{\refname}{}
}


\usepackage{geometry}
\geometry{a4paper, margin=21mm}

\makeatletter
\newcommand*{\shifttext}[2]{%
  \settowidth{\@tempdima}{#2}%
  \makebox[\@tempdima]{\hspace*{#1}#2}%
}
\makeatother
\setlength{\parindent}{0pt}% Just for this example


\usepackage{fancyhdr}

\renewcommand{\headrulewidth}{0pt}
% \lhead{\shifttext{-15mm}{\fontpagenr\thepage}}
\cfoot{}
\pagestyle{fancy}

% \linespread{1.3}
% \setlength{\parskip}{\baselineskip}%
% \setlength{\parindent}{0pt}%
%generally needs more spacing for legibility

%\usepackage[onehalfspacing]{setspace}%
\title{Smooth Piecewise Polynomials}
\author{Josef Greilhuber}
\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{stmaryrd}
\usepackage{svg}
\usepackage{array}
\usepackage[document]{ragged2e}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{calc}

\usepackage{thmtools}
%\newtheorem{stz}{Satz}
\newtheorem{cor}{Corollary}
%\newtheorem{lem}{Lemma}
\newtheorem{exa}{Example}
\newtheorem{thm}{Theorem}
\newtheorem{defi}{Definition}
%\theoremstyle{definition}
\newtheorem{rem}{Remark}
%\newtheorem*{bem}{Bemerkung}
%\newtheorem{bsp}{Beispiel}
\newtheorem{prop}{Proposition}
\newtheorem{lem}{Lemma}

\numberwithin{equation}{section}
% Datum der Abgabe
\setmainfont[BoldFont={Minion Pro Bold}, ItalicFont={Minion Pro Italic}]{Minion Pro}

\newcommand{\subheading}[1]{
\vspace{8mm}
{\fontsubheading\textcolor{headings}{#1}}\vspace{2mm}

}

\begin{document}

{\fontheading\textcolor{headings}{\foreignlanguage{nohyphenation}{\begin{sloppypar}
\baselineskip=48pt
  Braids, Knots and Grids
\end{sloppypar}}}}

\vspace{5mm}

{\fontauthor\textcolor{headings}{
  Jessica P. Wang
}}

\vspace{30mm}

\justify

\begin{multicols}{2}

To a large degree, the beauty of mathematics lies in the deep connections between seemingly different fields. For millennia, knots have been used as a tool for fastening and nautical devices. Today knot theory is known as a significant field in topology. However, it was not until the 1860s when Lord Kelvin, a mathematical physicist, first discovered a theoretical use of knots. He modelled atoms as knots in the aether, which led to the birth of topology after Peter Tait’s work of classifying knots [1]. Later, Emil Artin gave an explicit introduction to braid groups in 1925. I have drawn three diagrams for the cover picture, but they all can be seen as representations of the same thing, the left-hand trefoil knot.

\subheading{Braids}

In simple terms, a \textit{braid} is a collection of strands attached to two opposite ends such that each strand never turns back towards its starting point, and the strings never penetrate one another. Formally, in $\mathbb{R}^{3}$, let $A_{i}=(i,0,1)$, $B_{i}=(i,0,0)$ for $i\in \left \{ 1,\dots ,n \right \}$. An $n$-braid is a configuration of $n$ strands, $d_{i}$, each running from $A_{i}$ to some $B_{j}$, such that:
(i). The strands do not intersect one another;
(ii). Every level plane $P_{s}=\left \{ (x,y,s) \right \}$ parallel to the $xy$-plane intersects each strand at one and only one point.
For example, in the second figure below, the green strand turns upwards at one location, therefore it is not a braid.

\begin{tabular}{cc}
\includegraphics[width=3.6cm]{fig1.jpg} & \includegraphics[width=3.2cm]{fig2.jpg} \\
${\textcolor{headings}{\blacktriangle}}$\enspace{\fontfignr Figure 1} & ${\textcolor{headings}{\blacktriangle}}$\enspace{\fontfignr Figure 2} \\ {\fontcapt A $3$-braid} & {\fontcapt A non-braid}
\end{tabular}

\vspace{1em}

The mathematical definition of a knot is a closed string in $\mathbb{R}^{3}$ which does not self-intersect, and a link is a collection of knots. Two knots are equivalent ($\sim $) if one can be transformed to the other by a finite sequence of \textit{Reidemeister moves}. The four types of Reidemeister moves (Type 0, I, II and III) are shown on the right. Similarly, equivalent braids are ones which can be deformed into one another without cutting any strands.

\vspace{1em}
\begin{centering}
\includegraphics[width={\columnwidth-3cm}]{fig3.png} \\
${\textcolor{headings}{\blacktriangle}}$\enspace{\fontcapt {\fontfignr Figure 3:} The three Reidemeister moves} \\
\end{centering}
\end{multicols}


\vspace*{6.05cm}

\quad
\hspace*{8cm}

\quad \quad \quad \quad \quad \quad \quad \quad \quad \quad \quad \quad \quad \quad \quad \quad \quad \quad \quad \quad ${\textcolor{headings}{\blacktriangle}}$\enspace{\fontcapt {\fontfignr Figure 4:} The $L$-move on a figure-8 knot}

\vspace*{0.5cm}

\begin{multicols}{2}

For simplicity here, let’s refer both knots and links as knots. A braid $\beta $ can be easily transformed into a knot by connecting its corresponding ends, forming a \textit{braid closure / closed braid} $\bar{\beta}$:

\vspace{1em}
\begin{centering}
\includegraphics[width={\columnwidth-1cm}]{fig4.png} \\
\end{centering}
\vspace{0.5em}

	The reverse process, turning a knot into a braid, is less straight-forward. But James Waddell Alexander II told us that it is always possible [2]:

\begin{thm}{Alexander’s Theorem}
  Every knot or link can be transformed into a closed braid.
\end{thm}

	Intuitively, to prove Alexander’s Theorem, one would move all the crossings of a knot to one side in an attempt to construct a braid closure. However, some strands might be oriented upwards. The $L$-move corrects this problem by cutting each upward-running strand into two downward-running strands:

  \vspace{1em}
  \begin{centering}
  \includegraphics[width={\columnwidth-1cm}]{fig5.png} \\
  \end{centering}
  \vspace{0.5em}

	Figure 4 shows this move applied to a figure-8 knot.
  %
  % \vspace{1em}
  % \begin{centering}
  % \includegraphics[width={\columnwidth}]{fig7.png} \\
  % \end{centering}
  % \vspace{0.5em}


	Since a knot can be presented in different ways up to equivalence, applying $L$-moves to the same knot can produce different braids. For example, applying the Type I Reidemeister move to downward -flowing strands allows us to add more strands to the resulting braid:

  \vspace{1em}
  \begin{centering}
  \includegraphics[width={\columnwidth-1cm}]{fig8.png} \\
  \end{centering}
  \vspace{01.05em}

You could connect the corresponding ends of the braid in (5) to see that it is equivalent to the knot in (1). Note that each continuous section of the knot (between under-crossings) which points upwards needs an $L$-move, and the number of $L$-moves required gives the number of strands in the resulting braid. But how do we know that the $L$-moves are valid, i.e. the closure of the resulting braid is always equivalent to the original knot? We can use grid diagrams to prove the validity.

\subheading{Grid Diagrams}
	Knots are generally presented in a curvy structure. This nature means they are hard for a computer to digest. One way to increase computational efficiency is to represent a knot by a grid diagram, which only has vertical and horizontal strands, and two grid moves instead of the Reidemeister moves in knot theory. Grid diagrams are fundamental in defining grid homology [3].
	A \textit{grid diagram} is an $n\times n$ grid with exactly one $\circ $ and one $\times $ in each row and column. It can be transformed into a knot by connecting the $\circ $ and
  $\times $ in each row or column, with vertical lines always over horizontal ones. As for the reverse process, we first draw the knot so that the strands are horizontal and vertical, then arrange the vertical sections to be on top. For example, we have a grid diagram of the left-hand trefoil knot (Can you work out how many $5\times 5$ grid diagrams there are for the left-hand trefoil? Any generalisations?):

  \vspace{1.5em}
  \begin{centering}
  \includegraphics[width={\columnwidth-1cm}]{fig9.jpg} \\
  \end{centering}
  \vspace{2em}

	The grid diagrams are related by two grid moves, namely \textit{(de)-stabilisation} and \textit{communication}:
Stabilisation is the addition of a kink in any direction, while destabilisation is the removal. This preserves the associated knot but changes the grid size. Below shows an example of adding a kink to the top-right of $\times $.

\vspace{1.5em}
\begin{centering}
\includegraphics[width={\columnwidth-1cm}]{fig10.jpg} \\
\end{centering}
\vspace{1.9em}

Communication is the interchange of two consecutive rows or two columns. This preserves the grid size but can change the associated knot. A communication is called admissible if the associated knot is also preserved.

\vspace{1.5em}
\begin{centering}
\includegraphics[width={\columnwidth-1cm}]{fig11.jpg} \\
\end{centering}
\vspace{1.9em}

If we define $\mathcal{G} ={\textup{grid diagrams}}$, $\mathcal{K} ={\textup{equivalence classes of knots}}$, then the map $\mathcal{G} \rightarrow  \mathcal{K}$ is not injective, but we have a bijection
\[
\mathcal{K}\longleftrightarrow \mathcal{G}/\left ( \textit{(de)-stabilisation, adm. comm.} \right ).
\]
	Now, back to our objective of proving Alexander’s Theorem, we introduce the \textit{mid-grid move} [4], which resembles the $L$-move in knot diagrams. It breaks each upward strand into two downward strands, and creates an upward strand on the right edge of the grid. Each move increases the grid size by 2. In this way, a braid closure can be formed.


  \vspace{1em}
  \begin{centering}
  \includegraphics[width={\columnwidth}]{fig12-oben.png} \\[1em]
  \includegraphics[width={\columnwidth}]{fig12-unten.png} \\
  \end{centering}
  \vspace{1em}

\subheading{Proof of Alexander’s Theorem}
	The mid-grid move preserves the knot type as it can be expressed as a sequence of (de)-stabilisation and admissible communication moves. Consider an upward strand at the $i$th column in an $n\times n$ grid, with $\times $ and $\circ $ on the $j$th and $k$th row respectively:\\

\textbf{Step i:} Add kink to the top-right of $\times $;\\

\textbf{Step ii:} Communicate $(n-j)$ times to put $\circ - \times $ at the top;\\

\textbf{Step iii:} Similar to i and ii, add kink to the bottom-right of $\circ $ then communicate $(k-1)$ times to put $\times - \circ $ at the bottom;\\

  \vspace{-1.5em}
  \begin{centering}
  \includegraphics[width={\columnwidth}]{fig13.png} \\
  \end{centering}

\textbf{Step iv:} Communicate $(n-i)$ times to put the $(i+2)$th column to the rightmost of the grid.
Given a knot $K$, by the bijection we can transform it to a grid diagram, which then can be transformed into a closed braid by the mid-grid move.\hfill $\square $

\subheading{The Alexander Polynomial}
We have now proved Alexander’s Theorem. Unsurprisingly, our topologist Alexander also discovered the famous \textit{Alexander polynomial} for knots. It is a \textit{knot invariant}, meaning that if two knots have different Alexander polynomials, then they are not equivalent. But two different knots may have the same Alexander polynomial.

An easy way of computing the Alexander polynomial $\Delta _L(t)$ of a knot $L$ is to first compute the \textit{Alexander-Conway polynomial} $\nabla _L(t)$, which is a modified version of the Alexander polynomial found by John Conway [5]. It is defined by \textit{Conway’s skein relation}:
\[ \nabla _{unknot} (t) = 1,\]
 where the unknot denotes the simple closed loop ‘O’;
\[ \nabla _{L_+} - \nabla _{L_-} = t \cdot \nabla _{L_0}, \]
where $L$ is any knot diagram and $L_+$, $L_-$, $L_0$ are defined by
%
%
$ L_{+} = L$;
$L_-$ for the knot diagram obtained by changing the crossing to a negative;
$L_0$ for the knot diagram obtained by eliminating the crossing such that the orientation is preserved.

  \vspace{0em}
  \begin{centering}
  \includegraphics[width={\columnwidth-1cm}]{fig14.png} \\
  \end{centering}
  \vspace{1.5em}


The Alexander polynomial $\Delta _L(t)$ can then simply be calculated by the relation $$\Delta_L(t^2)=\nabla_L(t-t^{-1}).$$ Try it with the left-hand trefoil knot, you should get $$\Delta_{\text{left-hand trefoil}} (t) = t - 1 + t^{-1}.$$ Note that although the Alexander-Conway polynomial is considered as a simplification of the original Alexander polynomial, the latter has given significant results in knot Floer homology [3].\columnbreak

\subheading{The Alexander Polynomial using Grids}
	As said before, grid diagrams make calculations more computationally possible, and this includes computing the Alexander polynomial. The first step is to %\pagebreak
  construct the \textit{Minesweeper matrix}, which connects \textit{Seifert diagrams} with grid diagrams. As we work through the process, it will become clear that its ‘Minesweeper’ name comes from its unique structure, which looks rather like the well-known computer game.

  A Seifert diagram of a knot is created by eliminating its crossings, which is done via connecting the underpass with the overpass such that the orientation is preserved:

    \vspace{1.5em}
    \begin{centering}
    \includegraphics[width={\columnwidth-3cm}]{fig15.png} \\
    \end{centering}
    \vspace{1.5em}

This process creates closed loops; we’ll call them \textit{Seifert circles}. For our left-hand trefoil, we have:

  \vspace{1.13em}
  \begin{centering}
  \includegraphics[width={\columnwidth-0cm}]{fig16-arrow.png} \\
  \end{centering}
  \vspace{1.5em}

Now, for each Seifert diagram with $n$ Seifert circles, namely $C_{1},\dots,C_{n}$, define the \textit{wind} $\omega (C_{i})$ of $C_{i}$ ($i \in \left \{ 1,\dots,n \right \}$) as follows:
% \begin{itemize}
% \item
If $C_{i}$ is not in the interior of another Seifert circle $C_{j}$ ($i\neq j$), then
% \begin{itemize}
% \item
if $C_{i}$ has a clockwise orientation, $\omega (C_{i})=1$;
% \item
if $C_{i}$ has an anticlockwise orientation, $\omega (C_{i})=-1$.
% \end{itemize}
% \item

If however $C_{i}$ is in the interior of another Seifert circle $C_{j}$ ($i\neq j$), then
% \begin{itemize}
% \item
if $C_{i}$ has a clockwise orientation, $\omega (C_{i})=\omega (C_{j})+1$;
% \item
if $C_{i}$ has an anticlockwise orientation, $\omega (C_{i})=\omega (C_{j})-1$.
% \end{itemize}
% \end{itemize}
So if we take our previous diagram and label the bigger circle $C_1$, the smaller one $C_2$, then we have $\omega (C_1)=1$ and $\omega (C_2)=1+\omega (C_1)=2$.

Call the \textit{$(i,j)$th lattice point} the point where the $i$th row intersects with the $j$th column, counting from the top-left lattice point. For each lattice point in the\end{multicols}

\vspace*{1.22em}

\hspace{8cm}{\fontcapt {\fontfignr Figure 5:} Knots, Grids, and Braids}\enspace${\textcolor{headings}{\blacktriangledown}}$

% \begin{centering}
% \includegraphics[width={\textwidth-4cm}]{fig0.png} \\
% \hspace{6cm}${\textcolor{headings}{\blacktriangle}}$\enspace{\fontcapt {\fontfignr Figure 5:} Knots, Grids, and Braids}
% \end{centering}


\vspace{29.5em}


\begin{multicols}{2}
  grid, we can label it with the wind of the Seifert circle it is located inside. If it is not in any Seifert circles, then we simply write $0$. Each of these are called \textit{the wind of a lattice point}, written as $\omega(i,j)$. Following our left-hand trefoil example, we have


\vspace{1.5em}
  \begin{centering}
  \includegraphics[width={\columnwidth-5cm}]{fig17.png} \\
  \end{centering}
  \vspace{1.5em}

Finally, we have set up the foundation of constructing the Minesweeper matrix! Formally, if we let an $n\times n$ grid diagram $\mathbb{G}$ represent an oriented knot diagram, then the \textit{Minesweeper Matrix} $M(\mathbb{G})$ of $\mathbb{G}$ is an $n \times n$ matrix $(g_{ij})$ constructed as follows:
%\begin{itemize}
% \item
For each $i\leqslant n-1$ and $j\leqslant n-1$, we have $g_{ij}=t^{\omega(i,j)}$;
% \item
at all other points, $g_{ij}=1$.
%\end{itemize}

Basically, $M(\mathbb{G})$ is a square matrix with $t$ taken to the power of the individual winds as its elements, then being added an additional column and row of $1$'s. So denoting the grid diagram of our trefoil knot as $M(\mathbb{G})$ we get the following:
\[M(\mathbb{G})=\begin{pmatrix}
1 & t & t & t & 1\\
t & t^2 & t^2 & t & 1\\
t & t^2 & t & 1 & 1\\
t & t & 1 & 1 & 1\\
1 & 1 & 1 & 1 & 1
\end{pmatrix}\]

Knowing how to create the Minesweeper matrix, we are one step away from calculating the Alexander polynomial. In order to specify the locations of the $\circ $'s and the $\times $'s, we use a systematic approach by letting $\mathbb{O}$ be the set of unit squares containing a $\circ $, and $\mathbb{X}$ be the set of unit squares containing a $\times $. Then, counting from bottom to top and from left to right, suppose a $\circ $ is in the intersection between the $i$th column and the $j_{i}$th row. Let $\sigma _{\mathbb{O}}$ be the permutation which maps $i$ to $j_{i}$, such that $\sigma _{\mathbb{O}}=(
j_{1} \; j_{2} \; \dots \; j_{n}
)$. $\sigma _{\mathbb{X}}$ is defined in a similar fashion. Moreover, let $\epsilon (\mathbb{G})$ denote the sign of permutation which connects $\sigma _{\mathbb{O}}$ with the permutation $(
n \; n-1 \; \dots \; 1
)$. Lastly, consider our previous grid diagram which has the associated winds. Notice that each $\circ $ or $\times $ is contained in a unit square with wind(s) on its four corners. Let $a(\mathbb{G})$ be the sum of all the winds on the four corners of each $\circ $ and $\times $, divided by $-8$. However, remember that our mission is to make things more computationally effective, one way is to let, say, $\mathcal{W}_{1}$ be the set of winds which are in the corner of \textbf{exactly one} $\circ $ or $\times $, $\mathcal{W}_{2}$ be the set of winds which are in the corner of \textbf{exactly two} $\circ $'s or
$\times$'s, and so on. Using this method, we obtain
\[
a(\mathbb{G})=-\frac{1}{8}\cdot \sum_{k=1}^{4}\left ( k \cdot \sum_{\omega (i,j)\in \mathcal{W}_k}\omega (i,j) \right)_.
\]

	Using our definitions above, we get our final result. The Alexander polynomial of a knot $K$ is given by
\[
\Delta _K(t)=\epsilon (\mathbb{G})\cdot (t^{-\frac{1}{2}}-t^{\frac{1}{2}})^{1-n}\cdot \det\left ( M(\mathbb{G}) \right )\cdot t^{a(\mathbb{G})},
\]
where $\mathbb{G}$ is a corresponding grid diagram of $K$ [3].

One way of proving it is to show $\Delta _{K} (t)$ is invariant under admissible communications and (de)-stabilisations. One would also need to prove that it satisfies Conway's skein relation.

\subheading{The left-hand trefoil}

	To summarise, we have proved Alexander’s Theorem, which connects braids with knot theory, by using grid diagrams. Then we showed an alternative way of computing the Alexander polynomial, of which it would be natural to show an example:
	As you would have guessed, we are going to compute the Alexander polynomial of the left-hand trefoil. We need to determine $n$, $\epsilon (\mathbb{G})$, $\det (M(\mathbb{G}))$, and $a(\mathbb{G})$:
% \begin{itemize}
% \item

$n=5$, since we are working on a $5\times 5$ grid diagram.
% \item

$\epsilon (\mathbb{G})$:
From the leftmost column to the rightmost column, the $\circ $'s occupy the $1$st, $2$nd, $3$rd, $4$th and $5$th rows respectively. So we have $\sigma _{\mathbb{O}}=(
1 \; 2 \; 3 \; 4 \; 5
)$.
To obtain the sign of permutation which connects $\sigma _{\mathbb{O}}$ with $(
n \; n-1 \; \dots \; 1
) = (
5 \; 4 \; 3 \; 2 \; 1
)$, notice that the two are equal if we swap $1$ with $5$ and $4$ with $2$. In other words, we would need to find the sign of $(
1 \; 5
)(
4 \; 2
)$. Since this is a permutation with two transpositions, we have $\epsilon (\mathbb{G})=1$.
% \item

$\det (M(\mathbb{G}))$:
We have already computed $M(\mathbb{G})$ earlier, and
\begin{multline*}
\det (M(\mathbb{G})) =t^6-5t^5 +11t^4 \\
-14t^3+11t^2-5t+1.
\end{multline*}
% \item

$a(\mathbb{G})$:
We combine the grid diagram with the winds of its lattice points to give the below figure. For convenience, we label the winds which are in the corner of \textbf{exactly one} $\circ$ or $\times$ in {\color{red}red}; and the wind which are in the corner of \textbf{exactly two} $\circ$ or $\times$ in {\color{blue}blue}:

  \vspace{1.5em}
  \begin{centering}
  \includegraphics[width={\columnwidth-5cm}]{fig18.png} \\
  \end{centering}
  \vspace{1.5em}

From this we obtain
\begin{flalign*}
a(\mathbb{G}) &=-\frac{1}{8}\cdot \sum \textup{numbers in red} \\
   & \phantom{===} -\frac 14 \sum \textup{numbers in blue} \\ &= -\frac{1}{8}\cdot \left ( 6+2\cdot 9 \right ) \\ &= -3.
\end{flalign*}
% \end{itemize}


Thus, we have our desired polynomial:
\begin{flalign*}
  &\Delta_{\text{left-hand trefoil}} (t) = \\
 &=\epsilon (\mathbb{G})\cdot (t^{-\frac{1}{2}}-t^{\frac{1}{2}})^{1-n}\cdot \det\left ( M(\mathbb{G}) \right )\cdot t^{a(\mathbb{G})} \\
&=1 \cdot \frac{t^6-5t^5+11t^4-14t^3+11t^2-5t+1}{(t^{-\frac{1}{2}}-t^{\frac{1}{2}})^{4}} \cdot t^{-3}\\
&= (t^4 -t^3 + t^2)\cdot t^{-3}\\
&= t - 1 + t^{-1},
\end{flalign*}
which matches the result from our earlier method using Conway’s skein relation.

\subheading{References}

\vspace*{-1.8cm}

\bibliographystyle{alpha}
\begin{thebibliography}{\enspace}

	\addcontentsline{toc}{section}{References}


\bibitem[1]{1} Thomson, W. (1869). 4. On Vortex Atoms. Proceedings of the Royal Society of Edinburgh

\bibitem[2]{2} Alexander J. W. (1923). A Lemma on Systems of Knotted Curves. Proceedings of the National Academy of Sciences

\bibitem[3]{3} Ozsvath, P. S., Stipsicz A. I., \& Szabo Z. (2015). Grid Homology for Knots and Links

\bibitem[4]{4} Scherich, N. (2016). A Survey of Grid Diagrams and a Proof of Alexander's Theorem

\bibitem[5]{5} Livingston, C. (1993). Knot Theory. Mathematical Association of America. Cambridge University Press



% \bibitem[1]{CA14}
% 	Carlson G.: {\em Topological pattern recognition for point cloud data}.
% 	Acta Numerica, vol. \textbf{23}, 289--368. 2014.
%
% \bibitem[2]{GH08}
% 	Ghrist R.: {\em Barcodes: The persistent topology of data}.
% 	Bull. Amer. Math. Soc., vol. \textbf{45}, 61--75. 2008.
%
% 	\bibitem[3]{Hat02}
% 	Hatcher A.: {\em Algebraic Topology}.
% 	Cambridge University Press 2002.
%
% \bibitem[4]{UZ15}
% 	Usher M., Zhang J.: {\em Persistent homology and Floer-Novikov theory}.
% 	arXiv:1502.07928. 2015.


\end{thebibliography}
%
\end{multicols}


\end{document}
