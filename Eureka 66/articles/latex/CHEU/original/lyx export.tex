%% LyX 2.3.5.2 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
\documentclass[english]{article}
\usepackage[T1]{fontenc}
\usepackage[latin9]{inputenc}
\usepackage[a5paper]{geometry}
\geometry{verbose,tmargin=1.1cm,bmargin=1.1cm,lmargin=0.8cm,rmargin=0.8cm}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{setspace}
\setstretch{1.3}

\makeatletter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% User specified LaTeX commands.
\usepackage{amsmath}
\usepackage{color}
\usepackage{array}
\usepackage{booktabs}
\usepackage{mathtools}
\definecolor{olive}{rgb}{0,0.8,0}

\makeatother

\usepackage{babel}
\begin{document}
\title{\textbf{Gem of combinatorics - Catalan numbers}}
\author{Trevor Cheung}

\maketitle
\noindent When it comes to the associativity axiom of a group, i.e.
$a\cdot(b\cdot c)=(a\cdot b)\cdot c$, it basically tells us that
(for three elements) the order in which we bracket does not matter
as long as the the elements are not reordered. What about four elements,
or in general $n$ elements? It is not very difficult to prove, by
induction, that all the ways of bracketing gives us the exact same
answer provided that the associativity axiom holds, but there is now
a ``meta-question'':

\medskip
\noindent \begin{center}
\emph{``How many ways are there to bracket $(n+1)$ elements?''}
\par\end{center}

\medskip

\noindent Let's denote the answer to this as $C_{n}$, as it clearly
depends on $n$. Here, the answer makes sense if we have $n\ge2$,
but for reasons that become clear later, we also define

\begin{equation}
{\displaystyle \boxed{C_{0}=C_{1}=1}}
\end{equation}

\noindent We of course also have

\begin{equation}
{\displaystyle \boxed{C_{2}=2}}
\end{equation}
To get a glimpse of how we can solve this for general $n$, let's
see the case where $n=3$. Observe the 5 (i.e. $C_{3}=5$) different
ways of bracketing:

\[
(ab)(cd),a(b(cd),a((bc)d),((ab)c)d,(a(bc))d
\]

\noindent Here, we omit the binary operation symbol for brevity.

\medskip

\noindent Notice that there are mainly three categories in the bracketing
style, depending on \textbf{the position of the last operation}. For
the 1$^{\text{st}}$ way of bracketing, the last operation is \textbf{between
}$b$ \textbf{and $c$}; the 2$^{\text{nd}}$ and 3$^{\text{rd}}$way
of bracketing has its last operation \textbf{between $a$ and $b$};
and the last operation in 4$^{\text{th}}$ and 5$^{\text{th}}$ way
of bracketing is \textbf{between $c$ and $d$}. The reason why there
are 2 ways when the last operation is \textbf{between $a$ and $b$
}or \textbf{between $c$ and $d$ }is that $C_{2}=2$. Let's look
at the case for $n=4$ before we lay down the way to calculate $C_{n}$
in general.

$$\displaystyle \begin{matrix*}[l] \text{Between} &\text{Ways of bracketing} \\ab&a((bc)(de)), a(b(c(de)),a(b((cd)e)),a(((bc)d)e),a((b(cd))e)\\ bc&(ab)(c(de)),(ab)((cd)e) \\cd &(a(bc))(de),((ab)c)(de) \\de & ((ab)(cd))e,(a(b(cd)))e,(a((bc)d))e,(((ab)c)d)e,((a(bc))d)e \end{matrix*}$$

\noindent Again, the reason for having 5 bracketing ways in the 1$^{\text{st}}$
and last row is $C_{3}=5$; while for the 2$^{\text{nd}}$ and 3$^{\text{rd}}$
row, all of the bracketing ways are in groups of 2 and 3. Since there
is no room for bracketing for the group of 2; and 2 ways for bracketing
for the group of 3, each row has 2 elements. From this more involved
example, it is easier to see that, in general,

\begin{equation}
C_{n}=C_{n-1}+C_{n-2}+C_{n-3}C_{2}+...+C_{2}C_{n-3}+C_{n-2}+C_{n-1}
\end{equation}

\noindent So the reason why we have to define $C_{0}=C_{1}=1$ is,
simply, that we want to have a neater expression of $C_{n}$:

\begin{equation} \displaystyle C_{n}=\sum_{r=1}^{n-1}C_{r-1}C_{n-r} \label{eq1}\end{equation}

\noindent To obtain the general form of $C_{n}$, i.e. not recursively,
we can use the generating function of $C_{n}$. Let's define

\begin{equation}
f(x):=\sum_{n=0}^{\infty}C_{n}x^{n}
\end{equation}

\noindent To use the recurrence relation in \eqref{eq1}, we can consider
$[f(x)]^{2}$.

\noindent \begin{align} [f(x)]^2&= \sum_{n=0}^{\infty} \left (C_nC_0+C_{n-1}C_1+\cdots + C_0C_n\right ) x^n \nonumber \\ & = \sum_{n=0}^{\infty}C_{n+1}x^n \nonumber \\ & = \sum_{n=1}^{\infty} C_nx^{n-1} \nonumber \\ & = \frac 1x \sum_{n=1}^{\infty} C_nx^n \nonumber \\ & = \frac 1x\left (\sum_{n=0}^{\infty} C_nx^n - 1\right ) \nonumber \\  x[f(x)]^2&= f(x)-1 \nonumber \\  x[f(x)]^2-f(x)+1&=0 \label{eq2} \end{align}

\noindent Observe that \eqref{eq2} is quadratic in $f(x)$, and hence
using quadratic formula, we have

\begin{equation}
{\displaystyle f(x)=\dfrac{1-\sqrt{1-4x}}{2x}}
\end{equation}

\noindent as the other solution has no limit when $x\to0$. To recover
$C_{n}$, simply use Taylor (or binomial) series of $f(x)$. To this
end, note that

\noindent \begin{align}(1-4x)^{\frac 12} & = 1+\left (\frac 12\right )(-4x) + \dfrac {\left (\frac 12\right )\left (-\frac 12\right )}2(-4x)^2 + \dfrac {\left (\frac 12 \right )\left (-\frac 12 \right ) \left (-\frac 32\right )}{3!} (-4x)^3 + \cdots \nonumber \\ & = 1-2x+\sum_{r=2}^{\infty} (-1)^{2r-1}\frac {1\cdot 1\cdot 3 \cdot 5 \cdots (2r-3)4^r}{2^r r!}x^r \nonumber \\ & = 1-2x-2\sum_{r=2}^{\infty} \frac {(2r-2)!}{(r!r-1)!}x^r \nonumber \\  1-\sqrt{1-4x} & = 2x+2\sum_{r=2}^{\infty} \frac 1r\binom{2r-2}{r-1} x^r \nonumber \\ & = 2\sum_{r=1}^{\infty} \frac 1r \binom{2n-2}{r-1}x^r\nonumber \\ & = 2\sum_{r=0}^{\infty} \frac 1{r+1} \binom{2r}r x^{r+1} \nonumber \\ f(x)=\frac {1-\sqrt{1-4x}}{2x} & = \sum_{r=0}^{\infty} \frac 1{r+1}\binom{2r}r x^r  \label{eq3} \end{align}

\noindent And by simply comparing the coefficients, we have

\begin{equation}
C_{n}=\dfrac{1}{n+1}\binom{2n}{n}
\end{equation}

\noindent This class of numbers is called \textbf{Catalan numbers}.
Apart from being simply a ``meta-question'' of the associativity
axiom of groups, it is of vital importance to combinatorics because
of its ubiquity in combinatorial problems. Here are two other very
different-looking problems that also have $C_{n}$ as the answer:

\medskip

\emph{(i) How many ways are there to construct a northeasterly path
(each step sees an increase in either $x$ or $y$ coordinates) from
$(0,0)$ to $(n,n$) that does not go above the line $y=x$? }\label{q1}

\emph{(ii) How many ways are there to triangulate a regular $(n+2)$-gon
(i.e. slice the polygon into $n$ triangles using diagonals)? }\label{q2}

\medskip

\noindent We will illustrate this diagrammatically:

\medskip

\includegraphics[scale=0.4,bb = 0 0 200 100, draft, type=eps]{Grids.png}
\noindent \begin{center}
\emph{Figure 1: Illustration of question (i)}
\par\end{center}

\noindent Here, the {\color{red} red} line is {\color{red}$y=x$},
and the {\color{olive} green} path does not cross over the {\color{red} red}
line. Question (i) \ref{q1} simply ask how many such {\color{olive} green}
paths there are.

\medskip

\noindent It is possible to create a bijection between a path in this
question, and a way of bracketing $(n+1)$ elements. For example,
the path above corresponds to this way of bracketing 6 elements: $(a(b(cd))(ef)$.
To obtain the path from the bracketing, we first construct a step
to the right (no matter what the bracketing is), then go to the right
if there is an open bracket, upwards if there is a(n implicit) binary
operation. This is a bijection because it is possible to do these
in reverse.

\bigskip
\noindent \begin{center}
\includegraphics[scale=0.4,bb = 0 0 200 100, draft, type=eps]{Triangulation.png}
\par\end{center}

\noindent \begin{center}
\emph{Figure 2: Illustration of question (ii)}
\par\end{center}

\noindent The above is a triangulation of a pentagon mentioned in
question (ii) \ref{q2}, but it is not easy to create a bijection,
but we can verify the recurrence relation. $C_{2}=2$ is evident from
the fact there are two diagonals of a square; while the recurrence
relation can be explained as follows: for an $(n+2)$-gon, we first
construct 2 diagonals from 1 vertex forming 1 triangle,. It then divides
into a $(k+1)$-gon, and a $(n-k+2)$-gon. Then we can write

\[
{\displaystyle C_{n}=\sum_{k=1}^{n-1}C_{k-1}C_{n-k}}
\]

\noindent So with the exact same initial values and recurrence relation,
they produce the same set of answers for all $n$.

\noindent \bigskip

\noindent One of the most interesting properties of Catalan numbers
is its Hankel transform, which is a transformation of an infinite
(integer) sequence $a_{1},a_{2},a_{3}...$ into another infinite (integer)
sequence $h_{1},h_{2},h_{3,}...$, via the following:

$$h_n=\det\begin{pmatrix}a_1&a_2&\cdots & a_n\\a_2 & a_3&\cdots & a_{n+1} \\ \vdots & \vdots & \ddots & \vdots \\ a_n&a_{n+1}&\cdots & a_{2n-1}\end{pmatrix}$$

\noindent For example, the Hankel transform of Fibonacci numbers $f_{1},f_{2},f_{3},...$
is the constant sequence $1,0,0,...$. We can prove this easily by
observing that each row must be a linear combination of the previous
two rows (except for the first Hankel determinant).

\noindent Very interestingly, the Hankel transform of $C_{0},C_{1},C_{2},C_{3},...$
as well as $C_{1},C_{2},C_{3},...$ are the constant sequence $1,1,1,...$.
It is interesting in three different ways:

\medskip

\noindent (a) It is not guaranteed that by shifting the sequence,
the Hankel transform would still be the same (e.g. $C_{2},C_{3},C_{4},...$
does not have the Hankel transform of a constant sequence {[}although
it is actually a sequence we all know and love{]}) , but the two aforementioned
give the same Hankel transform

\noindent (b) The fact that the determinants stay the same when we
add more entries is pretty intriguing

\noindent (c) Although Fibonacci sequence also fulfills both (a) and
(b) {[}except that it is \emph{almost} a constant sequence{]}, it
is only because of our construction of the sequence; but for Catalan
numbers, it is not obvious that its Hankel transform is a constant
sequence even from its recurrence relation (which at first glance
does seem to be the key to prove it, but it is not).

\medskip

\noindent It is possible to use elementary combinatorics to explain
why the Hankel transform of Catalan numbers is a constant sequence,
but it is quite involved to be put in a short article. Needless to
say, Catalan numbers are a gem to combinatorics, solving many seemingly
unrelated combinatorial problems, as well as having interesting properties.
\end{document}
