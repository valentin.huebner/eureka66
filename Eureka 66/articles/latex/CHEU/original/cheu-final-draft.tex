%% LyX 2.3.5.2 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
\documentclass[english]{article}
\usepackage[T1]{fontenc}
\usepackage[latin9]{inputenc}
\usepackage[a5paper]{geometry}
% \geometry{verbose,tmargin=1.1cm,bmargin=1.1cm,lmargin=0.8cm,rmargin=0.8cm}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{setspace}
\setstretch{1.3}

\makeatletter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% User specified LaTeX commands.
\usepackage{amsmath}
\usepackage{color}
\usepackage{array}
\usepackage{booktabs}
\usepackage{mathtools}
\definecolor{olive}{rgb}{0,0.8,0}

\makeatother

\usepackage{babel}
\begin{document}
\title{\textbf{The Gem of Combinatorics}}
\author{Trevor Cheung}

\maketitle
 The associativity axiom of group theory is something so familiar to us all that we
seldomly think about it.
\[ a\cdot(b\cdot c)=(a\cdot b)\cdot c \]
It basically tells us that for three elements,
the order in which we bracket them does not matter as long as the they are not
reordered. What about four elements,
or in general $n$ elements? It is not very difficult to deduce from the axiom using induction that all the ways of bracketing are equivalent.
Here, we ask something of a ``meta-question'':

\medskip
 \begin{center}
\emph{``How many ways are there to bracket $(n+1)$ elements?''}
\par\end{center}

\medskip

 Let's denote the answer to this as $C_{n}$, as it clearly
depends on $n$. Although the above definition is most clear for $n\ge2$,
we interpret it also to mean

\[
{\displaystyle \boxed{C_{0}=C_{1}=1}} .
\]

 Next, we have

\[
{\displaystyle \boxed{C_{2}=2}}.
\]
To get a glimpse of how we can solve the question for general $n$, let's
consider the case of $n=3$. With a bit of thinking, we find $C_{3}=5$ different
ways of bracketing:
\[
(ab)(cd)   \qquad   a(b(cd)   \qquad   a((bc)d)   \qquad   ((ab)c)d   \qquad   (a(bc))d
\]

 From here, we omit the binary operation symbol for brevity.

\medskip

 Notice that we can put these bracketing styles into three categories, depending on the position of the outermost operation. With $(ab)(cd)$, the last operation is between
$b$ and $c$;
with $a(b(cd)$ and $a((bc)d)$, it is between $a$ and $b$;
with $((ab)c)d$ and $(a(bc))d$, the outermost operation is between $c$ and $d$.
The reason why there
are exactly two ways when the last operation is between $a$ and $b$
or between $c$ and $d$ is that $C_{2}=2$: that's in how many ways we can bracket the remaining triple.

Let's look
at the case of $n=4$ before we lay down the way to calculate $C_{n}$
in general:

$$\displaystyle \begin{matrix*}[l] \text{Between} &\text{Ways of bracketing} \\ab&a((bc)(de)), a(b(c(de)),a(b((cd)e)),a(((bc)d)e),a((b(cd))e)\\ bc&(ab)(c(de)),(ab)((cd)e) \\cd &(a(bc))(de),((ab)c)(de) \\de & ((ab)(cd))e,(a(b(cd)))e,(a((bc)d))e,(((ab)c)d)e,((a(bc))d)e \end{matrix*}$$

 Again, the reason for having 5 ways of bracketing in the first
and last row is $C_{3}=5$, while for the second and third
row, all of the bracketing ways are in groups of 2 and 3. Since there
is no room for bracketing for the group of 2; and 2 ways for bracketing
for the group of 3, each row has 2 elements. From this more involved
example, it is easier to see that, in general,
\[
C_{n}= C_0 C_{n-1} + \dots + C_{n-1} C_0
\]
or equivalently
\begin{equation} \displaystyle C_{n}=\sum_{r=1}^{n-1}C_{r-1}C_{n-r}. \label{eq1}
\end{equation}

 To obtain a closed expression for $C_{n}$ from \eqref{eq1},
we can use the generating function of $C_{n}$. Let's define
\[
f(x):=\sum_{n=0}^{\infty}C_{n}x^{n}.
\]

 and consider
$[f(x)]^{2}$:

 \begin{align} [f(x)]^2&= \sum_{n=0}^{\infty} \left (C_nC_0+C_{n-1}C_1+\cdots + C_0C_n\right ) x^n \nonumber \\ & = \sum_{n=0}^{\infty}C_{n+1}x^n \nonumber \\ & = \sum_{n=1}^{\infty} C_nx^{n-1} \nonumber \\ & = \frac 1x \sum_{n=1}^{\infty} C_nx^n \nonumber \\ & = \frac 1x\left (\sum_{n=0}^{\infty} C_nx^n - 1\right ) \nonumber  \\  x[f(x)]^2&= f(x)-1 \nonumber \\  x[f(x)]^2-f(x)+1&=0 \label{eq2} \end{align}

 We can apply the quadratic formula to \eqref{eq2} and obtain
\[
{\displaystyle f(x)=\dfrac{1-\sqrt{1-4x}}{2x}},
\]

 as the other solution diverges when $x\to0$. To recover
$C_{n}$, we will use the Taylor (or binomial) series of $f(x)$: Note that

 \begin{align}
  (1-4x)^{\frac 12} & = \sum_{r=0}^\infty \frac{\left (\frac 12\right )\left (-\frac 12\right )\dots\left (-\frac {2r-3}2\right )}{r!}(-4x)^r \nonumber \\
  & = 1 + \sum_{r=1}^{\infty} (-1)^{2r-1}\frac {1\cdot 1\cdot 3 \cdot 5 \cdots (2r-3)}{2^r r!}(4x)^r \nonumber \\
  & = 1 - 2\sum_{r=1}^{\infty} \frac {(2r-2)!}{r!(r-1)!}x^r \nonumber \\
  1-\sqrt{1-4x} & = 2\sum_{r=1}^{\infty} \frac {(2r-2)!}{r!(r-1)!}x^r \nonumber \\
  & = 2\sum_{r=1}^{\infty} \frac 1r \binom{2n-2}{r-1}x^r\nonumber \\ & = 2\sum_{r=0}^{\infty} \frac 1{r+1} \binom{2r}r x^{r+1} \nonumber \\ f(x)=\frac {1-\sqrt{1-4x}}{2x} & = \sum_{r=0}^{\infty} \frac 1{r+1}\binom{2r}r x^r  \label{eq3} \end{align}

 And by simply comparing the coefficients, we have
\[
C_{n}=\dfrac{1}{n+1}\binom{2n}{n}.
\]

 This number sequence is called the \textbf{Catalan numbers}.
Far from just a curious question related to the associativity
axiom of group theory, it is of vital importance to combinatorics because
of its ubiquity in combinatorial problems. Here are two other very
different-looking problems that also have $C_{n}$ as the answer:

\medskip

\emph{(i) How many northeasterly paths
(we go north or east in each step) from
$(0,0)$ to $(n,n)$ are there not crossing the line $x=y$? }

\emph{(ii) How many ways are there to triangulate a regular $(n+2)$-gon
(i.e.\ slice the polygon into $n$ triangles using a selection of diagonals)? }\label{q2}

\medskip

 We will illustrate this diagrammatically:

\medskip

 \begin{center}
\includegraphics[width=10cm]{Grids.png}
\emph{Figure 1: Illustration of question (i)}
\par\end{center}

 Here, the red line is $x=y$,
and the green path does not cross the red line
line. Question (i) simply asks how many such green
paths there are.

\medskip

 It is possible to create a bijection between paths in this
question and ways of bracketing $(n+1)$ elements.
To obtain the corresponding path from a bracketing, we first move one step
to the right (no matter what the bracketing is), then traverse the bracketing (as a string of symbols), going to the right
whenever we encounter an opening bracket and upwards whenever we encounter a binary
operation. It is possible to reverse this construction, so it is bijective.

For example,
the path above corresponds to this way of bracketing 6 elements: $(a\cdot(b\cdot (c \cdot d)) \cdot (e \cdot f)$.

\bigskip
 \begin{center}
\includegraphics[width=10cm]{Triangulation.png}
\par\end{center}

 \begin{center}
\emph{Figure 2: Illustration of question (ii)}
\par\end{center}

 The above figure shows several triangulations of a pentagon, as mentioned in Question (ii).
 It would again be possible to construct a bijection, but we will instead verify that the recurrence relation holds. Technically, there is no $2$-gon, but $C_3 = 1$ conforms to a triangle being already triangulated, while the recurrence
relation can be explained as follows: For a general triangulisation $(n+2)$-gon, we choose one special edge and consider the triangle that it is adjacent to. This triangle splits the $(n+2)$-gon into a $(k+1)$-gon and a $(n-k+2)$-gon, each with independent and arbitrary triangulations. So the number of triangulations satisfies
\[
{\displaystyle C_{n}=\sum_{k=1}^{n-1}C_{k-1}C_{n-k}}.
\]
 It has the same initial values and recurrence relation as the Catalan numbers,
hence is identical for all $n$.

 \bigskip

 One of the most interesting properties of Catalan numbers
is their Hankel transform. The Hankel transform is a transformation of an infinite
(integer) sequence $a_{1}, a_{2}, a_{3}, \dots$ to another infinite (integer)
sequence $h_{1},h_{2},h_{3,}, \dots$ by the following rule:

$$h_n=\det\begin{pmatrix}a_1&a_2&\cdots & a_n\\a_2 & a_3&\cdots & a_{n+1} \\ \vdots & \vdots & \ddots & \vdots \\ a_n&a_{n+1}&\cdots & a_{2n-1}\end{pmatrix}$$

 For example, the Hankel transform of the Fibonacci numbers $1, 1, 2, 3, \dots$
is the eventually constant sequence $1,1,0,0, \dots$. We can easily prove this for $n \ge 3$ by
observing that each row is a linear combination of the previous
two.

 Very interestingly, the Hankel transform of $C_{0},C_{1},C_{2},C_{3}, \dots$
as well as $C_{1},C_{2},C_{3}, \dots$ is the constant sequence $1,1,1, \dots$.
This is interesting for a number of reasons:

\medskip

 (a) It is not guaranteed that by shifting the sequence,
the Hankel transform is still the same (e.g. $C_{2},C_{3},C_{4}, \dots$
does not have the Hankel transform of a constant sequence (although
it is actually a sequence we all know and love!).

 (b) The fact that the determinants stay the same when we
add more entries is pretty intriguing.

 (c) Although the Fibonacci sequence also fulfills both (a) and
(b) (except that it is only an \emph{almost} constant sequence), this
is clearly to be expected from our construction of the sequence; but for Catalan
numbers, it is not obvious that its Hankel transform is a constant
sequence even from the recurrence relation (which at first glance
does seem to be the key to prove it, but actually is not).

\medskip

 It is possible to use elementary combinatorics to explain
why the Hankel transform of the Catalan numbers is a constant sequence,
but this is too involved to be put in a short article. Needless to
say, Catalan numbers are a gem to combinatorics, solving many seemingly
unrelated combinatorial problems, as well as having interesting properties.
\end{document}
