\documentclass[9.5pt,headsepline,cleardoubleempty,bibtotoc,tablecaptionabove,english,twoside]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage{multicol}
\usepackage{xcolor}
\usepackage{fontspec}

\setlength{\columnsep}{7mm}
\setlength{\parskip}{3mm}

\newfontfamily\fontheading[SizeFeatures={Size=48}]{MyriadPro-Bold}
\newfontfamily\fontauthor[SizeFeatures={Size=24}]{MyriadPro-Light}
\newfontfamily\fontpagenr[SizeFeatures={Size=14}]{Myriad Pro}
\newfontfamily\fontsubheading[SizeFeatures={Size=16}]{MyriadPro-Bold}
\newfontfamily\fontcapt[SizeFeatures={Size=10}]{MyriadPro-Light}
\newfontfamily\fontfignr[SizeFeatures={Size=10}]{Myriad Pro}

\definecolor{headings}{HTML}{2b367c}
% violett #c02555
% dunkelblau #2b367c
% sehr dunkles Grau #231f20
% hellblau #00adbd
% orange #f99d1c
% lila #f99d1c
% grün #62bb46





\usepackage{libertine}
\usepackage{babel}

\addto{\captionsenglish}{%
  \renewcommand{\refname}{}
}


\usepackage{geometry}
\geometry{a4paper, margin=21mm}

\makeatletter
\newcommand*{\shifttext}[2]{%
  \settowidth{\@tempdima}{#2}%
  \makebox[\@tempdima]{\hspace*{#1}#2}%
}
\makeatother
\setlength{\parindent}{0pt}% Just for this example


\usepackage{fancyhdr}

\renewcommand{\headrulewidth}{0pt}
% \lhead{\shifttext{-15mm}{\fontpagenr\thepage}}
\cfoot{}
\pagestyle{fancy}

% \linespread{1.3}
% \setlength{\parskip}{\baselineskip}%
% \setlength{\parindent}{0pt}%
%generally needs more spacing for legibility

%\usepackage[onehalfspacing]{setspace}%
\title{Smooth Piecewise Polynomials}
\author{Josef Greilhuber}
\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{stmaryrd}
\usepackage{svg}
\usepackage{array}
\usepackage[document]{ragged2e}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{calc}
\usepackage{mathtools}

\usepackage{thmtools}
%\newtheorem{stz}{Satz}
\newtheorem{cor}{Corollary}
%\newtheorem{lem}{Lemma}
\newtheorem{exa}{Example}
\newtheorem{thm}{Theorem}
\newtheorem{defi}{Definition}
%\theoremstyle{definition}
\newtheorem{rem}{Remark}
%\newtheorem*{bem}{Bemerkung}
%\newtheorem{bsp}{Beispiel}
\newtheorem{prop}{Proposition}
\newtheorem{lem}{Lemma}


\numberwithin{equation}{section}
\usepackage{chngcntr}
\counterwithout{equation}{section}
% Datum der Abgabe
\setmainfont[BoldFont={Minion Pro Bold}]{Minion Pro}

\newcommand{\subheading}[1]{
\vspace{8mm}
{\fontsubheading\textcolor{headings}{#1}}\vspace{2mm}

}

\begin{document}

{\fontheading\textcolor{headings}{\foreignlanguage{nohyphenation}{\begin{sloppypar}
\baselineskip=48pt
  The Gem of Combinatorics
\end{sloppypar}}}}

\vspace{5mm}

{\fontauthor\textcolor{headings}{
  Trevor Cheung % University of Durham
}}

\vspace{30mm}

\justify

\begin{multicols}{2}


   The associativity axiom of group theory is something so familiar to all of us that we
  seldom think about it.
  \[ a\cdot(b\cdot c)=(a\cdot b)\cdot c \]
  It basically tells us that for three elements,
  the order in which we bracket them does not matter as long as they are not
  reordered. What about four elements,
  or in general $n$ elements? It is not very difficult to deduce from the axiom using induction that all the ways of bracketing are equivalent.
  Here, we ask something of a ``meta-question'':


   \begin{center}
  \emph{``How many ways are there to \\ bracket $(n+1)$ elements?''}
  \par\end{center}



   Let's denote the answer to this as $C_{n}$, as it clearly
  depends on $n$. Although the above definition is most clear for $n\ge2$,
  we interpret it also to mean
  \[
C_{0}=C_{1}=1 .
  \]
   Next, we have
  \[
  C_{2}=2.
  \]
  To get a glimpse of how we can solve the question for general $n$, let's
  consider the case of $n=3$. With a bit of thinking, we find $C_{3}=5$ different
  ways of bracketing:
  \begin{center}
  (ab)(cd)   \qquad   a(b(cd)   \qquad   a((bc)d)   \\[1em]   ((ab)c)d   \qquad   (a(bc))d
\end{center}

   From here, we omit the binary operation symbol for brevity.



   Notice that we can put these bracketing styles into three categories, depending on the position of the outermost operation. With $(ab)(cd)$, the last operation is between
  $b$ and $c$;
  with $a(b(cd)$ and $a((bc)d)$, it is between $a$ and $b$;
  with $((ab)c)d$ and $(a(bc))d$, the outermost operation is between $c$ and $d$.
  The reason why there
  are exactly two ways when the last operation is between $a$ and $b$
  or between $c$ and $d$ is that $C_{2}=2$: that's in how many ways we can bracket the remaining triple.

  \setlength\arraycolsep{0.5cm}

  Let's look
  at the case of $n=4$ before we lay down the way to calculate $C_{n}$
  in general:
  $$\displaystyle \begin{matrix*}[l] \text{Category} &\text{Bracketing} \\

  a \cdot b &       a \cdot ((bc)(de)) \\ & a \cdot (b(c(de))) \\ & a \cdot (b((cd)e)) \\ & a \cdot (((bc)d)e) \\ & a \cdot ((b(cd))e)\\
  b \cdot c &       (ab) \cdot (c(de)) \\ & (ab) \cdot ((cd)e) \\
  c \cdot d &       (a(bc)) \cdot (de) \\ & ((ab)c) \cdot (de) \\
  d \cdot e &       ((ab)(cd)) \cdot e \\ & (a(b(cd))) \cdot e \\ & (a((bc)d)) \cdot e \\ & (((ab)c)d) \cdot e \\ & ((a(bc))d) \cdot e

  \end{matrix*}$$

   Again, the reason for having 5 ways of bracketing in the first
  and last category is $C_{3}=5$. For the second and third
  category, where we break the five elements into a group of 2 and a group and 3, we have
  just one way of bracketing the 2 elements, but 2 ways for the 3 elements. So these categories each have $C_1 \cdot C_2 = 2$ elements. From this more involved
  example it is easy to see that, in general,
  \[
  C_{n+1}= C_0 C_{n} + \dots + C_{n} C_0,
  \]
  or equivalently
  \begin{equation} \displaystyle C_{n+1}=\sum_{r=0}^{n}C_{r}C_{n-r}. \label{eq1}
  \end{equation}

  \subheading{A closed expression}

   To obtain a closed expression for $C_{n}$ from \eqref{eq1},
  we can use the generating function of $C_{n}$. Let's define
  \[
  f(x):=\sum_{n=0}^{\infty}C_{n}x^{n}.
  \]

   and consider
  $[f(x)]^{2}$:
   \begin{align} [f(x)]^2&= \sum_{n=0}^{\infty} \big( C_0C_n+C_1C_{n-1} \nonumber \\
     &\qquad\quad + \cdots + C_nC_0 \big)\,x^n \nonumber \\ & = \sum_{n=0}^{\infty}C_{n+1}x^n \nonumber \\ & = \sum_{n=1}^{\infty} C_nx^{n-1} \nonumber \\ & = \frac 1x \sum_{n=1}^{\infty} C_nx^n \nonumber \\ & = \frac 1x\left (\sum_{n=0}^{\infty} C_nx^n - 1\right ) \nonumber  \\  x[f(x)]^2&= f(x)-1 \nonumber \\  x[f(x)]^2-f(x)+1&=0 \label{eq2} \end{align}

   We can apply the quadratic formula to \eqref{eq2} and obtain
  \[
  {\displaystyle f(x)=\dfrac{1-\sqrt{1-4x}}{2x}},
  \]
   as the other solution diverges when $x\to0$. To recover
  $C_{n}$, we will use the Taylor (or binomial) series of $f(x)$: Note that

   \begin{align}
    &(1-4x)^{\frac 12} = \nonumber \\
    & \quad = \sum_{r=0}^\infty \frac{\left (\frac 12\right )\left (-\frac 12\right )\dots\left (-\frac {2r-3}2\right )}{r!}(-4x)^r \nonumber \\
    & \quad = 1 + \sum_{r=1}^{\infty} (-1)^{2r-1}\frac {1\cdot 1\cdot 3 \cdot 5 \cdots (2r-3)}{2^r r!}(4x)^r \nonumber \\
    & \quad = 1 - 2\sum_{r=1}^{\infty} \frac {(2r-2)!}{r!(r-1)!}x^r \nonumber \\
    &1-\sqrt{1-4x} = \nonumber \\
    & \quad = 2\sum_{r=1}^{\infty} \frac {(2r-2)!}{r!(r-1)!}x^r \nonumber \\
    & \quad = 2\sum_{r=1}^{\infty} \frac 1r \binom{2r-2}{r-1}x^r\nonumber \\
    & \quad = 2\sum_{r=0}^{\infty} \frac 1{r+1} \binom{2r}r x^{r+1} \nonumber \\
    &f(x) =  \frac {1-\sqrt{1-4x}}{2x} = \nonumber \\
    & \quad = \sum_{r=0}^{\infty} \frac 1{r+1}\binom{2r}r x^r  \label{eq3}
  \end{align}

   And by simply comparing the coefficients, we have
  \[
  C_{n}=\dfrac{1}{n+1}\binom{2n}{n}.
  \]

  \subheading{The Catalan numbers}

   This number sequence is called the \textbf{Catalan numbers}.
  Far from just a curious question related to the associativity
  axiom of group theory, it is of vital importance to combinatorics because
  of its ubiquity in combinatorial problems. Here are two other, very much
  different-looking problems that also have $C_{n}$ as the answer:



  \emph{(i) How many north-easterly paths
  (paths that go north or east in each step) from
  $(0,0)$ to $(n,n)$ are there not crossing the line $x=y$? }

  \emph{(ii) How many ways are there to triangulate a regular $(n+2)$-gon
  (i.e.\ slice the polygon into $n$ triangles using a selection of non-intersecting diagonals)? }\label{q2}



Question (i) can be illustrated with a diagram:
   In Figure 1, the red line is $x=y$,
  and the green path does not cross the red line
  line. Question (i) simply asks how many such green
  paths there are.
     \begin{center}
  \includegraphics[width=7cm]{Grids.png}\\
  ${\textcolor{headings}{\blacktriangle}}$\enspace{\fontcapt {\fontfignr Figure 1:} Illustration of Question (i)}
  \end{center}




   It is possible to create a bijection between paths in this
  question and ways of bracketing $(n+1)$ elements.
  To obtain the corresponding path from a bracketing, we first move one step
  to the right, then traverse the bracketing (as a string of symbols), going to the right
  whenever we encounter an opening bracket and upwards whenever we encounter a binary
  operation. It is possible to reverse this construction, so it is bijective.

  For example,
  the path above corresponds to this way of bracketing 6 elements: $(a\cdot(b\cdot (c \cdot d)) \cdot (e \cdot f)$.
\vspace{2em}
   \begin{center}
  \includegraphics[width=\columnwidth]{Triangulation.png}
  ${\textcolor{headings}{\blacktriangle}}$\enspace{\fontcapt {\fontfignr Figure 2:} Illustration of Question (ii)}
  \end{center}
\vspace{-0.5em}

   Figure 2 shows several triangulations of a pentagon, as mentioned in Question (ii).
   It would again be possible to construct a bijection, but we will instead verify that the recurrence relation holds. Technically, there is no $2$-gon, but $C_1 = 1$ conforms to a triangle being already triangulated, while the recurrence
  relation can be explained as follows: For a general triangulation of an $(n+2)$-gon, we choose one special edge and consider the triangle that it is adjacent to. This triangle splits the $(n+2)$-gon into a $(k+1)$-gon and a $(n-k+2)$-gon, each with independent and arbitrary triangulations. So the number $T_n$ of triangulations satisfies
  \[
  {\displaystyle T_{n+1}=\sum_{r=0}^{n}T_{r}T_{n-r}}.
  \]
   It has the same initial values and recurrence relation as the Catalan numbers,
  hence is identical for all $n$.

  \subheading{Hankel transforms}

   One of the most interesting properties of Catalan numbers
  is their Hankel transform. The Hankel transform is a transformation of an infinite
  (integer) sequence $a_{1}, a_{2}, a_{3}, \dots$ to another infinite (integer)
  sequence $h_{1},h_{2},h_{3,}, \dots$ by the following rule:

  $$h_n=\det\begin{pmatrix}a_1&a_2&\cdots & a_n\\a_2 & a_3&\cdots & a_{n+1} \\ \vdots & \vdots & \ddots & \vdots \\ a_n&a_{n+1}&\cdots & a_{2n-1}\end{pmatrix}$$

   For example, the Hankel transform of the Fibonacci numbers $1, 1, 2, 3, 5, \dots$
  is the eventually constant sequence $1,1,0,0, 0, \dots$. We can easily prove this for $n \ge 3$ by
  observing that each row is a linear combination of the previous
  two.

   Interestingly, the Hankel transform of $C_{0},C_{1},C_{2}, \dots$
  as well as $C_{1},C_{2},C_{3}, \dots$ is the constant sequence $1,1,1, \dots$.
  There is a number of reasons why this is interesting:



   (a) It is not guaranteed that by shifting a sequence,
  the Hankel transform stays the same (e.g. $C_{2},C_{3},C_{4}, \dots$
  does not have the Hankel transform of a constant sequence (although
  it is actually a sequence we all know and love!).

   (b) The fact that the determinants stay the same when we
  add more entries is pretty intriguing.

   (c) Although the Fibonacci sequence also fulfils both (a) and
  (b) (except that it is only an \emph{almost} constant sequence), this
  is clearly to be expected from our construction of the sequence; but for Catalan
  numbers, it is not obvious that its Hankel transform is a constant
  sequence even from the recurrence relation (which at first glance
  does seem to be the key to prove it, but actually is not).



   It is possible to use elementary combinatorics to explain
  why the Hankel transform of the Catalan numbers is a constant sequence,
  but this is too involved to be put in a short article. Needless to
  say, Catalan numbers are a real gem of combinatorics, solving many seemingly
  unrelated combinatorial problems, as well as having interesting properties.


\end{multicols}

\end{document}
