Eureka
handover notes for the editor


# This document

Original author: Valentin Hübner, editor of Issue 66

This document is intended as a helpful guide for creating future issues of
Eureka. Obviously, you, the committe, and your editorial team should take
the magazine towards whichever direction you see fit, so feel free to ignore
any or all parts of it.

If I can help you with anything, contact me at valentin.huebner@gmail.com.
Below are the contact details of the editors immediately before me.

- Michael Grayling (Issue 65): Michael.Grayling@newcastle.ac.uk
- Jasper Bird (Issue 64): j1bird@ucsd.edu
- Longtin Chan (Issue 63): https://m.facebook.com/profile.php?id=720939535

Please **make sure this handover document is not lost** and add your own helpful
information. Thank you!

I have included it as part of a repository on Gitlab, which is public at
https://gitlab.com/valentin.huebner/eureka66/. It contains useful files
mentioned in this document. If you want to use the repository to pass these
files and this document on to your successor, you will likely also want to
edit them, so I would suggest making your own fork of my repository, setting
it public, and changing the above link. But of course I can also give you edit
access to mine if you wish.


# Getting submissions

The most important thing is to ask for submissions early. People take ages to
write, during which time you can't do anything else anyway. Many professors
told me they would be willing to write something, only just now they absolutely
don't have any time whatsoever, how about in a few months? Ideally plan over a
year ahead.

About half of our submissions were from people who had given a talk for the
Part III seminar series, and they were all high-quality. So I can recommend
sending out an email to those people (and the participants of the Archimedeans
student talks as well). I sent out personalised emails to them.
I also contacted authors from previous issues, and professors at Cambridge. For
Cambridge professors, if possible ask in person. I had a feeling they felt much
greater pressure to say yes when I did that.

The emails we sent out are included for reference at the end of this document.


# Editing

Decide which articles to accept and which to reject, then assign an editor to
each article. The editor's job is to co-ordinate with the author on suggested
edits, on structure, style, spelling & grammar, figures, references for further
reading, etc. etc. I was really bad at keeping track of which of the editors
were actually getting their job done and which just said they would. I guess
leadership skills aren't a meaningless concept after all but what can I do.


# Layout

Once an article is edited, you can do the layout for it. I don't think that
this task can be reasonably split up amongst several people, so unless you
disagree you will have to do it all yourself.

The difficult question is what software to use.

*Adobe Indesign* is perfect for everything except formulae. For the cover, the
editorial, the contents page, etc. it is definitely the best choice.

*Latex* is good for formulae, ok for a simple two-column layout, but cannot
handle any kind of more complex layout, like text flowing around an image or
any of the myriad of cool things that Indesign can do to make your layout look
nice.

I even tried *Word* as a kind of middle ground and it might have been okay, but
I just thought that the formulae really don't look good.

So my solution – and this is very, very far from perfect – was to combine Latex
and Indesign. I created the page layouts in Latex by using some horrible hacks
like page-, column-, and linebreaks to put the figures where I wanted them. You
can see it all in the Latex code. I created these layouts individually for each
article to minimise unnoticed side effects of imported packages etc.
I created PDFs and imported these into one single Indesign file for the entire
magazine. Sometimes, I left blank spaces for larger images in the Latex document
and placed them in Indesign. It's really not great, but it worked, and I
couldn't think of any other way. Maybe you can come up with something better.

To be precise, I used Xelatex. I don't know about GUIs but if you use the
command line it works just like regular pdflatex. Xelatex is necessary to use
custom fonts in a sane way. The fonts you need are all in the `Fonts` folder.
For the past few issues, we have been using Myriad Pro for headers and Minion
Pro for bodycopy (the main text). Nunito is the font I chose for the cover, and
I forgot why Computer Modern is in that folder. Computer Modern is the default
font of Latex, and Myriad and Minion are something like standard fonts in the
Adobe products, I think.

I mistakenly created the Latex documents in A4 format, while the actual
magazine is B5, but luckily they have the same ratio and I set margins and font
size to fit visually with the prevous issues since I didn't have access to
their source code. So I was able to just scale down the final PDF. If you
decide to reuse my Latex code, you might want to fix that first.

Adobe has relatively cheap student subscriptions which you can supposedly pay
monthly and cancel anytime. I must have either clicked something wrong or been
treated very unfairly quite frankly, very unfairly, let me tell you that,
because when I cancelled Adobe tried to charge me £89 for the rest of the year
so I had to rescue all my money to a new bank account. So watch out when you
sign up!


# Images

I read online that images should have at least 300dpi for high-quality print.
By Google calculator, that is a height of 3000 pixels for a full-page image.
The website letsenhance.io worked really well for when I didn't have an image
that was large enough (although I haven't seen the final print yet …).

I didn't really bother too much about copyright, but made sure to list all the
image sources.

On the cover you can place a barcode to make it look professional. The barcode
is a graphical representation of the ISSN number, which is composed of
9770071224001 + the issue number, so for example 977007122400166 for Issue 66.
There are online tools for creating SVG images from an ISSN.


# Printing

As suggested by Michael, the editor of Issue 65, I ordered the prints from a
local Cambridge company called The Langham Press (www.langhampress.co.uk).
When you request a quote from them, they ask for the following details.

- Size: B5
- Number of pages
- Number of colours: 4 (CMYK)
- Stock: 120gsm Offset, 300gsm Silk for the cover
- Finishing: Gloss laminate for the outer cover. Perfect bind.

We ordered a quantity of 350 copies, Michael ordered 500.


# Distribution

Previously to Issue 66, there used to be a system where subscribers sent cash in
envelopes to top up accounts that they had with us, and then whenever a new
issue came out, the price would be subtracted from their balance. Of course,
that is not only horrible but also economically ineficcient at today's postage
rates. What's more, the list of subscribers together with any account balances
seems to have been irretrievably lost. So we now have a new system where we
send out invoices payable by bank transfer with any shipment, whether for a
subscription or an individual purchase. The invoice template is in the
repository, although you might want to modify it for subscriptions. Invoice
numbers are generated from the current date and a daily-reset counter. Note
that the invoice template is from a period when our treasurer lost access to
the bank account, and والله this is why I had to ask for transfers to my
personal account. So you should change the payment details to the new society
bank account.

We currently charge £6 per issue, plus postage.


# Archive

There is an archive at https://archim.org.uk/eureka/archive. This archive is
also mirrored at https://archive.org/details/eureka-magazine, just in case any
future Archimedeans webmaster messes something up. Anyone can create an account
to upload a document. To add a document to the Eureka collection, I always had
to send a request by email. I am not sure if anyone can do that or if you have
to be given ownership of a collection first, but let me know as soon as you
need it and I will figure it out.

The PDFs all have a cover page with some license information, which is included
in the repository.


# Appendix: emails

## to students

I had the feeling that mentioning some famous names helped capture people's
attention, and many people used the possibility to just click the link to
message me on Facebook Messenger, where sending an email might have seemed as
more of a barrier.

    Hello!

    The Archimedeans are looking for submissions for Eureka, a mathematical
    journal. Our magazine has a long history: Since its first publication in 1939,
    many famous mathematicians and scientists like Paul Erdős, Stephen Hawking,
    Paul Dirac, and G. H. Hardy have written articles for Eureka.
    Most of our authors are students though. To give you an idea of what we are
    looking for, these are some of the topics featured in recent issues:

    - A variant of Ramsey’s theorem
    - Space-filling curves
    - Prime factors of binomials
    - A history of forgotten mathematicians
    - “When logic meets geometry”
    - Mathematical origami
    - Fractals in financial mathematics


    As you can see, the articles may relate to Tripos material, but don’t have to.
    You can even simply write about an aspect of a specific course in a way that
    would be interesting for students who didn’t take that course.

    So if you have an idea for an article that you could contribute, please
    message me on Facebook Messenger or send us an email at archim-eureka@srcf.net!

    Valentin

In 2017, Michael wrote this email:

    Dear all,

    Once more we would like to invite all of you to contribute to Eureka -
    the Armichemedeans’ journal.

    As you may know, the magazine consists of a wide range of articles on
    current mathematical research, recreational mathematics and anything
    else vaguely mathematical. Past writers include Paul Erdos, GH Hardy and
    Stephen Hawking. If you would like to find out more you can check out
    several recent issues here https://mathigon.org/eureka, or the
    guidelines for authors here
    http://www.archim.org.uk/eureka_author_guide.php.

    We are looking for any articles of mathematical nature: applications of
    maths, research, biographies, puzzles or anything else you think might
    interest undergraduate mathematics students. If you are interested in
    contributing, please email archim-eureka-editor@srcf.ucam.org by Sunday
    5th March.

    In addition, if you would like to buy past issues, or subscribe to
    future ones, please email the subscriptions manager
    archim-subscriptions-manager@srcf.ucam.org.

    We look forward to hearing from you!

    Best wishes,

    Michael

    Editor
    Issue 65


## to past contributors

best not take any hints from me, since all of the ones I asked declined


## to Seminar Series speakers

    Hi X,

    The Archimedeans (the Cambridge maths society) are currently preparing issue 66
    of Eureka, a student maths magazine. Eureka has a long history, and many famous
    mathematicians and scientists like Paul Erdős, Stephen Hawking, Paul Dirac, and
    G. H. Hardy have written articles for it.
    I am writing to ask if you would like to write an article for the upcoming
    issue. You gave a talk at the Part III seminar series last term, and if you
    think you could present that topic in written form that would certainly make for
    agreat article for our readers.
    Please let me know if you are interested!

    Best,
    Valentin

    PS: you can also contact me on Facebook Messenger if you want.


